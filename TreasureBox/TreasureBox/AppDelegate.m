//
//  AppDelegate.m
//  TreasureBox
//
//  Created by Chen on 16/2/3.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "JoyTextVC.h"
#import "NewsHomeVC.h"
#import "BoxTabBar.h"
#import "BoxTabbarView.h"
#import "MusicHomeVC.h"
#import "XTPageViewController.h"
#import "JoyPicVC.h"
#import "JoyVideoVC.h"
#import "YYFPSLabel.h"


@interface AppDelegate ()<XTPageViewControllerDataSource>{

}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    ViewController *vc = [[ViewController alloc]init];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    nav.navigationBarHidden = YES;
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
 
    
    
//    JoyHomeVC *joyhome = [[JoyHomeVC alloc]init];
    
    
    XTPageViewController *joyhome = [[XTPageViewController alloc] initWithTabBarStyle:XTTabBarStyleCursorUnderline];
    joyhome.dataSource = self;
//    joyhome.boxNavigationBar.hidebar = YES;
    joyhome.tabBarItem.title = @"笑话";
    UINavigationController *joyhomenav = [[UINavigationController alloc]initWithRootViewController:joyhome];
    joyhomenav.navigationBarHidden = YES;

    joyhomenav.tabBarItem.title = @"笑话";
    
  
    
    
    
    NewsHomeVC *newshome = [[NewsHomeVC alloc]init];
    newshome.tabBarItem.title = @"新闻";
    UINavigationController *newshomenav = [[UINavigationController alloc]initWithRootViewController:newshome];
    newshomenav.navigationBarHidden = YES;
    newshomenav.tabBarItem.title = @"新闻";
    
    
    MusicHomeVC *musichome = [[MusicHomeVC alloc]init];
    musichome.tabBarItem.title = @"音乐";
    UINavigationController *musichomenav = [[UINavigationController alloc]initWithRootViewController:musichome];
    musichomenav.navigationBarHidden = YES;
    musichomenav.tabBarItem.title = @"音乐";

    
    
  
    
    ViewController *tabC = [[ViewController alloc]init];
     tabC.viewControllers=@[joyhomenav,newshomenav,musichomenav];
//    tabC.tabBar.hidden = YES;
    BoxTabbarView *view  = [[BoxTabbarView alloc]initWithFrame:tabC.tabBar.bounds];
    view.delegate = self;
//    [tabC.tabBar addBoxTabbarView:view];
    self.window.rootViewController = tabC;
    YYFPSLabel *fps = [YYFPSLabel new];
    fps.centerY = 40;
    fps.left = 5;
    
    
    
    
   
    [self.window makeKeyAndVisible];
     [[UIApplication sharedApplication].keyWindow addSubview:fps];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    if ([viewController.tabBarItem.title isEqualToString:[[tabBarController.tabBar items]objectAtIndex:tabBarController.selectedIndex].title]) {
        
       ;
        return NO;
    }
    else{
        return YES;
    }
}

-(void)boxTabbar:(BoxTabbarView *)view didselectAt:(NSInteger)index{
    UITabBarController *tab =   (UITabBarController*)self.window.rootViewController;
    tab.selectedIndex = index;
}

- (NSInteger)numberOfPages {
    return 3;
}

- (NSString*)titleOfPage:(NSInteger)page {
    
    NSArray *titleArr = [NSArray arrayWithObjects:@"文字",@"纯图",@"视频", nil];
    return titleArr[page];
}

- (BoxBaseViewControler*)constrollerOfPage:(NSInteger)page {
    JoyTextVC *joyText = [[JoyTextVC alloc] init];
    UINavigationController *joyTextnav = [[UINavigationController alloc]initWithRootViewController:joyText];
    joyTextnav.navigationController.navigationBarHidden = YES;
    
    JoyPicVC *joyPic = [[JoyPicVC alloc] init];
    UINavigationController *joyPicNav = [[UINavigationController alloc]initWithRootViewController:joyPic];
    joyPicNav.navigationController.navigationBarHidden = YES;
    
    
    JoyVideoVC *joyVideoVC = [[JoyVideoVC alloc] init];
    UINavigationController *JoyVideoNav = [[UINavigationController alloc]initWithRootViewController:joyVideoVC];
    JoyVideoNav.navigationController.navigationBarHidden = YES;
    
    
    
    
    
    NSArray *arr = [NSArray arrayWithObjects:joyText,joyPic,joyVideoVC, nil];
    return [arr objectAtIndex:page];
}


@end
