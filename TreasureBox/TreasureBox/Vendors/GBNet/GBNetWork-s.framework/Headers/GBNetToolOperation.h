//
//  GBNetToolOperation.h
//  GBNetWork
//
//  Created by chenguibang on 16/3/3.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 缓存枚举
 */
typedef enum : NSUInteger {
    GBCachePolicyNone,//!< 无缓存 直接请求服务器  不读缓存
    GBCachePolicyPermanent,//!<  首先读缓存  若缓存存在 则不向服务器请求数据
} GBCachePolicy;


typedef enum : NSUInteger {
    GBDebugzModeAlertRequest,
    GBDebugzModeAlertRespose,
    GBDebugzModeAlertBoth,
    GBDebugzModeAlertNone,
} GBDebugzMode;

@interface GBNetToolOperation : NSObject
@property(nonatomic,assign) BOOL showHud; //!< 是否显示请求指示器
@property(nonatomic,assign) BOOL encrypt; //!< 是否对数据进行加密
@property(nonatomic,copy)   NSString *hudText;//!< 请求提示的信息;
@property(nonatomic,assign) GBCachePolicy cachePolicy; //!< 缓存策略
@property(nonatomic,assign) GBDebugzMode debugLogMode; //!< 调试模式
@property(nonatomic,assign) BOOL thirdServer; //!< 是否请求的是第三方服务器  如果是这样 就不做加密处理

+(GBNetToolOperation*)defaultOperation;

@end
