//
//  ReqBodyModel.h
//  GBNetWork
//
//  Created by Chen on 16/2/1.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReqBodyModel : NSObject

-(NSDictionary*)formatToDict;

@end
