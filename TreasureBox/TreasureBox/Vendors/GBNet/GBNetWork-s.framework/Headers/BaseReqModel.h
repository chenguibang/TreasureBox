//
//  BaseReqModel.h
//  GBNetWork
//
//  Created by Chen on 16/2/1.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReqHeadModel.h"
#import "ReqBodyModel.h"

@interface BaseReqModel : NSObject
@property(nonatomic,strong) ReqHeadModel *head;
@property(nonatomic,strong) ReqBodyModel *body;
//@property(nonatomic,readonly,copy) NSString *baseUrl;  //!< 请求服务器应该是只读属性
@property(nonatomic,copy) NSString *baseUrl;  //!< 请求服务器应该是只读属性

@end
