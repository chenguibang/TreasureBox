//
//  ReqHeadModel.h
//  GBNetWork
//
//  Created by Chen on 16/2/1.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReqHeadModel : NSObject

@property(nonatomic,copy,readonly) NSString *uuid;           //!< 设备唯一标示.
@property(nonatomic,copy,readonly) NSString *sessionId;      //!< sessionId.
@property(nonatomic,copy,readonly) NSString *appVersion;     //!< app的版本.


@end
