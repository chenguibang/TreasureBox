//
//  GBNetTool.h
//  GBNetWork
//
//  Created by Chen on 16/2/1.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GBNetWork/BaseReqModel.h>
#import <GBNetWork/GBNetToolOperation.h>


/**
 缓存枚举
 */
typedef enum : NSUInteger {
    CachePolicyNone,//!< 无缓存 直接请求服务器  不读缓存
    CachePolicyPermanent,//!<  首先读缓存  若缓存存在 则不向服务器请求数据
} CachePolicy;
/**
 请求方式
 */
typedef enum : NSUInteger {
    RequestModeGet,
    RequestModePost,
    RequestModeDownLoad,
} RequestMode;

@class AFHTTPRequestOperation;
typedef void(^ReqSuccess)(NSDictionary* result);
typedef void(^ReqFailure)(NSURLSessionDataTask * task, NSError *error);

typedef void(^DownLoadProgress)(NSProgress * downloadProgress ,float percent);
typedef void(^DownLoadCompletion)(NSURLResponse * response, NSURL * filePath, NSError * error);

@interface GBNetTool : NSObject{
   
}
/**
 *  单例模式 GBNetTool
 *
 *  @return 返回网络请求工具实例 GBNetTool
 */
+(GBNetTool*)shareGBNetTool;


#pragma mark - GET请求方法
/**
 *  get 异步请求
 *
 *  @param requestModel 请求模型
 *  @param sucess       请求成功回调
 *  @param failure      请求失败回调
 */
+(void)netGetAsyn:(BaseReqModel*)requestModel
          success:(ReqSuccess)sucess
          failure:(ReqFailure)failure
        operation:(GBNetToolOperation*)operation;
#pragma mark - POST请求方法
/**
 *  post 异步请求
 *
 *  @param requestModel 请求模型
 *  @param sucess       请求成功回调
 *  @param failure      请求失败回调
 */
+(void)netPostAsyn:( BaseReqModel*)requestModel
           success:(ReqSuccess)sucess
           failure:(ReqFailure)failure
         operation:(GBNetToolOperation*)operation;



+(void)netDownLoadFile:(NSString*)urlStr
              progress:(DownLoadProgress)progress
                compet:(DownLoadCompletion)complet
             operation:(GBNetToolOperation*)operation;

/**
 *  清空所有url缓存
 */
+(void)clearAllCache;

@end
