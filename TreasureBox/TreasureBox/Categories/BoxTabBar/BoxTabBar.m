//
//  BoxTabBar.m
//  TreasureBox
//
//  Created by Chen on 16/2/3.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "BoxTabBar.h"
#import <objc/runtime.h>
@implementation UITabBar(BoxTabBar)

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


//+(void)load{
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        Method originalMethod = class_getInstanceMethod([self class], @selector(layoutSubviews));
//        Method swizzledMethod = class_getInstanceMethod([self class], @selector(swizzled_layoutSubviews));
//        method_exchangeImplementations(originalMethod, swizzledMethod);
//    });
//    
//}


-(void)setBackgroundView{
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            Method originalMethod = class_getInstanceMethod([self class], @selector(layoutSubviews));
            Method swizzledMethod = class_getInstanceMethod([self class], @selector(swizzled_layoutSubviews));
            method_exchangeImplementations(originalMethod, swizzledMethod);
        });
}

- (void)swizzled_layoutSubviews
{
    [self swizzled_layoutSubviews];
    for (UIView *chidView in self.subviews) {
        [chidView removeFromSuperview];
    }
    
    
    UIView *bg = [[UIView alloc]initWithFrame:self.bounds];
    bg.backgroundColor = [UIColor grayColor];
    
    [self addSubview:bg];
    
    

    
    
    
    
    NSArray *ass = self.subviews;

    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        for (UIView *view in [self subviews]) {
//            view.backgroundColor = [UIColor purpleColor];
        }
    }
    return self;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        for (UIView *view in [self subviews]) {
//            view.backgroundColor = [UIColor purpleColor];
        }
    }
    return self;
}


-(void)addBoxTabbarView:(UIView *)tabbarView{
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        Method originalMethod = class_getInstanceMethod([self class], @selector(layoutSubviews));
//        Method swizzledMethod = class_getInstanceMethod([self class], @selector(swizzled_layoutSubviews));
//        method_exchangeImplementations(originalMethod, swizzledMethod);
//        
//        
//    });
    
    for (UIView *chidView in self.subviews) {
        [chidView removeFromSuperview];
    }
    [self addSubview:tabbarView];
}



@end
