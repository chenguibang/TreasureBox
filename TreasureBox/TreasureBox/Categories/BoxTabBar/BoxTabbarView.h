//
//  BoxTabbarView.h
//  TreasureBox
//
//  Created by Chen on 16/2/7.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol BoxTabbarViewDelegate;

@interface BoxTabbarView : UIView
@property(nonatomic,assign) id<BoxTabbarViewDelegate> delegate;
@end


@protocol BoxTabbarViewDelegate <NSObject>

-(void)boxTabbar:(BoxTabbarView*)view didselectAt:(NSInteger)index;

@end