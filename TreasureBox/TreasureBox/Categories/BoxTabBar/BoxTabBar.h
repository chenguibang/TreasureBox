//
//  BoxTabBar.h
//  TreasureBox
//
//  Created by Chen on 16/2/3.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBar(BoxTabBar)

-(void)setBackgroundView;
-(void)addBoxTabbarView:(UIView *)tabbarView;
@end
