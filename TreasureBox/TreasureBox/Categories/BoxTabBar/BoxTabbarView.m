//
//  BoxTabbarView.m
//  TreasureBox
//
//  Created by Chen on 16/2/7.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "BoxTabbarView.h"

@implementation BoxTabbarView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor orangeColor];
        
        for (int i = 0; i<4; i++) {
            UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(i*self.frame.size.width/4, 0, self.frame.size.width/4, self.frame.size.height)];
            [btn setTitle:@"按钮" forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(btn:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:btn];
            btn.tag = i;
        }
    }
    return self;
}

-(void)btn:(UIButton*)sender{
    if (self.delegate&&[self.delegate respondsToSelector:@selector(boxTabbar:didselectAt:)]) {
        [self.delegate boxTabbar:self didselectAt:sender.tag];
    }
}
@end
