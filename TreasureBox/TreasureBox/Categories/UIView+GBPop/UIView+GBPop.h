//
//  UIView+GBPop.h
//  PopView
//
//  Created by chenguibang on 16/5/24.
//  Copyright © 2016年 chenguibang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBBackgroudView.h"




typedef enum : NSUInteger {
    GBPopAnimationStatusBegin,
    GBPopAnimationStatusOn,
    GBPopAnimationStatusEnd
} GBPopAnimationStatus;

typedef enum : NSUInteger {
    GBPopAnimationInSlideFromTop,
    GBPopAnimationInSlideFromLeft,
    GBPopAnimationInSlideFromBottom,
    GBPopAnimationInSlideFromRight,
    
    GBPopAnimationInShake
} GBPopAnimationIn;

typedef enum : NSUInteger {
    GBPopAnimationOutNone,
    GBPopAnimationOutSlideToTop,
    GBPopAnimationOutSlideToLeft,
    GBPopAnimationOutSlideToBottom,
    GBPopAnimationOutSlideToRight
} GBPopAnimationOut;

typedef void(^AnimationInConfig)(GBBackgroudView *backgroundView ,GBPopAnimationStatus animationStatus ,UIView *view);
typedef void(^AnimationOutConfig)(GBBackgroudView *backgroundView ,GBPopAnimationStatus animationStatus ,UIView *view);
@interface UIView(GBPop)


/**
 *  弹到一个新的windown上
 */



-(void)gb_popInView:(UIView*)baseView animationIn:(GBPopAnimationIn)animationIn backgroundConfig:(AnimationInConfig)config touchOutside:(TouchBlock)touchOutside;



//-(void)gb_popWithInConfig:(AnimationInConfig)inConfig outConfig:(AnimationOutConfig)outConfig;

-(void)gb_dismiss:(GBPopAnimationOut)animationOut;

-(void)gb_slideFromTop;

-(void)gb_slideFromLeft;

-(void)gb_slideFromBottom;

-(void)gb_slideFromRight;


-(void)gb_slideOutTop;

-(void)gb_slideOutLeft;

-(void)gb_slideOutBottom;

-(void)gb_slideOutRight;

@end
