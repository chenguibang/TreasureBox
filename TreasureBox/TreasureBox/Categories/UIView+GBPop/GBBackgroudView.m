//
//  GBBackgroudView.m
//  PopView
//
//  Created by chenguibang on 16/5/24.
//  Copyright © 2016年 chenguibang. All rights reserved.
//

#import "GBBackgroudView.h"

@implementation GBBackgroudView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+(GBBackgroudView *)defaultBackground{
//    static dispatch_once_t onceToken;
     GBBackgroudView *background = nil;
//    dispatch_once(&onceToken, ^{
//        background = [[self alloc]init];
//        
//    });
    background = [[self alloc]init];
    background.frame = [UIScreen mainScreen].bounds;
    background.backgroundColor = [UIColor clearColor];
    return background;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    if (self.touchBlock) {
        self.touchBlock(touches,event);
    }
}

@end
