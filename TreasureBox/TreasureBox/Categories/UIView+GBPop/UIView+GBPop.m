//
//  UIView+GBPop.m
//  PopView
//
//  Created by chenguibang on 16/5/24.
//  Copyright © 2016年 chenguibang. All rights reserved.
//

#import "UIView+GBPop.h"

@implementation UIView(GBPop)


//-(void)gb_popWithInConfig:(AnimationInConfig)inConfig outConfig:(AnimationOutConfig)outConfig{
//    UIWindow *win = [UIApplication sharedApplication].keyWindow;
//    GBBackgroudView *bgView = [GBBackgroudView defaultBackground];
//    bgView.alpha = 1;
//    [win addSubview:bgView];
//    [win addSubview:self];
//    
//    
//    if (inConfig) {
//        inConfig(bgView,GBPopAnimationStatusBegin,self);
//    }
//    
//    if (inConfig) {
//        inConfig(bgView,GBPopAnimationStatusOn,self);
//    }
//    
//    if (inConfig) {
//        inConfig(bgView,GBPopAnimationStatusEnd,self);
//    }
//    
//    
//
//    __weak GBBackgroudView *weakbgView = bgView;
//    bgView.touchBlock = ^(NSSet<UITouch *> *touches,UIEvent *event){
//        if (outConfig) {
//            outConfig(weakbgView,GBPopAnimationStatusBegin,self);
//        }
//        
//        if (outConfig) {
//            outConfig(weakbgView,GBPopAnimationStatusOn,self);
//        }
//        
//        if (outConfig) {
//            outConfig(weakbgView,GBPopAnimationStatusEnd,self);
//        }
//
//    };
//  
//    
//    
//    
//    
//}




-(void)gb_popInView:(UIView*)baseView animationIn:(GBPopAnimationIn)animationIn backgroundConfig:(AnimationInConfig)config touchOutside:(TouchBlock)touchOutside{
//    UIWindow *win = [UIApplication sharedApplication].keyWindow;
    
    UIView *baseContentView = nil;
    if (baseView != nil) {
        baseContentView = baseView.window;
    }else{
        baseContentView = [UIApplication sharedApplication].keyWindow;
    }
    
    GBBackgroudView *bgView = [GBBackgroudView defaultBackground];
    bgView.alpha = 1;
    [baseContentView addSubview:bgView];
    [baseContentView addSubview:self];

    //动画开始前
    if (config) {
        config(bgView,GBPopAnimationStatusBegin,self);
    }
    
    //这里动画事件
    
    
    
    
    
    switch (animationIn) {
        case GBPopAnimationInSlideFromTop:{
            [self gb_slideFromTop];
            
        }
            
            break;
            
        case GBPopAnimationInSlideFromLeft:{
            [self gb_slideFromLeft];
        
            
        }
            break;
            
            
        case GBPopAnimationInSlideFromBottom:{
            [self gb_slideFromBottom];
        }
            
            
            break;
            
            
        case GBPopAnimationInSlideFromRight:{
            [self gb_slideFromRight];
           
        
        }
            
            
            break;
            
        case GBPopAnimationInShake:{
            [self gb_shakeIn];
        }
            
            break;
            
        default:
            break;
    }
    
    //动画结束
    
   
    
    
    if (touchOutside) {
        bgView.touchBlock = touchOutside;
    }else{
        bgView.touchBlock = ^(NSSet<UITouch *> *touches,UIEvent *event){
            [self gb_dismiss:GBPopAnimationOutNone];
        };
        
    }
    
}



-(void)gb_dismiss:(GBPopAnimationOut)animationOut{
    
    
    
    
    
    
    
    
    
    
    
    GBBackgroudView *bgView = [GBBackgroudView defaultBackground];
    NSMutableArray<GBBackgroudView*> *backgroudViews = [[NSMutableArray alloc]init];
    for (UIView *view in [UIApplication sharedApplication].keyWindow.subviews) {
        if ([view isKindOfClass:[GBBackgroudView class]]) {
            [backgroudViews addObject:(GBBackgroudView*)view];
        }
    }
    
    [self removeFromSuperview];
    [backgroudViews.lastObject removeFromSuperview];
    
//    [bgView removeFromSuperview];
  
    
}

-(void)gb_slideFromTop{
    self.alpha = 0;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.alpha = 1;
    [UIView commitAnimations];
    CGPoint tempPoint = self.center;
    self.center = CGPointMake(tempPoint.x,- self.frame.size.height/2);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [self setCenter:tempPoint];
    [UIView commitAnimations];
}

-(void)gb_slideFromLeft{
    self.alpha = 0;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.alpha = 1;
    [UIView commitAnimations];
    CGPoint tempPoint = self.center;
    self.center = CGPointMake(-self.frame.size.width/2,tempPoint.y);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [self setCenter:tempPoint];
    [UIView commitAnimations];

}

-(void)gb_slideFromBottom{
    self.alpha = 0;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.alpha = 1;
    [UIView commitAnimations];
    CGPoint tempPoint = self.center;
    self.center = CGPointMake(tempPoint.x,[UIScreen mainScreen].bounds.size.height+self.frame.size.height/2);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [self setCenter:tempPoint];
    [UIView commitAnimations];
}

-(void)gb_slideFromRight{
    self.alpha = 0;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.alpha = 1;
    [UIView commitAnimations];
    CGPoint tempPoint = self.center;
    self.center = CGPointMake([UIScreen mainScreen].bounds.size.width+self.frame.size.width/2,tempPoint.y);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [self setCenter:tempPoint];
    [UIView commitAnimations];

}


-(void)gb_shakeIn{
//     [self pulseToSize:1.05 duration:0.2 repeat:NO];
}


-(void)gb_slideOutTop{
    GBBackgroudView *bgView = [GBBackgroudView defaultBackground];
    self.alpha = 1;
    bgView.alpha = 1;
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0;
        bgView.alpha = 0;
        self.center  = CGPointMake(self.center.x, -self.frame.size.height/2);
        
    } completion:^(BOOL finished) {
        NSMutableArray<GBBackgroudView*> *backgroudViews = [[NSMutableArray alloc]init];
        for (UIView *view in [UIApplication sharedApplication].keyWindow.subviews) {
            if ([view isKindOfClass:[GBBackgroudView class]]) {
                [backgroudViews addObject:(GBBackgroudView*)view];
            }
        }
        
        [self removeFromSuperview];
        [backgroudViews.lastObject removeFromSuperview];
        [bgView removeFromSuperview];
    }];
}

-(void)gb_slideOutLeft{
    GBBackgroudView *bgView = [GBBackgroudView defaultBackground];
    self.alpha = 1;
    bgView.alpha = 1;
    [UIView animateWithDuration:0.2 animations:^{
        bgView.alpha = 0;
        self.alpha = 0;
        self.center  = CGPointMake(-self.frame.size.width/2, self.center.y);
        
    } completion:^(BOOL finished) {
       
        NSMutableArray<GBBackgroudView*> *backgroudViews = [[NSMutableArray alloc]init];
        for (UIView *view in [UIApplication sharedApplication].keyWindow.subviews) {
            if ([view isKindOfClass:[GBBackgroudView class]]) {
                [backgroudViews addObject:(GBBackgroudView*)view];
            }
        }
        
        [self removeFromSuperview];
        [backgroudViews.lastObject removeFromSuperview];
        [bgView removeFromSuperview];

    }];
}
-(void)gb_slideOutBottom{
    GBBackgroudView *bgView = [GBBackgroudView defaultBackground];
    self.alpha = 1;
     bgView.alpha = 1;
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0;
         bgView.alpha = 0;
        self.center  = CGPointMake(self.center.x, [UIScreen mainScreen].bounds.size.height+self.frame.size.height/2);
        
    } completion:^(BOOL finished) {
        
        NSMutableArray<GBBackgroudView*> *backgroudViews = [[NSMutableArray alloc]init];
        for (UIView *view in [UIApplication sharedApplication].keyWindow.subviews) {
            if ([view isKindOfClass:[GBBackgroudView class]]) {
                [backgroudViews addObject:(GBBackgroudView*)view];
            }
        }
        
        [self removeFromSuperview];
        [backgroudViews.lastObject removeFromSuperview];
        [bgView removeFromSuperview];

    }];
}

-(void)gb_slideOutRight{
    GBBackgroudView *bgView = [GBBackgroudView defaultBackground];
    bgView.alpha = 1;
    self.alpha = 1;
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0;
        bgView.alpha = 0;
        self.center  = CGPointMake([UIScreen mainScreen].bounds.size.width+self.frame.size.width/2, self.center.y);
        
    } completion:^(BOOL finished) {
       
        NSMutableArray<GBBackgroudView*> *backgroudViews = [[NSMutableArray alloc]init];
        for (UIView *view in [UIApplication sharedApplication].keyWindow.subviews) {
            if ([view isKindOfClass:[GBBackgroudView class]]) {
                [backgroudViews addObject:(GBBackgroudView*)view];
            }
        }
        
        [self removeFromSuperview];
        [backgroudViews.lastObject removeFromSuperview];
        [bgView removeFromSuperview];

    }];
}



@end
