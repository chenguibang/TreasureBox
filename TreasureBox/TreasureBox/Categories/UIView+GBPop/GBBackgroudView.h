//
//  GBBackgroudView.h
//  PopView
//
//  Created by chenguibang on 16/5/24.
//  Copyright © 2016年 chenguibang. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^TouchBlock)(NSSet<UITouch *> *touches,UIEvent *event);

@interface GBBackgroudView : UIView
@property(nonatomic,strong)TouchBlock touchBlock;

+(GBBackgroudView*)defaultBackground;


@end
