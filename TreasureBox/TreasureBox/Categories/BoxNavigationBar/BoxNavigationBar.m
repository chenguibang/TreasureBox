//
//  BoxNavigationBar.m
//  TreasureBox
//
//  Created by Chen on 16/2/3.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "BoxNavigationBar.h"

@implementation BoxNavigationBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(instancetype)initWithDelegate:(id<BoxNavigationBarDelegate>)delegate{
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64);
        
        // 定义毛玻璃效果
        UIBlurEffect * blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView * effe = [[UIVisualEffectView alloc]initWithEffect:blur];
        effe.backgroundColor = [UIColor whiteColor];
        effe.frame = self.bounds;
//        [self addSubview:effe];
        self.leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH/4, 44)];
        [self.leftBtn addTarget:self action:@selector(leftAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        [self.leftBtn setImage:[UIImage imageNamed:@"titleback"] forState:UIControlStateNormal];
        [self addSubview:self.leftBtn];
        self.midLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH, 44)];
        [self addSubview:self.midLabel];
        self.midLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
        self.midLabel.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64);
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64);
    }
    return self;
}

-(void)leftAction:(id)sender{
    if (self.delegate&&[self.delegate respondsToSelector:@selector(didClickLeftBtn:)]) {
        [self.delegate didClickLeftBtn:sender];
    }else{

    }
    
}

-(void)setHidebar:(BOOL)hidebar{
    if (hidebar) {
       
         self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20);
    }else{
        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64);
    }
     _hidebar= hidebar;
}

-(void)setHideAllBar:(BOOL)hideAllBar{
    if (hideAllBar) {
        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0);
    }else{
        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64);
    }
}

@end
