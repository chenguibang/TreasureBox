//
//  BoxNavigationBar.h
//  TreasureBox
//
//  Created by Chen on 16/2/3.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BoxNavigationBarDelegate <NSObject>

-(void)didClickLeftBtn:(id)sender;

@end

@interface BoxNavigationBar : UIView
@property(nonatomic,assign)id<BoxNavigationBarDelegate> delegate;
@property(nonatomic,strong) UIButton *leftBtn;
@property(nonatomic,strong) UILabel  *midLabel;
@property(nonatomic,strong) UIButton *rightBtn;
@property(nonatomic,assign) BOOL hidebar;
@property(nonatomic,assign) BOOL hideAllBar;
-(instancetype)initWithDelegate:(id<BoxNavigationBarDelegate>)delegate;
@end
