//
//  CornerView.h
//  WoChuangFu
//
//  Created by chenguibang on 16/3/17.
//  Copyright © 2016年 asiainfo-linkage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView(CornerView)

+(UIView*)addCorner;

-(UIImage*)addCornerWithradius:(CGFloat)radius borderWidth:(CGFloat)borderWidth backgroundColor:(UIColor *)backgroundColor borderColor:(UIColor*)borderColor;

@end
