//
//  CornerView.m
//  WoChuangFu
//
//  Created by chenguibang on 16/3/17.
//  Copyright © 2016年 asiainfo-linkage. All rights reserved.
//

#import "CornerView.h"

@implementation UIView(CornerView)

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+(UIView *)addCorner{
    
    return nil;
    
}

-(UIImage*)addCornerWithradius:(CGFloat)radius borderWidth:(CGFloat)borderWidth backgroundColor:(UIColor *)backgroundColor borderColor:(UIColor*)borderColor {
//    UIGraphicsBeginImageContextWithOptions(sizeToFit, false, [UIScreen mainScreen].scale);
//    
//    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    CGContextMoveToPoint(context, 开始位置);  // 开始坐标右边开始
//    CGContextAddArcToPoint(context, x1, y1, x2, y2, radius);  // 这种类型的代码重复四次
//    
//    CGContextDrawPath(UIGraphicsGetCurrentContext(), .FillStroke)
//    UIImage *output = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    
    CGFloat scale =   [UIScreen mainScreen].scale;
    UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, 1.0f);
    CGContextRef contentRef =  UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(contentRef,1,1,1,1.0);//画笔线的颜色
    CGContextSetRGBFillColor(contentRef, 0, 0, 0, 0);
//    CGContextSetLineWidth(contentRef, 10.0);//线的宽度
    CGContextAddArc(contentRef, self.width/2    , self.height/2, self.width/2, 0, 2*M_PI, 0); //添加一个圆
    CGContextDrawPath(contentRef, kCGPathFillStroke); //绘制路径
    
    UIImage *image  =    UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
