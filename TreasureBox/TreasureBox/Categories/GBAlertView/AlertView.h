//
//  AlertView.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/21.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertView : UIView
@property(nonatomic,retain) UILabel *title;
@property(nonatomic,retain)UITextView *textFile;
@property(nonatomic,copy) NSArray *buttonTitles;
@end
