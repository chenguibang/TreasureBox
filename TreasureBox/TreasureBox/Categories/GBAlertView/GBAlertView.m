//
//  GBAlertView.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/21.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "GBAlertView.h"

@implementation GBAlertView

+(GBAlertView *)shareAlertView{
    static dispatch_once_t onceToken;
    static GBAlertView *alert = nil;
    dispatch_once(&onceToken, ^{
        alert = [[self alloc]init];
    });
    return alert;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        windown = [UIApplication sharedApplication].keyWindow;
    }
    return self;
}

+(void)showAlertViewWith:(NSString *)title buttonTitle:(NSArray *)titles message:(NSString*)message clickBlock:(ClickBlock)block{
    
    [[self shareAlertView] showAlertViewWith:title message:message buttonTitle:titles clickBlock:block];
}

-(void)showAlertViewWith:(NSString *)title message:(NSString*)message buttonTitle:(NSArray *)titles clickBlock:(ClickBlock)block{
    self.alertwinView.backgroundColor = UIColorWithRGBA(0, 0, 0, 0.9);
    self.alertView.title.text = title;
    self.alertView.textFile.text = message;
    
}

-(AlertView *)alertView{
    if (!_alertView) {
        _alertView = [AlertView new];
        [windown addSubview:_alertView];
        
        _alertView.sd_layout.widthRatioToView(windown,0.8).centerXEqualToView(windown).topSpaceToView(windown,100);
    }
    return _alertView;
}

-(UIVisualEffectView *)alertwinView{
    if (!_alertwinView) {
//        _alertwinView = [UIView new];
        UIBlurEffect *beffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        _alertwinView = [[UIVisualEffectView alloc]initWithEffect:beffect];
        _alertwinView.alpha = 0.5;
        [windown addSubview:_alertwinView];
        _alertwinView.sd_layout.widthRatioToView(windown,1).heightRatioToView(windown,1).leftEqualToView(windown).rightEqualToView(windown);
        
        
        UITapGestureRecognizer*  single = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTap:)];
        [_alertwinView addGestureRecognizer:single];
    }
    return _alertwinView;
}

-(void)singleTap:(UITapGestureRecognizer*)reco{
    if (self.alertView.textFile&&[self.alertView.textFile isFirstResponder]) {
        [self.alertView.textFile resignFirstResponder];
    }else{
       [self dismiss];
    }
    
    
}

-(void)dismiss{
    [UIView animateWithDuration:0.3 animations:^{
        self.alertwinView.alpha = 0;
        self.alertView.alpha = 0;
        
    } completion:^(BOOL finished) {
        [self.alertwinView removeFromSuperview];
        [self.alertView removeFromSuperview];
        self.alertView = nil;
        self.alertwinView = nil;
    }];
}
-(UILabel *)title{
    if (!_title) {
        _title = [UILabel new];
    }
    return _title;
}

@end
