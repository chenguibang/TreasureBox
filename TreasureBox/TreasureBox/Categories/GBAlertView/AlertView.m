//
//  AlertView.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/21.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "AlertView.h"

@implementation AlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.sd_cornerRadius = @(5);
    }
    return self;
}

-(UILabel *)title{
    if (!_title) {
        _title = [UILabel new];
        [self addSubview:_title];
        _title.textAlignment = NSTextAlignmentCenter;
        _title.sd_layout.leftEqualToView(self).rightEqualToView(self).topEqualToView(self).autoHeightRatio(0);
        _title.backgroundColor = [UIColor whiteColor];
    }
    return _title;
}
-(UITextView *)textFile{
    if (!_textFile) {
        _textFile = [UITextView new];
        [self addSubview:_textFile];
        _textFile.sd_layout.leftEqualToView(self).rightEqualToView(self).topSpaceToView(self.title,1).autoHeightRatio(0).minHeightIs(33);
        
        _textFile.backgroundColor = [UIColor whiteColor];
        
        _textFile.font = [UIFont systemFontOfSize:13];
        [self setupAutoHeightWithBottomView:_textFile bottomMargin:0];
        [_textFile.rac_textSignal subscribeNext:^(id x) {
            
            CGSize size = [_textFile sizeThatFits:CGSizeMake(_textFile.frame.size.width, 200)];
            CGFloat textViewContentHeight = size.height>150?150:size.height;
            

            [UIView animateWithDuration:0.3 animations:^{
                NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
                [style setLineBreakMode:NSLineBreakByCharWrapping];
                NSDictionary *attributes = @{ NSFontAttributeName : _textFile.font, NSParagraphStyleAttributeName : style };
                float height = [_textFile.text boundingRectWithSize:CGSizeMake(_textFile.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:attributes context:nil].size.height;;
//                [_textFile setContentSize:CGSizeMake(_textFile.width, textViewContentHeight)];
                _textFile.sd_resetLayout.leftEqualToView(self).rightEqualToView(self).topSpaceToView(self.title,0).minHeightIs(44).heightIs(textViewContentHeight).maxHeightIs(150);
            }];
            
       
        }];
    }
    return _textFile;
}
-(void)setButtonTitles:(NSArray *)buttonTitles{
    _buttonTitles = buttonTitles;
    
}
@end
