//
//  GBAlertView.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/21.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlertView.h"

typedef BOOL(^ClickBlock)(NSInteger *index);

@interface GBAlertView : NSObject{
    UIWindow *windown;
}
@property(nonatomic,strong)UIVisualEffectView *alertwinView;
@property(nonatomic,strong)AlertView *alertView;
@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UITextField *message;
+(GBAlertView*)shareAlertView;
+(void)showAlertViewWith:(NSString *)title buttonTitle:(NSArray *)titles message:(NSString*)message clickBlock:(ClickBlock)block;
@end
