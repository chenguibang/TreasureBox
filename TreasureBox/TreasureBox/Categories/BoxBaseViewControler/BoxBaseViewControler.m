//
//  BoxBaseViewControler.m
//  TreasureBox
//
//  Created by Chen on 16/2/3.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "BoxBaseViewControler.h"
#import "BoxNavigationBar.h"

@interface BoxBaseViewControler ()<BoxNavigationBarDelegate>

@end

@implementation BoxBaseViewControler
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:self.boxNavigationBar];
    self.navigationController.navigationBarHidden = YES;
    self.boxNavigationBar.midLabel.text = self.tabBarItem.title;
    self.boxNavigationBar.midLabel.textColor = [UIColor whiteColor];
    [self creatContentView];
    
//    [self removeDefultNavigationBar];

    
    
}


-(void)removeDefultNavigationBar{
  
   
}




-(void)creatContentView{
    self.boxContentView = [UIView new];
    [self.view addSubview:self.boxContentView];
    self.boxContentView.sd_layout.leftEqualToView(self.view).rightEqualToView(self.view).topSpaceToView(self.boxNavigationBar,0).bottomSpaceToView(self.tabBarController.tabBar,0);
    
//    self.boxContentView.layer.borderColor = [UIColor blueColor].CGColor;
//    self.boxContentView.layer.borderWidth = 2;
    
//    self.boxContentView.backgroundColor = UIColorWithString(0xf4f4f4);
    
    

    
//    ;NSLog(@"底部%f",self.tabBarController.tabBar.frame.size.height)
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setBoxNavigationBarHiden:(BOOL)boxNavigationBarHiden{
    if (boxNavigationBarHiden) {
        self.boxNavigationBar.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20);
    }else{
        self.boxNavigationBar.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64);
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BoxNavigationBar *)boxNavigationBar{
    if (_boxNavigationBar==nil) {
        _boxNavigationBar = [[BoxNavigationBar alloc]initWithDelegate:self];
        _boxNavigationBar.backgroundColor = NavigationBarColor;
//        for (UIView *subView in [self.navigationController.navigationBar subviews]) {
////            NSLog(@"subviewframe %f ",subView.frame.size.height);
////            if ([subView isKindOfClass:NSClassFromString(@"_UINavigationBarBackground")]) {
////                [subView addSubview:_boxNavigationBar];
////            }else{
////               [subView removeFromSuperview];
////            }
//            
//        }
        
        
        
        
        
        [self.view addSubview:_boxNavigationBar];
        


    }
    return _boxNavigationBar;
}


-(void)test{
    
    UIView *vie1 =[UIView new];
    [self.boxContentView addSubview:vie1];
    
    
    vie1.frame = CGRectMake(0, 0, 100, 100);
    
    
    UIView *view2 = [UIView new];
    [self.boxContentView addSubview:view2];
    view2.sd_layout.topSpaceToView(vie1,5).leftEqualToView(self.boxContentView).heightIs(50).widthIs(50);
    
    
    vie1.backgroundColor = [UIColor orangeColor];
    view2.backgroundColor = [UIColor purpleColor];
    
    
    UIView *v3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    [view2 addSubview:v3];
    v3.backgroundColor = [UIColor blackColor];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        vie1.frame = CGRectMake(vie1.frame.origin.x, vie1.frame.origin.y
                                , vie1.frame.size.width, 30);
    });
}


-(void)didClickLeftBtn:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
