//
//  BoxBaseViewControler.h
//  TreasureBox
//
//  Created by Chen on 16/2/3.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoxNavigationBar.h"
@interface BoxBaseViewControler : UIViewController

@property(nonatomic,strong) BoxNavigationBar *boxNavigationBar;
@property(nonatomic,strong) UIViewController *tapRootViewControler;
@property(nonatomic,assign) BOOL boxNavigationBarHiden;
@property(nonatomic,strong) UIView *boxContentView; //!<除了导航条和底部tabbar 内容视图放在里边
@property(nonatomic,strong) UIView *boxEmptyView; //!< 加载数据时候应该显示的视图
@property(nonatomic,copy) id reciveObject;


-(void)test;
@end

