//
//  XMPPUtil.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/3.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "XMPPUtil.h"
#import "XMPPJID.h"
#import "XMPPPresence.h"
#import "UIAlertView+BlocksKit.h"
#import "XMPPvCardTemp.h"
#import "XMPPvCardTempModule.h"
#import "XMPPRoomMemoryStorage.h"
#define XMPPHostName @"127.0.0.1"
#define XMPPHostPort 5222
#define XMPPDomin @"127.0.0.1"
#define XMPPResource @"IOS"
@implementation XMPPUtil
+(XMPPUtil *)defultUtil{
    static dispatch_once_t onceToken;
    static XMPPUtil *util = nil;
    dispatch_once(&onceToken, ^{
        util = [[XMPPUtil alloc]init];
    });
    return util;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.xmppStream = [[XMPPStream alloc]init];
        self.xmppStream.hostName = XMPPHostName;
        self.xmppStream.hostPort = XMPPHostPort;
        [self.xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
        
        
        
        self.xmppRosterDataStorage = [[XMPPRosterCoreDataStorage alloc] init];
        self.xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:_xmppRosterDataStorage];
        [self.xmppRoster activate:self.xmppStream];
        [self.xmppRoster addDelegate:self delegateQueue:dispatch_get_main_queue()];
         [self.xmppRoster setAutoFetchRoster:YES];
        self.xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = YES;

        
         self.recon = [[XMPPReconnect alloc]init];
        [self.recon activate:self.xmppStream];
        [self.recon setAutoReconnect:YES];
        
        
        //1.消息存储对象
        self.msgStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        self.msgModule = [[XMPPMessageArchiving alloc] initWithMessageArchivingStorage:self.msgStorage];
        [self.msgModule activate:self.xmppStream];
        [self.msgModule addDelegate:self delegateQueue:dispatch_get_main_queue()];
        
        
        
        
   
               
       
        
        presenceStates = @[
                           @"available",
                           @"away",
                           @"do not disturb",
                           @"unavailable"
                           ];
    }
    return self;
}

-(void)login{
    self.loginState = YES;
    [self connrctToServer];
}

-(void)regiest{
    self.loginState = NO;
    [self connrctToServer];
}

-(void)connrctToServer{
    XMPPJID *jid = [XMPPJID jidWithUser:self.name domain:XMPPDomin resource:XMPPResource];
    self.xmppStream.myJID = jid;
    
    if ([self.xmppStream isConnected]|| [self.xmppStream isConnecting]) {
        XMPPPresence *pre = [XMPPPresence presenceWithType:@"unavailable"];
        [self.xmppStream sendElement:pre];
        [self.xmppStream disconnect];
    }
    
    NSError *error = nil;
    
    [self.xmppStream connectWithTimeout:-1 error:&error];
    
    if (error != nil) {
        NSLog(@"链接失败  %@",error);
    }
    
}






#pragma mark -XMPPDelegate 实现
-(void)xmppStreamConnectDidTimeout:(XMPPStream *)sender{
    NSLog(@"超时");
}
- (void)xmppStreamDidConnect:(XMPPStream *)sender{
    NSLog(@"链接成功");
    NSError *error = nil;
    
    
    if (self.loginState) {
        [self.xmppStream authenticateWithPassword:self.pwd error:&error];
    }else{
        [self.xmppStream registerWithPassword:self.pwd error:&error];
    }
    
    if (error != nil) {
        NSLog(@"denglu 失败  %@",error);
    }
    
}

/**
 *  验证登录成功
 *
 *  @param sender
 */
-(void)xmppStreamDidAuthenticate:(XMPPStream *)sender{
    NSLog(@"登录成功");
    UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:@"登录结果" message:@"成功"];
    [alert bk_addButtonWithTitle:@"确定" handler:^{
        
    }];
    [alert show];
    XMPPPresence *pre = [XMPPPresence presenceWithType:@"available"];
    [self.xmppStream sendElement:pre];
}
/**
 *  登录失败
 *
 *  @param sender
 *  @param error
 */
-(void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(DDXMLElement *)error{
    NSLog(@"登录失败");
    
    NSLog(@"didNotAuthenticate :密码校验失败，登录不成功,原因是：%@", [error XMLString]);
    UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:@"登录结果" message:@"失败"];
    [alert bk_addButtonWithTitle:@"确定" handler:^{
        
    }];
    [alert show];
    
};

/**
 *  注册成功
 *
 *  @param sender
 */
-(void)xmppStreamDidRegister:(XMPPStream *)sender{
    UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:@"注册结果" message:@"成功"];
    [alert bk_addButtonWithTitle:@"确定" handler:^{
        
    }];
    [alert show];
}

/**
 *  注册失败
 *
 *  @param sender
 *  @param error
 */
-(void)xmppStream:(XMPPStream *)sender didNotRegister:(DDXMLElement *)error{
    UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:@"注册结果" message:@"失败"];
    [alert bk_addButtonWithTitle:@"确定" handler:^{
        
    }];
    [alert show];
}

/**
 *  更改用户状态回调
 *
 *  @param sender
 *  @param presence
 */
-(void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence{
    NSString *presenceType = [presence type];
    NSString *presenceFromUser = [[presence from] user];
    if (![presenceFromUser isEqualToString:[[sender myJID] user]]) {
        if ([presenceType isEqualToString:@"available"]) {
            //
            NSLog(@"已上线");
        } else if ([presenceType isEqualToString:@"unavailable"]) {
            //
            NSLog(@"已下线");
        }
    }
}

/**
 *  接收到消息
 *
 *  @param sender
 *  @param message
 */
-(void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message{
    NSString *messageBody = [[message elementForName:@"body"] stringValue];
    NSLog(@"收到的消息为 :%@",messageBody);
    
}

//获取到一个好友节点
-(void)xmppRoster:(XMPPRoster *)sender didReceiveRosterItem:(DDXMLElement *)item{
    
}

//接收好友请求
-(void)xmppRoster:(XMPPRoster *)sender didReceivePresenceSubscriptionRequest:(XMPPPresence *)presence{
    
    UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:@"提示:有人添加你" message:presence.from.user];
    
    [alert bk_addButtonWithTitle:@"接受" handler:^{
        [self.xmppRoster acceptPresenceSubscriptionRequestFrom:presence.from andAddToRoster:YES];
        
    }];
    [alert bk_addButtonWithTitle:@"拒绝" handler:^{
        [self.xmppRoster rejectPresenceSubscriptionRequestFrom:presence.from];
    }];
    
    
    [alert show];
}


-(void)xmppRosterDidBeginPopulating:(XMPPRoster *)sender{
    NSLog(@"开始检索好友列表");
}
// 正在检索好友列表的方法
-(void)xmppRoster:(XMPPRoster *)sender didRecieveRosterItem:(DDXMLElement *)item{
    NSLog(@"每一个好友都会走一次这个方法");
    //获得item的属性里的jid字符串，再通过它获得jid对象
    NSString *jidStr = [[item attributeForName:@"jid"] stringValue];
    XMPPJID *jid = [XMPPJID jidWithString:jidStr];
    
    
    UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:@"查询到好友" message:jid.user];
    [alert bk_addButtonWithTitle:@"确定" handler:^{
        
    }];
    
    [alert show];
//    //是否已经添加
//    if ([self.rosterJids containsObject:jid]) {
//        return;
//    }
//    //将好友添加到数组中去
//    [self.rosterJids addObject:jid];
//    //添加完数据要更新UI（表视图更新）
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.rosterJids.count-1 inSection:0];
//    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}
//获取完好友列表
- (void)xmppRosterDidEndPopulating:(XMPPRoster *)sender{
      NSLog(@"好友列表检索完毕"); 
}
////到服务器上请求联系人名片信息
//- (void)fetchvCardTempForJID:(XMPPJID *)jid{
//
//}
////请求联系人的名片，如果数据库有就不请求，没有就发送名片请求
//- (void)fetchvCardTempForJID:(XMPPJID *)jid ignoreStorage:(BOOL)ignoreStorage{
//
//}
////获取联系人的名片，如果数据库有就返回，没有返回空，并到服务器上抓取
//- (XMPPvCardTemp *)vCardTempForJID:(XMPPJID *)jid shouldFetch:(BOOL)shouldFetch{
//
//}
////更新自己的名片信息
//- (void)updateMyvCardTemp:(XMPPvCardTemp *)vCardTemp{
//
//}
////获取到一盒联系人的名片信息的回调
//- (void)xmppvCardTempModule:(XMPPvCardTempModule *)vCardTempModule
//        didReceivevCardTemp:(XMPPvCardTemp *)vCardTemp
//                     forJID:(XMPPJID *)jid{
//
//}


- (void)xmppRoomDidCreate:(XMPPRoom *)sender
{
    //永久新保存
    NSXMLElement *field = [NSXMLElement elementWithName:@"field"];
    [field addAttributeWithName:@"type"stringValue:@"boolean"];
    [field addAttributeWithName:@"var"stringValue:@"muc#roomconfig_persistentroom"];
    [field addChild:[NSXMLElement elementWithName:@"value"objectValue:@"1"]];  // 将持久属性置为YES。
    NSXMLElement *x = [NSXMLElement elementWithName:@"x"xmlns:@"jabber:x:data"];
    [x addAttributeWithName:@"type"stringValue:@"form"];
    [x addChild:field];
    [sender configureRoomUsingOptions:x];
    NSLog(@"成功创建聊天室");
}

- (void)xmppRoomDidJoin:(XMPPRoom *)sender
{
    NSLog(@"成功加入房间");
    [sender fetchConfigurationForm];
    [sender fetchBanList];
    [sender fetchMembersList];
    [sender fetchModeratorsList];
}


#pragma mark - 如果房间存在，会调用委托

// 收到禁止名单列表
- (void)xmppRoom:(XMPPRoom *)sender didFetchBanList:(NSArray *)items{
    NSLog(@"收到禁止名单列表");
}
// 收到好友名单列表
- (void)xmppRoom:(XMPPRoom *)sender didFetchMembersList:(NSArray *)items{
    NSLog(@"收到好友名单列表");
}
// 收到主持人名单列表
- (void)xmppRoom:(XMPPRoom *)sender didFetchModeratorsList:(NSArray *)items{
    NSLog(@"收到主持人名单列表");
}


//房间不存在，调用委托

-(void)xmppRoom:(XMPPRoom*)sender didNotFetchBanList:(XMPPIQ*)iqError{
    
}
-(void)xmppRoom:(XMPPRoom*)sender didNotFetchMembersList:(XMPPIQ*)iqError{
    
}
-(void)xmppRoom:(XMPPRoom*)sender didNotFetchModeratorsList:(XMPPIQ*)iqError{
    
}
//离开聊天室
-(void)xmppRoomDidLeave:(XMPPRoom *)sender{
    NSLog(@"离开聊天室");
}

-(void)xmppRoom:(XMPPRoom *)sender occupantDidJoin:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence{
    NSLog(@"新人加入群聊");
}

-(void)xmppRoom:(XMPPRoom *)sender occupantDidLeave:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence{
    NSLog(@"有人退出群聊");
}

- (void)xmppRoom:(XMPPRoom *)sender didReceiveMessage:(XMPPMessage *)message fromOccupant:(XMPPJID *)occupantJID
{
    NSLog(@"有人在群里发言");
}

-(BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq{
    NSMutableArray *array = [NSMutableArray array];
    for (DDXMLElement *element in iq.children) {
        if ([element.name isEqualToString:@"query"]) {
            for (DDXMLElement *item in element.children) {
                if ([item.name isEqualToString:@"item"]) {
                    [array addObject:item.attributes];          //array  就是你的群列表
                    
                }
            }
        }
    }
    
    for (NSArray *data  in array){
        NSString* jid = [[data objectAtIndex:0] stringValue];
        NSString * name  = [[data objectAtIndex:1] stringValue];
        NSLog(@"dd");

    }
    
    return YES;
}


-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    NSManagedObject *obj = anObject;
    if([obj isKindOfClass:[XMPPMessageArchiving_Message_CoreDataObject class]])
    {
        NSLog(@"聊天的信息的数据库发生变化");
    }    else
        NSLog(@"数据库有变化");
}



#pragma mark - 用户方法

-(void)setPresenceState:(PresenceState)presenceState{
    _presenceState = presenceState;
    XMPPPresence *pre = [XMPPPresence presenceWithType:presenceStates[presenceState]];
    [self.xmppStream sendElement:pre];
}


-(void)addFriend:(NSString *)name{
    XMPPJID *jid = [XMPPJID jidWithUser:name domain:XMPPDomin resource:XMPPResource];
    [self.xmppRoster subscribePresenceToUser:jid];
    
}

-(void)removeFriend:(NSString *)name{
    XMPPJID *jid = [XMPPJID jidWithString:[NSString stringWithFormat:@"%@@%@",name,XMPPDomin]];
    
    [self.xmppRoster removeUser:jid];

}

- (void)sendMessage:(NSString *)msg toUser:(NSString *)name {
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:msg];
    
    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    NSString *to = [NSString stringWithFormat:@"%@@%@", name,XMPPDomin];
    [message addAttributeWithName:@"to" stringValue:to];
    [message addChild:body];
    [self.xmppStream sendElement:message];
}
/**
 *  获取好友列表
 */
-(void)fetchFriends{
    NSManagedObjectContext *context = self.xmppRosterDataStorage.mainThreadManagedObjectContext;
    NSFetchRequest *request =  [[NSFetchRequest alloc] initWithEntityName:@"XMPPUserCoreDataStorageObject"];
    
    NSString *userinfo = [NSString stringWithFormat:@"%@@%@",self.name,XMPPDomin];
    NSLog(@"本人:userinfo = %@",userinfo);
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:@"streamBareJidStr = %@",userinfo];
    
    request.predicate = predicate;
    
    //排序
    NSSortDescriptor * sort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES];
    request.sortDescriptors = @[sort];
    
    self.fetFriend = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    self.fetFriend.delegate = self;
    NSError *error;
    [self.fetFriend performFetch:&error];
    
    //返回的数组是XMPPUserCoreDataStorageObject  *obj类型的
    //名称为 obj.displayName
    
    
    for (XMPPUserCoreDataStorageObject *item in self.fetFriend.fetchedObjects) {
        
        NSLog(@"我的朋友有 :%@",item.jid.user);
    }
    
    
    //    return  self.fetFriend.fetchedObjects;
    
    


}


-(void)makeRoom:(NSString *)roomName{
    NSString *jidRoom = [NSString stringWithFormat:@"%@@conference.%@",roomName,XMPPDomin];
    XMPPJID *jid = [XMPPJID jidWithString:jidRoom];
    
    XMPPRoomCoreDataStorage *roomstorage = [[XMPPRoomCoreDataStorage alloc] initWithInMemoryStore];
    XMPPRoom *room = [[XMPPRoom alloc] initWithRoomStorage:roomstorage jid:jid dispatchQueue:dispatch_queue_create("com.我的工程.RoomQueue",NULL)];
    
    XMPPStream *stream = [self xmppStream];
    [room activate:stream];
    
  
    
    [room addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [room joinRoomUsingNickname:@"dd" history:nil];

}

-(void)joinRoom:(NSString *)nickName{
//    [self.xmppRoom joinRoomUsingNickname:nickName history:nil];
}

-(void)leaveRoom{
//     [self.xmppRoom deactivate];
}

-(void)fetchLocalFriends{
}


- (void)getExistRoom   
{
    
    NSXMLElement *queryElement= [NSXMLElement elementWithName:@"query" xmlns:@"http://jabber.org/protocol/disco#items"];
    NSXMLElement *iqElement = [NSXMLElement elementWithName:@"iq"];
    [iqElement addAttributeWithName:@"type" stringValue:@"get"];
    [iqElement addAttributeWithName:@"from" stringValue:[NSString stringWithFormat:@"%@@%@",self.name,XMPPDomin]];
    [iqElement addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"conference.%@",XMPPDomin]];
    [iqElement addAttributeWithName:@"id" stringValue:@"getexistroomid"];
    [iqElement addChild:queryElement];
    [self.xmppStream sendElement:iqElement];
}


//获得与某个好友的聊天记录
-(NSArray*)getRecords:(NSString*)friendsName
{
    //所有账号 和所有人的聊天记录都在同一个数据库内  所以 要写查询条件
    NSManagedObjectContext *context = self.msgStorage.mainThreadManagedObjectContext;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"XMPPMessageArchiving_Message_CoreDataObject"];
    NSString *userinfo = [NSString stringWithFormat:@"%@@%@",self.name,XMPPDomin];
    NSString *friendinfo = [NSString stringWithFormat:@"%@@%@",friendsName,XMPPDomin];
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:@"streamBareJidStr = %@ and bareJidStr = %@",userinfo,friendinfo];
    request.predicate = predicate;
    NSSortDescriptor * sort = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES];
    request.sortDescriptors = @[sort];
    self.fetMsgRecord = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    self.fetMsgRecord.delegate = self;
    NSError *error;
    [self.fetMsgRecord performFetch:&error];
    //   返回的值类型 XMPPMessageArchiving_Message_CoreDataObject
    return self.fetMsgRecord.fetchedObjects;
}

@end
