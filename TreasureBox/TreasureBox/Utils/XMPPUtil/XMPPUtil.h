//
//  XMPPUtil.h
//  TreasureBox
//
//  Created by chenguibang on 16/7/3.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"
#import "XMPPStream.h"
#import "XMPP.h"
#import "XMPPRoster.h"
#import "XMPPRosterCoreDataStorage.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "XMPPReconnect.h"
#import "XMPPRosterMemoryStorage.h"
#import "XMPPRoom.h"
#import "XMPPRoomCoreDataStorage.h"


typedef enum : NSUInteger {
    PresenceStateAvailable = 0, //!< 上线
    PresenceStateAway, //!< 离开
    PresenceStateBusy, //!< 忙碌
    PresenceStateunavailable, //!< 下线
} PresenceState;

@interface XMPPUtil : NSObject<XMPPStreamDelegate,XMPPRosterDelegate,NSFetchedResultsControllerDelegate,XMPPRoomDelegate>{
    NSArray *presenceStates;
}
@property(nonatomic,strong) XMPPStream *xmppStream;
@property(nonatomic,strong) XMPPRoster *xmppRoster;//花名册模块
//@property(nonatomic,strong) XMPPRoom *xmppRoom;
//@property(nonatomic,strong) XMPPRoomCoreDataStorage *xmppRoomCoreDataStorage;

@property (nonatomic , strong) XMPPRosterCoreDataStorage *xmppRosterDataStorage; //花名册存储
@property(nonatomic,retain)XMPPRosterMemoryStorage *xmppRosterMS;
@property(strong,nonatomic) XMPPMessageArchivingCoreDataStorage *msgStorage;//消息存储
@property(strong,nonatomic) XMPPMessageArchiving * msgModule;//消息模块
@property(strong,nonatomic) XMPPReconnect *recon;


@property(strong,nonatomic) NSFetchedResultsController *fetFriend;//查询好友的Fetch
@property(strong,nonatomic) NSFetchedResultsController *fetMsgRecord;//查询消息的Fetch

@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *pwd;
@property(nonatomic,assign) PresenceState presenceState; //!< 用户状态
@property(nonatomic,assign) BOOL loginState;//是否登录
+(XMPPUtil*)defultUtil;
-(void)login;
-(void)regiest;

/**
 *  添加好友
 */
-(void)addFriend:(NSString*)name;

/**
 *  删除好友
 *
 *  @param name 
 */
- (void)removeFriend:(NSString *)name;
/**
 *  发送消息
 *
 *  @param msg
 *  @param name 
 */
- (void)sendMessage:(NSString *)msg toUser:(NSString *)name;

/**
 *  获取好友列表
 */
-(void)fetchFriends;

/**
 *  创建聊天室
 */
-(void)makeRoom:(NSString*)roomName;

/**
 *  获得聊天室
 */
-(void)getExistRoom;

/**
 *  加入聊天室
 */
-(void)joinRoom:(NSString*)nickName;

/**
 *  离开房间
 */
-(void)leaveRoom;

/**
 *  获取聊天记录
 *
 *  @param friendsName
 *
 *  @return
 */
-(NSArray*)getRecords:(NSString*)friendsName;
@end
