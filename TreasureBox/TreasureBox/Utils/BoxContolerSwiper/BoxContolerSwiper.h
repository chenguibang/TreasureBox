//
//  BoxContolerSwiper.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/9.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BoxContolerSwiper : NSObject
+(BoxContolerSwiper*)defaultSwiper;

+(void)switchTo:(NSString*)vcEnd form:(id)vcStart object:(id)object;

+(void)presendTo:(NSString*)vcEnd form:(id)vcStart complete:(void(^)(void))block;
@end
