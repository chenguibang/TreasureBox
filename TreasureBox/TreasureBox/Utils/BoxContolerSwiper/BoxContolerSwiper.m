//
//  BoxContolerSwiper.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/9.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "BoxContolerSwiper.h"

@implementation BoxContolerSwiper

+(BoxContolerSwiper *)defaultSwiper{
    static BoxContolerSwiper *swiper;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        swiper = [[self alloc]init];
    });
    return swiper;
}
+(void)switchTo:(NSString *)vcEnd form:(id)vcStart object:(id)object{
    Class class  =   NSClassFromString(vcEnd);
    BoxBaseViewControler *endVC = (BoxBaseViewControler*)[[class alloc]init];
    endVC.reciveObject = object;
    UIViewController *startVC = (UIViewController*)vcStart;
    [startVC.navigationController pushViewController:endVC animated:YES];
}

+(void)presendTo:(NSString*)vcEnd form:(id)vcStart complete:(void (^)(void))block{
    Class class  =   NSClassFromString(vcEnd);
    UIViewController *endVC = (UIViewController*)[[class alloc]init];
    UIViewController *startVC = (UIViewController*)vcStart;
    [startVC presentViewController:endVC animated:YES completion:^{
        if (block) {
            block();
        }
    }];
}
@end
