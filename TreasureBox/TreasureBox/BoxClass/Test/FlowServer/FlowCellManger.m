//
//  FlowCellManger.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/22.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowCellManger.h"

@interface FlowCellManger ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableDictionary *dict;
}

@end

@implementation FlowCellManger

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
    dict = [[NSMutableDictionary alloc]init];
    [dict setObject:self.reciveObject[@"row"] forKey:@"row"];
    [dict setObject:self.reciveObject[@"section"] forKey:@"section"];
    self.boxNavigationBar.midLabel.text = @"cell管理";
//    NSDictionary *ste = (NSDictionary*)self.reciveObject;
    NSLog(@"收到  %@",self.reciveObject);
    self.mangerView.backgroundColor = UIColorWithRandom;
    self.mangerView.menuTable.backgroundColor = UIColorWithRandom;
    
    [self.mangerView.picView.imageNametf.rac_textSignal subscribeNext:^(id x) {
        
        self.mangerVM.picViewModel.picName = x;
        
    }];
    [RACObserve(self.mangerVM.picViewModel, picName) subscribeNext:^(NSString* x) {
        self.mangerView.picView.imageView.image = [UIImage imageNamed:x];
        [dict setObject:x forKey:@"picName"];
    }];
    [self.mangerView.headView.iconName.rac_textSignal subscribeNext:^(id x) {
        self.mangerVM.HeadViewModel.icon = x;
    }];

    
    
    [RACObserve(self.mangerVM.HeadViewModel, icon) subscribeNext:^(NSString* x) {
        self.mangerView.headView.imageView.image = [UIImage imageNamed:x];
        [dict setObject:x forKey:@"headicon"];
    }];
    
    
    
    [self.mangerView.headView.titleName.rac_textSignal subscribeNext:^(id x) {
        self.mangerVM.HeadViewModel.title = x;
        
    }];
    
    [RACObserve(self.mangerVM.HeadViewModel, title) subscribeNext:^(id x) {
        [dict setObject:x forKey:@"headtitle"];
    }];
    
    
    [self.mangerView.webView.urltf.rac_textSignal subscribeNext:^(id x) {
        self.mangerVM.WebViewModel.urlStr = x;
        
    }];
    
    [RACObserve(self.mangerVM.WebViewModel, urlStr) subscribeNext:^(id x) {
        [dict setObject:x forKey:@"urlStr"];
    }];
    
    
    
    [[self.mangerView.sureBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        NSLog(@"%@",dict);
         [self.navigationController popViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"addinfo" object:dict];
       
    }];
    
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(FlowCellMangerView *)mangerView{
    if (!_mangerView) {
        _mangerView = [FlowCellMangerView new];
        [self.view addSubview:_mangerView];
        _mangerView.sd_layout.leftEqualToView(self.boxContentView).topEqualToView(self.boxContentView).rightEqualToView(self.boxContentView).bottomEqualToView(self.boxContentView);
        _mangerView.menuTable.dataSource = self;
        _mangerView.menuTable.delegate = self;
    }
    return _mangerView;
}

-(FlowCellMangerVM *)mangerVM{
    if (!_mangerVM) {
        _mangerVM = [[FlowCellMangerVM alloc]init];
    }
    return _mangerVM;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = self.mangerVM.menuArr[indexPath.row];
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return  self.mangerVM.menuArr.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        [self.mangerView bringSubviewToFront:self.mangerView.picView];
    }else if (indexPath.row == 1){
         [self.mangerView bringSubviewToFront:self.mangerView.headView];
    }else if (indexPath.row == 2){
        [self.mangerView bringSubviewToFront:self.mangerView.webView];
    }
    
    [dict setObject:[NSNumber numberWithInt:(int)(indexPath.row+100)] forKey:@"itemtype"];
}
@end
