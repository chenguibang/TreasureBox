//
//  FlowCellMangerView.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/22.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowCellMangerView.h"

@implementation FlowCellMangerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.sureBtn.backgroundColor = UIColorWithRandom;
    }
    return self;
}

-(UITableView *)menuTable{
    if (!_menuTable) {
        _menuTable = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        [self addSubview:_menuTable];
        _menuTable.sd_layout.leftEqualToView(self).topEqualToView(self).bottomSpaceToView(self,44).widthIs(80);
        _menuTable.backgroundColor = UIColorWithRandom;
        
    }
    return _menuTable;
}

-(UIView *)picView{
    if (!_picView) {
        _picView = [FlowCellMPicView new];
        [self addSubview:_picView];
        _picView.sd_layout.leftSpaceToView(self.menuTable,0).topEqualToView(self).rightEqualToView(self).bottomSpaceToView(self,44);
    }
    return _picView;
}

-(FlowCellMHeadView *)headView{
    if (!_headView) {
        _headView = [FlowCellMHeadView new];
        [self addSubview:_headView];
        _headView.sd_layout.leftSpaceToView(self.menuTable,0).topEqualToView(self).rightEqualToView(self).bottomSpaceToView(self,44);
    }
    return _headView;
}
-(FlowCellMWebView *)webView{
    if (!_webView) {
        _webView = [FlowCellMWebView new];
        [self addSubview:_webView];
        _webView.sd_layout.leftSpaceToView(self.menuTable,0).topEqualToView(self).rightEqualToView(self).bottomSpaceToView(self,44);
    }
    return _webView;
}
-(UIButton *)sureBtn{
    if (!_sureBtn) {
        _sureBtn = [UIButton new];
        [self addSubview:_sureBtn];
        _sureBtn.sd_layout.leftEqualToView(self).bottomEqualToView(self).heightIs(40).widthEqualToHeight();
        [_sureBtn setTitle:@"sure" forState:UIControlStateNormal];
    }
    return _sureBtn;
}
@end
