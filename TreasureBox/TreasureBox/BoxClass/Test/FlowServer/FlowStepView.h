//
//  FlowStepView.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/20.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlowStepView : UIView{
    CGFloat temp;
}
@property(nonatomic,retain)UILabel *title;
@property(nonatomic,retain) UITextField *value;
@property(nonatomic,retain) UIStepper *steper;
@end
