
//
//  FlowCellMPicView.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/23.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowCellMPicView.h"

@implementation FlowCellMPicView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.imageNametf = [[UITextField alloc]init];
        [self addSubview:self.imageNametf];
        self.imageNametf.sd_layout.leftEqualToView(self).topEqualToView(self).rightEqualToView(self).heightIs(44);
        self.imageNametf.backgroundColor = [UIColor grayColor];
        self.imageNametf.placeholder = @"请输入图片名";
        
        
        self.imageView = [[UIImageView alloc]init];
        [self addSubview:self.imageView];
        self.imageView.sd_layout.leftEqualToView(self.imageNametf).topSpaceToView(self.imageNametf,0).rightEqualToView(self.imageNametf).heightEqualToWidth();
        self.imageView.backgroundColor = [UIColor whiteColor];
        
    }
    return self;
}

@end
