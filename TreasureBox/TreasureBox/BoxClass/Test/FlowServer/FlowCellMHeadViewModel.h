//
//  FlowCellMHeadViewModel.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/23.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlowCellMHeadViewModel : NSObject
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *icon;
@end
