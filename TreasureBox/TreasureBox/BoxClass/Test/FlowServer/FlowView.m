//
//  FlowView.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/19.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowView.h"

@implementation FlowView

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.touchDown = YES;
    //保存触摸起始点位置
    CGPoint point = [[touches anyObject] locationInView:self];
    startPoint = point;
    
    //该view置于最前
    [[self superview] bringSubviewToFront:self];
}

-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    //计算位移=当前位置-起始位置
    CGPoint point = [[touches anyObject] locationInView:self];
    float dx = point.x - startPoint.x;
    float dy = point.y - startPoint.y;
    
    //计算移动后的view中心点
    CGPoint newcenter = CGPointMake(self.center.x + dx, self.center.y + dy);
    
    
    
    float halfx = CGRectGetMidX(self.bounds);
    //x坐标左边界
    newcenter.x = MAX(halfx, newcenter.x);
    //x坐标右边界
    newcenter.x = MIN(self.superview.bounds.size.width - halfx, newcenter.x);
    
    //y坐标同理
    float halfy = CGRectGetMidY(self.bounds);
    newcenter.y = MAX(halfy, newcenter.y);
    newcenter.y = MIN(self.superview.bounds.size.height - halfy, newcenter.y);
    //移动view
    self.center = newcenter;
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    self.touchDown = NO;
}
-(UIView *)verLine{
    if (!_verLine) {
        _verLine = [[UIView alloc]init];
        _verLine.frame = CGRectMake(0, 0, 1, self.height - 6);
        _verLine.center = CGPointMake(self.width/2, self.height/2);
        _verLine.backgroundColor =[UIColor whiteColor];
        [self addSubview:_verLine];
    }
    return _verLine;
    
}
-(UITextView *)infoTv{
    if (!_infoTv) {
        _infoTv  = [[UITextView alloc]initWithFrame:self.bounds];
        [self addSubview:_infoTv];
        _infoTv.font = [UIFont systemFontOfSize:5];
        _infoTv.backgroundColor = [UIColor clearColor];
        _infoTv.userInteractionEnabled = NO;
        
        
        
    }
    return _infoTv;
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
     self.infoTv.text = [NSString stringWithFormat:@"x:%f  y:%f \n width:%f  height:%f \n centerX:%f centerY:%f",self.frame.origin.x,self.frame.origin.y,self.width,self.height,self.center.x,self.center.y];
}

-(void)setCenter:(CGPoint)center{
    [super setCenter:center];
        self.infoTv.text = [NSString stringWithFormat:@"x:%f  y:%f \n width:%f  height:%f \n centerX:%f centerY:%f",self.frame.origin.x,self.frame.origin.y,self.width,self.height,self.center.x,self.center.y];
}

-(void)setOnChoose:(BOOL)onChoose{
    if (onChoose) {
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(1, 1);
        self.layer.shadowOpacity = 1;
    }else{
        self.layer.shadowColor = [UIColor clearColor].CGColor;

        self.layer.shadowOpacity = 0.0;
        self.layer.shadowOffset = CGSizeMake(0, 0);
    } 
}
@end
