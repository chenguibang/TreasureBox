//
//  FlowCellManger.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/22.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlowCellMangerView.h"
#import "FlowCellMangerVM.h"

@interface FlowCellManger : BoxBaseViewControler
@property(nonatomic,strong)FlowCellMangerView *mangerView;
@property(nonatomic,strong)FlowCellMangerVM *mangerVM;
@end
