//
//  FlowView.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/19.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlowView : UIView{
    CGPoint startPoint;
}
@property(nonatomic,strong)UIView *verLine;
@property(nonatomic,assign)BOOL touchDown;
@property(nonatomic,strong)UITextView *infoTv;
@property(nonatomic,assign)BOOL onChoose;


@end
