//
//  FlowCellMPicViewModel.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/23.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlowCellMPicViewModel : NSObject
@property(nonatomic,copy)NSString *picName;
@end
