//
//  FlowCellMPicView.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/23.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlowCellMPicView : UIView
@property(nonatomic,strong) UITextField *imageNametf;
@property(nonatomic,strong) UIImageView *imageView;
@end
