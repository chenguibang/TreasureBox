//
//  FlowServer.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/19.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowServer.h"
#import "FlowStepView.h"
#import "FlowCollectionVC.h"

@interface FlowServer (){
    NSMutableDictionary *sectionDict;
}
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGestureRecognizer;
@property(nonatomic,strong) UIPanGestureRecognizer *panGestureRecognizer;
@property(nonatomic,strong) FlowView *currentView;
@end

@implementation FlowServer

- (void)viewDidLoad {
    [super viewDidLoad];
    FlowStepView *stap = [[FlowStepView alloc]initWithFrame:CGRectMake(0, self.view.height-170, self.view.width, 30)];
    stap.title.text = @"x";
    [[stap.value rac_textSignal]subscribeNext:^(NSNumber* x) {
        CGRect tem = self.currentView.frame;
        tem.origin.x = x.floatValue;
        self.currentView.frame =tem;
    }];
    
     [RACObserve(stap.value, text) subscribeNext:^(NSNumber* x) {
                 CGRect tem = self.currentView.frame;
                 tem.origin.x = x.floatValue;
         
                 self.currentView.frame =tem;
     }];
    stap.backgroundColor = UIColorWithRandom;
    [self.view addSubview:stap];
    
    
    FlowStepView *stap22 = [[FlowStepView alloc]initWithFrame:CGRectMake(0, self.view.height-140, self.view.width, 30)];
    stap22.title.text = @"y";
    [[stap22.value rac_textSignal]subscribeNext:^(NSNumber* x) {
        
        CGRect tem = self.currentView.frame;
        tem.origin.y = x.floatValue;
        
        self.currentView.frame =tem;
        
    }];
    
    [RACObserve(stap22.value, text) subscribeNext:^(NSNumber* x) {
        CGRect tem = self.currentView.frame;
        tem.origin.y = x.floatValue;
        
        self.currentView.frame =tem;
    }];
    stap22.backgroundColor = UIColorWithRandom;
    [self.view addSubview:stap22];
    
    
    
    FlowStepView *stap33 = [[FlowStepView alloc]initWithFrame:CGRectMake(0, self.view.height-110, self.view.width, 30)];
    stap33.title.text = @"width";
    [[stap33.value rac_textSignal]subscribeNext:^(NSNumber* x) {
        
        CGRect tem = self.currentView.frame;
        tem.size.width = x.floatValue;
        
        self.currentView.frame =tem;
        
    }];
    
    [RACObserve(stap33.value, text) subscribeNext:^(NSNumber* x) {
        CGRect tem = self.currentView.frame;
        tem.size.width = x.floatValue;
        
        self.currentView.frame =tem;
    }];
    stap33.backgroundColor = UIColorWithRandom;
    [self.view addSubview:stap33];
    
    
    FlowStepView *staph = [[FlowStepView alloc]initWithFrame:CGRectMake(0, self.view.height-80, self.view.width, 30)];
    staph.title.text = @"hei";
    [[staph.value rac_textSignal]subscribeNext:^(NSNumber* x) {
        
        CGRect tem = self.currentView.frame;
        tem.size.height = x.floatValue;
        
        self.currentView.frame =tem;
        
    }];
    
    [RACObserve(staph.value, text) subscribeNext:^(NSNumber* x) {
        CGRect tem = self.currentView.frame;
        tem.size.height = x.floatValue;
        
        self.currentView.frame =tem;
    }];
    staph.backgroundColor = UIColorWithRandom;
    [self.view addSubview:staph];
    
    
    
    // Do any additional setup after loading the view.
    self.bgView.layer.borderWidth = 1;
    self.bgView.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panwi:)];
    self.fd_interactivePopDisabled = YES;
    [RACObserve(self.bgView, touchDown) subscribeNext:^(NSNumber* x) {
        if (x.boolValue) {
            self.currentView.onChoose = NO;
            self.currentView =self.bgView;
            self.currentView.onChoose = YES;
        }else{
            //            currentView = nil;
        }
    }];
    UIButton *addbtn = [[UIButton alloc]initWithFrame:CGRectMake(0, self.view.height-50, 50, 50)];
    addbtn.backgroundColor = UIColorWithRandom;
    [self.view addSubview:addbtn];
    [addbtn setTitle:@"add" forState:UIControlStateNormal];
    [[addbtn rac_signalForControlEvents:UIControlEventTouchDown]subscribeNext:^(id x) {
        //综合排序
        [self.bgView addSubview:[self creatView]];
    }];
    
    UIButton *deldebtn = [[UIButton alloc]initWithFrame:CGRectMake(50, self.view.height-50, 50, 50)];
    deldebtn.backgroundColor = UIColorWithRandom;
    [self.view addSubview:deldebtn];
    [deldebtn setTitle:@"dele" forState:UIControlStateNormal];
    [[deldebtn rac_signalForControlEvents:UIControlEventTouchDown]subscribeNext:^(id x) {
        [self.currentView removeFromSuperview];
        self.currentView  = nil;
    }];
    
    
    UIButton *savebtn = [[UIButton alloc]initWithFrame:CGRectMake(100, self.view.height-50, 50, 50)];
    savebtn.backgroundColor = UIColorWithRandom;
    [self.view addSubview:savebtn];
    [savebtn setTitle:@"save" forState:UIControlStateNormal];
    [[savebtn rac_signalForControlEvents:UIControlEventTouchDown]subscribeNext:^(id x) {
        //遍历VIew   生成数组
        
      NSArray *aaa =  [self.bgView subviews];
        
        NSMutableDictionary *dcit = [[NSMutableDictionary alloc]init];
        
        CGFloat sectionScale = self.bgView.frame.size.height/self.bgView.frame.size.width;
        [dcit setObject:[NSNumber numberWithFloat:sectionScale] forKey:@"scale"];
        NSMutableArray *items = [[NSMutableArray alloc]init];
        for (FlowView *view in [self.bgView subviews]) {
            if (view.width ==0 || view.height ==0) {
                continue;
            }
            
            
            
            NSDictionary *itemFrame = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithFloat:(view.frame.origin.x/self.bgView.frame.size.width)],@"xpoint",
                                      [NSNumber numberWithFloat:(view.frame.origin.y/self.bgView.frame.size.height)],@"ypoint",
                                      [NSNumber numberWithFloat:(view.frame.size.height/self.bgView.frame.size.height)],@"heightscale",
                                      [NSNumber numberWithFloat:(view.frame.size.width/self.bgView.frame.size.width)],@"widthscale",
                                      [NSNumber numberWithFloat:1],@"top",
                                      [NSNumber numberWithFloat:1],@"left",
                                      [NSNumber numberWithFloat:1],@"bottom",
                                      [NSNumber numberWithFloat:1],@"right",
                                       nil];
            
            NSMutableDictionary *itemdata = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1],@"itemtype", nil];
            
         
            NSMutableDictionary *iteminfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:itemFrame,@"itemFrame",itemdata,@"itemdata",nil];
            [items addObject:iteminfo];
        }
        
        [dcit setObject:items forKey:@"items"];
        NSLog(@"%@",dcit);
        NSArray *arrss = [NSArray arrayWithObjects:dcit, nil];
        FlowCollectionVC *fle = [[FlowCollectionVC alloc]init];
//        fle.sectionArr = arrss;
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"aa" object:arrss];
//        [self.navigationController pushViewController:fle animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    
    UIButton *retrunbtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.width-50  , self.view.height-50, 50, 50)];
    retrunbtn.backgroundColor = UIColorWithRandom;
    [self.view addSubview:retrunbtn];
    [retrunbtn setTitle:@"retrun" forState:UIControlStateNormal];
    [[retrunbtn rac_signalForControlEvents:UIControlEventTouchDown]subscribeNext:^(id x) {
        //遍历VIew   生成数组
       
        [self.navigationController popViewControllerAnimated:YES];
        
       
    }];
    

    [RACObserve(self,currentView) subscribeNext:^(NSNumber* x) {
        [RACObserve(self.currentView,center) subscribeNext:^(NSNumber* x) {
            stap.value.text = [NSString stringWithFormat:@"%f",self.currentView.frame.origin.x];
            stap22.value.text = [NSString stringWithFormat:@"%f",self.currentView.frame.origin.y];
            stap33.value.text = [NSString stringWithFormat:@"%f",self.currentView.frame.size.width];
            staph.value.text = [NSString stringWithFormat:@"%f",self.currentView.frame.size.height];
        }];
    }];
    
    
}

-(void)panwi:(UIPanGestureRecognizer*)paramSender{
    
   
    if (paramSender.state == UIGestureRecognizerStateBegan) {
        
    }
    if (paramSender.state == UIGestureRecognizerStateEnded) {
        
    }
    
    if (paramSender.state != UIGestureRecognizerStateEnded && paramSender.state != UIGestureRecognizerStateFailed){
        //通过使用 locationInView 这个方法,来获取到手势的坐标
        CGPoint location = [paramSender locationInView:paramSender.view.superview];
        self.bgView.center = location;
    }

    
    
     NSLog(@"hua");
}
- (void)handleSwipes:(UISwipeGestureRecognizer *)sender
{
    

    
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"zuo");
        [UIView animateWithDuration:0.2 animations:^{
            CGPoint piont = self.currentView.center;
            piont.x -= 0.1;
            self.currentView.center = piont;
        }];
        
    }
    if (sender.direction == UISwipeGestureRecognizerDirectionRight) {
        [UIView animateWithDuration:0.2 animations:^{
            CGPoint piont = self.currentView.center;
            piont.x += 0.1;
           self.currentView.center = piont;
        }];
        NSLog(@"you");
    }
}

- (void)stepTag:(UIStepper *)stepper {
    NSLog(@"sssss %f",stepper.value);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [FlowView new];
        [self.view addSubview:_bgView];
        _bgView.sd_layout.leftSpaceToView(self.view,0).topSpaceToView(self.view,20)
        .rightSpaceToView(self.view,0).heightIs(300);
//        _bgView.userInteractionEnabled = NO;
        _bgView.backgroundColor = [UIColor lightGrayColor];
    }
    return _bgView;
}

-(FlowView*)creatView{
    int w = (arc4random() % 61) + 60;
    int h = (arc4random() % 61) + 60;
    FlowView *flow = [[FlowView alloc]initWithFrame:CGRectMake(0, 0, w, h)];
    flow.backgroundColor = UIColorWithRandom;
    [self.bgView addSubview:flow];
    
    
    [RACObserve(flow, touchDown) subscribeNext:^(NSNumber* x) {
        if (x.boolValue) {
            self.currentView.onChoose = NO;
            self.currentView =flow;
            self.currentView.onChoose = YES;
        }else{
            //            currentView = nil;
        }
    }];
    return flow;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    self.currentView.onChoose = NO;
}

@end
