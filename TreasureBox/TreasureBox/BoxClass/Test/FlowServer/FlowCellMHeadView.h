//
//  FlowCellHeadView.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/23.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlowCellMHeadView : UIView
@property(nonatomic,strong) UITextField *titleName;
@property(nonatomic,strong) UITextField *iconName;
@property(nonatomic,strong) UIImageView *imageView;
@end
