
//
//  FlowStepView.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/20.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowStepView.h"

@implementation FlowStepView
-(UILabel *)title{
    if (!_title) {
        _title = [UILabel new];
        [self addSubview:_title];
        _title.sd_layout.leftEqualToView(self).topEqualToView(self).bottomEqualToView(self).minWidthIs(50);
        _title.text = @"dasd";
    }
    return _title;
}
-(UITextField *)value{
    if (!_value) {
        _value = [UITextField new];
        [self addSubview:_value];
        _value.sd_layout.leftSpaceToView(self.title,0).topEqualToView(self).bottomEqualToView(self).rightSpaceToView(self.steper,0).minWidthIs(50);
        _value.clearButtonMode =  UITextFieldViewModeAlways;
        _value.keyboardType = UIKeyboardTypeNumberPad;
    }
    return _value;
}

-(UIStepper *)steper{
    if (!_steper) {
        _steper = [UIStepper new];
        [self addSubview:_steper];
        _steper.sd_layout.topEqualToView(self).leftSpaceToView(self.value,0).bottomEqualToView(self).rightEqualToView(self);
        temp = _steper.value;
        _steper.stepValue = 0.1;
        [_steper addTarget:self action:@selector(st:) forControlEvents:UIControlEventValueChanged];
        
    }
    return _steper;
}


-(void)st:(UIStepper*)sender{
    if (temp>sender.value) {
        NSLog(@"-");
        
        if (([self.value.text floatValue] - sender.stepValue)>=0) {
           self.value.text = [NSString stringWithFormat:@"%f",[self.value.text floatValue] - sender.stepValue]; 
        }
    }else{
        NSLog(@"+");
        self.value.text = [NSString stringWithFormat:@"%f",[self.value.text floatValue] + sender.stepValue];
    }
    temp = sender.value;
}

@end
