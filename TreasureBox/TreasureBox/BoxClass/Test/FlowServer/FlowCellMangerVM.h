//
//  FlowCellMangerVM.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/23.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FlowCellMHeadViewModel.h"
#import "FlowCellMPicViewModel.h"
#import "FlowCellMWebViewModel.h"
@interface FlowCellMangerVM : NSObject
@property(nonatomic,strong) NSArray *menuArr;
@property(nonatomic,strong) FlowCellMHeadViewModel *HeadViewModel;
@property(nonatomic,strong) FlowCellMPicViewModel *picViewModel;
@property(nonatomic,strong)  FlowCellMWebViewModel *WebViewModel;
@end
