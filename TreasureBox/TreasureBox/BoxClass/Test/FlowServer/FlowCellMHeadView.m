//
//  FlowCellHeadView.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/23.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowCellMHeadView.h"

@implementation FlowCellMHeadView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.titleName = [[UITextField alloc]init];
        
    
        [self addSubview:self.titleName];
        self.titleName.sd_layout.leftEqualToView(self).topEqualToView(self).rightEqualToView(self).heightIs(44);
        self.titleName.backgroundColor = [UIColor grayColor];
        self.titleName.placeholder = @"标题名";
        
        
        self.iconName = [[UITextField alloc]init];
        [self addSubview:self.iconName];
        self.iconName.sd_layout.leftEqualToView(self.titleName).topSpaceToView(self.titleName,0).rightEqualToView(self.titleName).heightIs(50);
        self.iconName.placeholder = @"图片名";
        
        self.imageView = [[UIImageView alloc]init];
        [self addSubview:self.imageView];
        self.imageView.sd_layout.leftEqualToView(self.iconName).topSpaceToView(self.iconName,0).rightEqualToView(self.iconName).heightEqualToWidth();
        self.imageView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

@end
