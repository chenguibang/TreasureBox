//
//  FlowCellMangerView.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/22.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlowCellMPicView.h"
#import "FlowCellMHeadView.h"
#import "FlowCellMWebView.h"
@interface FlowCellMangerView : UIView
@property(nonatomic,strong) UITableView *menuTable;
@property(nonatomic,strong) FlowCellMPicView *picView;
@property(nonatomic,strong) UIButton *sureBtn;
@property(nonatomic,strong) FlowCellMHeadView *headView;
@property(nonatomic,strong) FlowCellMWebView *webView;
@end
