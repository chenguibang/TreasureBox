//
//  FlowCellMWebView.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/23.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowCellMWebView.h"

@implementation FlowCellMWebView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init
{
    self = [super init];
    if (self) {
       
        self.urltf = [[UITextField alloc]init];
        
        [self addSubview:self.urltf];
        self.urltf.sd_layout.leftEqualToView(self).topEqualToView(self).rightEqualToView(self).heightIs(44);
        self.urltf.backgroundColor = [UIColor grayColor];
        self.urltf.placeholder = @"网址";
    }
    return self;
}
@end
