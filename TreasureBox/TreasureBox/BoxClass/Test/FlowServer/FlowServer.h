//
//  FlowServer.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/19.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlowView.h"

@interface FlowServer : UIViewController
@property(nonatomic,strong) FlowView *bgView;
@end
