//
//  FlowCellMangerVM.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/23.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowCellMangerVM.h"

@implementation FlowCellMangerVM


-(NSArray *)menuArr{
    return @[@"图片",@"表头",@"网页",@"图文",@"滚动广告"];
}

-(FlowCellMHeadViewModel *)HeadViewModel{
    if (!_HeadViewModel) {
        _HeadViewModel = [[FlowCellMHeadViewModel alloc]init];
    }
    return _HeadViewModel;
}

-(FlowCellMPicViewModel *)picViewModel{
    if (!_picViewModel) {
        _picViewModel = [[FlowCellMPicViewModel alloc]init];
    }
    return _picViewModel;
}

-(FlowCellMWebViewModel *)WebViewModel{
    if (!_WebViewModel) {
        _WebViewModel = [[FlowCellMWebViewModel alloc]init];
        
    }
    return _WebViewModel;
}
@end
