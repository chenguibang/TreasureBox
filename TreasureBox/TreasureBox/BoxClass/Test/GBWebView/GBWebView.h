//
//  GBWebView.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/18.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GBWebView;
@protocol GBWebViewProgressDelegate <NSObject>
@optional
- (void) webView:(GBWebView*)webView didReceiveResourceNumber:(int)resourceNumber totalResources:(int)totalResources;
@end
@interface GBWebView : UIWebView
@property (nonatomic, assign) int resourceCount;
@property (nonatomic, assign) int resourceCompletedCount;

@property (nonatomic, assign) IBOutlet id<GBWebViewProgressDelegate> progressDelegate;
@end
