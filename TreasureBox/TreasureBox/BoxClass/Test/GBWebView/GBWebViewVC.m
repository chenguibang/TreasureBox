//
//  GBWebViewVC.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/18.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "GBWebViewVC.h"
#import "GBWebView.h"

@interface GBWebViewVC ()<UIWebViewDelegate,GBWebViewProgressDelegate>
@property(nonatomic,strong)GBWebView *gbWebView;
@property(nonatomic,strong)UIProgressView *progressView;
@property(nonatomic,strong)UINavigationBar *navBar;
@end

@implementation GBWebViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:@"http://www.baidu.com"]];
    
    [self.gbWebView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(GBWebView *)gbWebView{
    if (!_gbWebView) {
        _gbWebView = [GBWebView new];
        [self.view addSubview:_gbWebView];
        _gbWebView.backgroundColor = [UIColor blackColor];
        _gbWebView.sd_layout.leftEqualToView(self.view).topSpaceToView(self.navBar ,0).rightEqualToView(self.view).bottomEqualToView(self.view);
        _gbWebView.delegate = self;
        _gbWebView.progressDelegate = self;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    return _gbWebView;
}

-(UIProgressView *)progressView{
    if (!_progressView) {
        _progressView = [[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
        [self.gbWebView addSubview:_progressView];
        _progressView.sd_layout.leftEqualToView(self.gbWebView).rightEqualToView(self.gbWebView).topEqualToView(self.gbWebView).autoHeightRatio(0);
    }
    return _progressView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIWebViewDelegate
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    self.progressView.hidden = NO;
    [self.progressView setProgress:0 animated:NO];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    self.progressView.hidden = YES;
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    self.progressView.hidden = YES;
}
#pragma mark -GBWebViewDelegate
-(void)webView:(GBWebView *)webView didReceiveResourceNumber:(int)resourceNumber totalResources:(int)totalResources
{
    [self.progressView setProgress:resourceNumber/totalResources animated:YES];
}

-(UINavigationBar *)navBar{
    if (!_navBar) {
        _navBar = [[UINavigationBar alloc]init];
        [self.view addSubview:_navBar];
        _navBar.sd_layout.topEqualToView(self.view).rightEqualToView(self.view).leftEqualToView(self.view).heightIs(64);
    }
    return _navBar;
}

@end
