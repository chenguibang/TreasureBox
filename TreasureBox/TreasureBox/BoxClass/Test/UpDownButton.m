//
//  UpDownButton.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/10.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "UpDownButton.h"

@implementation UpDownButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.udBtn = [UIButton new];
        [self addSubview:self.udBtn];
        self.udBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.udBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
        self.udBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.icon = [UIImageView new];
        [self addSubview:self.icon];
        
        self.udBtn.sd_layout.leftEqualToView(self).topEqualToView(self).bottomEqualToView(self).rightEqualToView(self);
        self.icon.backgroundColor = [UIColor orangeColor];
        self.icon.sd_layout.centerYEqualToView(self).widthIs(20).heightIs(20).rightEqualToView(self.udBtn);

        
//        RAC(self.icon,backgroundColor) = [RACObserve(self, isUp) map:^id(NSNumber* value) {
//            return value.boolValue? [UIColor yellowColor]:[UIColor blackColor];
//        }];
      
    }
    return self;
}

-(void)setTitle:(NSString *)title{
    _title = title;
    [self.udBtn setTitle:title forState:UIControlStateNormal];
}
-(void)setIsUp:(BOOL)isUp{
    _isUp = isUp;
    self.icon.backgroundColor =_isUp? [UIColor yellowColor]:[UIColor blackColor];
}


@end
