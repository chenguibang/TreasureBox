//
//  Test1.m
//  TreasureBox
//
//  Created by Chen on 16/2/3.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "Test1.h"
#import "Test2.h"
#import "BoxContolerSwiper.h"
#import "FlowCollectionVC.h"
#import "ProductListVC.h"
#import "MVVMTestVC.h"
#import "GBWebViewVC.h"
#import "FlowServer.h"
#import "GPUImageTestVC.h"
#import "CAGradientLayerVC.h"
#import "PopViewVC.h"
#import "XMPPUtil.h"
#import "XMPPControler.h"
#import "ChatControler.h"

@interface Test1 ()<UITableViewDataSource,UITableViewDelegate>{
    NSArray *tableArr;
}

@end

@implementation Test1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor orangeColor];
//    [self performSelector:@selector(bb) withObject:nil afterDelay:2];
    
    [self test22];
    
 }


-(void)test22{
    tableArr = [NSArray arrayWithObjects:@"瀑布流布局", @"MVVM",@"MVVMTest",@"Swift",@"WebView",@"FlowServer",@"GPUImage",@"CAGradientLayerVC",@"弹窗动画",@"XMPP",@"ChatControler",@"YYKitVC",@"RegexVC",@"XMPPChatControler",@"MasonryVC",@"RuntimeVC",@"PaintCodeController",
                @"SafeJSONVC",
                nil];
    UITableView *table = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    table.dataSource = self;
    table.delegate =self;
    [self.boxContentView addSubview:table];
    
    table.sd_layout.leftEqualToView(self.boxContentView).topEqualToView(self.boxContentView).widthRatioToView(self.boxContentView,1).heightRatioToView(self.boxContentView,1);
    
    

    
}

-(void)test11{
    UIView *vie1 =[UIView new];
    [self.boxContentView addSubview:vie1];
    
    
    vie1.frame = CGRectMake(0, 0, 100, 100);
    
    
    UIView *view2 = [UIView new];
    [self.boxContentView addSubview:view2];
    view2.sd_layout.topSpaceToView(vie1,5).leftEqualToView(self.boxContentView).heightIs(50).widthIs(50);
    
    
    vie1.backgroundColor = [UIColor orangeColor];
    view2.backgroundColor = [UIColor purpleColor];
    
    
    UIView *v3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    [view2 addSubview:v3];
    v3.backgroundColor = [UIColor blackColor];
    
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        vie1.frame = CGRectMake(vie1.frame.origin.x, vie1.frame.origin.y
    //                                , vie1.frame.size.width, 30);
    //
    //        self.boxNavigationBar.hidebar = YES;
    //        [BoxContolerSwiper switchTo:NSStringFromClass([Test2 class]) form:self];
    //
    //    });
    self.boxNavigationBar.midLabel.text = @"测试";
    
    
    
    UITextField *tf = [UITextField new];
    [self.view addSubview:tf];
    tf.sd_layout.leftEqualToView(self.view).topSpaceToView(self.view,200).widthIs(200).heightIs(44);
    tf.backgroundColor = [UIColor grayColor];
    
    
    UITextField *tf2 = [UITextField new];
    [self.view addSubview:tf2];
    tf2.sd_layout.leftEqualToView(self.view).topSpaceToView(self.view,250).widthIs(200).heightIs(44);
    tf2.backgroundColor = [UIColor grayColor];
    
    //    [tf.rac_textSignal subscribeNext:^(id x) {
    //        NSLog(@"输入的内容为：%@",x);
    //    } completed:^{
    //        NSLog(@"完成");
    //    }];
    
    [[tf.rac_textSignal filter:^BOOL(id value) {
        NSString*text = value;
        return text.length > 3;
        
        
    }] subscribeNext:^(id x) {
        //        NSLog(@"输入的内容为：%@",x);
    } completed:^{
        //         NSLog(@"完成");
    }];
    
    
    [[[tf.rac_textSignal map:^id(NSString* value) {
        return @(value.length);
    }]filter:^BOOL(NSNumber* value) {
        return[value integerValue] > 3;
    }] subscribeNext:^(id x) {
        //        NSLog(@"输入的内容为：%@",x);
    } error:^(NSError *error) {
        
    } completed:^{
        
    }];
    
    
    
    
    RACSignal *validUsernameSignal =
    [tf.rac_textSignal
     map:^id(NSString *text) {
         NSLog(@"输入的内容为：%@",text);
         return @([self isValidUsername:text]);
     }];
    RACSignal *validPasswordSignal =
    [tf2.rac_textSignal
     map:^id(NSString *text) {
         return @([self isValidUsername:text]);
     }];
    
    [[tf.rac_textSignal filter:^BOOL(id value) {
        NSLog(@"判断长度是否大于3");
        NSString *s = value;
        return s.length>3;
    }]subscribeNext:^(id x) {
        NSLog(@"%@  大于三了",x);
    }];
    
    
    
    RAC(tf, backgroundColor) =
    [validUsernameSignal
     map:^id(NSNumber *passwordValid){
         return[passwordValid boolValue] ? [UIColor purpleColor]:[UIColor yellowColor];
     }];
    
    
    RAC(tf2, backgroundColor) =
    [validPasswordSignal
     map:^id(NSNumber *passwordValid){
         return[passwordValid boolValue] ? [UIColor whiteColor]:[UIColor redColor];
     }];
    
    
    RACSignal *loginsi = [RACSignal combineLatest:@[validPasswordSignal,validUsernameSignal] reduce:^id(NSNumber*usernameValid, NSNumber *passwordValid){
        return @([usernameValid boolValue]&&[passwordValid boolValue]);
    }];
    
    [loginsi subscribeNext:^(id x) {
//        self.boxContentView.backgroundColor =  x?[UIColor orangeColor]:[UIColor blueColor];
    }];

}

-(BOOL)isValidUsername:(NSString*)text{
    
    return text.length>5;
}

-(void)bb{
    Test2 *t2 = [[Test2 alloc]init];
    t2.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:t2 animated:YES];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *title = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
    if ([title isEqualToString:@"瀑布流布局"]) {
        FlowCollectionVC *vc = [[FlowCollectionVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"MVVM"]) {
        ProductListVC *vc = [[ProductListVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"MVVMTest"]) {
        MVVMTestVC *vc = [[MVVMTestVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"Swift"]) {
//        SwiftControler *vc = [[SwiftControler alloc]init];
//        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"WebView"]) {
        GBWebViewVC *vc = [[GBWebViewVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([title isEqualToString:@"FlowServer"]) {
        FlowServer *vc = [[FlowServer alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if ([title isEqualToString:@"GPUImage"]) {
        GPUImageTestVC *vc = [[GPUImageTestVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if ([title isEqualToString:@"CAGradientLayerVC"]) {
        CAGradientLayerVC *vc = [[CAGradientLayerVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if ([title isEqualToString:@"弹窗动画"]) {
        PopViewVC *vc = [[PopViewVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    else if ([title isEqualToString:@"XMPP"]) {
        XMPPControler *vc = [[XMPPControler alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    else if ([title isEqualToString:@"ChatControler"]) {
        ChatControler *vc = [[ChatControler alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        Class vcs = NSClassFromString(title);
        
        
        UIViewController *vc = (UIViewController*)[[vcs alloc]init];
        [self.navigationController pushViewController:vc animated:YES];

        
    }
    
    

    
    
    
    
    
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableArr.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = tableArr[indexPath.row];
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
