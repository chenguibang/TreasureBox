//
//  FlowCollectionLayout.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/12.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlowCollectionSectionAtrrbute.h"
#import "CollectionViewItemAttribute.h"

@protocol FlowCollectionLayoutDelegate <UICollectionViewDelegateFlowLayout>

-(NSInteger*)numsLinesOfSection:(NSInteger*)section;

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath;
-(CollectionViewItemAttribute *) itemAttributeAtIndexPath:(NSIndexPath *)indexPath;

-(FlowCollectionSectionAtrrbute*)sectionAttributeAtIndexPath:(CGFloat)section;

@end

typedef CGFloat(^HeightBlock)(NSIndexPath *indexPath , CGFloat width);
@interface FlowCollectionLayout : UICollectionViewLayout

///** 列数 */
//@property (nonatomic, assign) NSInteger lineNumber;
////行间距
//@property (nonatomic, assign) CGFloat rowSpacing;
////列间距
//@property (nonatomic, assign) CGFloat lineSpacing;
////内编剧
//@property (nonatomic, assign) UIEdgeInsets sectionInset;

//section 间距
@property (nonatomic, assign) CGFloat seactionSpacing;

@property(nonatomic,assign) CGFloat contentHeight;
@property (nonatomic,assign) id<FlowCollectionLayoutDelegate> flowDelegate;
//计算item高度
- (void)computeIndexCellHeightWithWidthBlock:(CGFloat(^)(NSIndexPath *indexPath , CGFloat width))block;
@end
