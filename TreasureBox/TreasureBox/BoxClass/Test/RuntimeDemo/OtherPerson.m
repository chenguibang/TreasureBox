//
//  OtherPerson.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "OtherPerson.h"

@implementation OtherPerson
+(void)load{
    Class clazz = NSClassFromString(@"Person");
    Method instance_eat = class_getClassMethod(clazz, @selector(resolveInstanceMethod:));
    Method instance_notEat = class_getClassMethod(self, @selector(hy_resolveInstanceMethod:));
    method_exchangeImplementations(instance_eat, instance_notEat);
    
    Method class_eat = class_getClassMethod(clazz, @selector(resolveClassMethod:));
    Method class_notEat = class_getClassMethod(self, @selector(hy2_resolveClassMethod:));
    method_exchangeImplementations(class_eat, class_notEat);
}

void eat_1(id self,SEL sel){
    NSLog(@"到底吃不吃饭了");
    NSLog(@"%@ %@",self,NSStringFromSelector(sel));
}

void eat_2(id self,SEL sel, NSString* str1,NSString* str2){
    NSLog(@"到底吃不吃饭了");
    NSLog(@"%@ %@",self,NSStringFromSelector(sel));
    NSLog(@"打印两个参数值：%@ and %@",str1,str2);
}

+(BOOL)hy_resolveInstanceMethod:(SEL)sel{
    if (sel == NSSelectorFromString(@"eat")) {
        class_addMethod(self, sel, (IMP)eat_1, "v@:");
    }
    return YES;
}
+(BOOL)hy2_resolveClassMethod:(SEL)sel{
    if (sel == NSSelectorFromString(@"eat:with:")) {
        class_addMethod(objc_getMetaClass("Person"), sel, (IMP)eat_2, "v#:@@");
    }
    return YES;
}
@end
