//
//  RuntimeVC.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/12.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "RuntimeVC.h"
#import "XMPPChatCell.h"
#import "RuntimeObjc.h"
#import <objc/runtime.h>
@interface RuntimeVC ()

@end

@implementation RuntimeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    unsigned int count;
    //获取属性列表
    objc_property_t *propertyList = class_copyPropertyList([RuntimeObjc class], &count);
    for (unsigned int i=0; i<count; i++) {
        const char *propertyName = property_getName(propertyList[i]);

        NSLog(@"property---->%@", [NSString stringWithUTF8String:propertyName]);
    }
    
    
    
    
   
    //动态添加方法
     class_addMethod([RuntimeObjc class], @selector(method::), (IMP)myAddingFunction, "i@:i@");
     //获取方法列表
    Method *memberFuncs = class_copyMethodList([RuntimeObjc class], &count);//所有在.m文件显式实现的方法都会被找到
    for (int i = 0; i < count; i++) {
        SEL name = method_getName(memberFuncs[i]);
        NSString *methodName = [NSString stringWithCString:sel_getName(name) encoding:NSUTF8StringEncoding];
        NSLog(@"member method:%@", methodName);
    }
    
    
    
    
    
    
    //获取成员变量列表
    Ivar *ivarList = class_copyIvarList([RuntimeObjc class], &count);
    for (unsigned int i; i<count; i++) {
        Ivar myIvar = ivarList[i];
        const char *ivarName = ivar_getName(myIvar);
        const char *ivartype = ivar_getTypeEncoding(myIvar);
        NSLog(@"Ivar---->%@   -- %s", [NSString stringWithUTF8String:ivarName],ivartype);
    }
    
    //修改私有变量
    
    RuntimeObjc *object= [[RuntimeObjc alloc]init];
    object_setIvar(object, ivarList[1], @"我改了私变量");
    NSLog(@"改后 -- %@",[object description]);
    
    [object performSelector:@selector(method::) withObject:nil];
    
    
    
    
    
    //获取协议列表
    __unsafe_unretained Protocol **protocolList = class_copyProtocolList([RuntimeObjc class], &count);
    for (unsigned int i; i<count; i++) {
        Protocol *myProtocal = protocolList[i];
        const char *protocolName = protocol_getName(myProtocal);
        NSLog(@"protocol---->%@", [NSString stringWithUTF8String:protocolName]);
    }
    
    
    [self tryMethodExchange];
    
    [self addPro];
}



/**
 *  添加属性
 */
-(void)addPro{
    RuntimeObjc *object= [[RuntimeObjc alloc]init];
    static char associatedObjectKey;
    //设置关联对象
    objc_setAssociatedObject(object, &associatedObjectKey, @(56), OBJC_ASSOCIATION_RETAIN_NONATOMIC); //获取关联对象
    
    NSNumber *string = objc_getAssociatedObject(object, &associatedObjectKey);
    NSLog(@"AssociatedObject = %d", string.intValue);

}
- (id)getAssociatedObject{
    return objc_getAssociatedObject(self, _cmd);
}

- (IBAction)changeSL:(id)sender {
    
    Class clazz = NSClassFromString(@"Person");
           //get this Person Instance 拿到这个Person实例
           id person = [[clazz alloc] init];
    
           //send message to 'eat' method in Person Class or Person Instance
            //发送消息给Person类或者Person实例的‘eat’方法 不含参数
            [person performSelector:@selector(eat) withObject:nil];
             //发送消息给Person类的‘eat’方法 含两个参数
             [clazz performSelector:@selector(eat:with:)
                                withObject:@"Hello"
                                    withObject:@"World"];
    
}



//方法替换
- (void)tryMethodExchange
{
    Method method1 = class_getInstanceMethod([NSString class], @selector(lowercaseString));
    Method method2 = class_getInstanceMethod([NSString class], @selector(uppercaseString));
    method_exchangeImplementations(method1, method2);
    NSLog(@"lowcase of WENG zilin:%@", [@"WENG zilin" lowercaseString]);
    NSLog(@"uppercase of WENG zilin:%@", [@"WENG zilin" uppercaseString]);
}


int myAddingFunction(id self, SEL _cmd, int var1, NSString *str)
{
    NSLog(@"I am added funciton");
    return 10;
}





//void eat_1(id self,SEL sel)
//{
//    NSLog(@"到底吃不吃饭了");
//    NSLog(@"%@ %@",self,NSStringFromSelector(sel));
//}
//void eat_2(id self,SEL sel, NSString* str1,NSString* str2)
//{
//    NSLog(@"到底吃不吃饭了");
//    NSLog(@"%@ %@",self,NSStringFromSelector(sel));
//    NSLog(@"打印两个参数值：%@ and %@",str1,str2);
//}
//
//+(BOOL)hy_resolveInstanceMethod:(SEL)sel{
//    //当sel为实现方法中 有 eat 方法
//    if (sel == NSSelectorFromString(@"eat")) {
//        //就 动态添加eat方法
//        
//        // 第一个参数：给哪个类添加方法
//        // 第二个参数：添加方法的方法编号
//        // 第三个参数：添加方法的函数实现（函数地址）
//        // 第四个参数：函数的类型，(返回值+参数类型) v:void @:对象->self :表示SEL->_cmd
//        class_addMethod(self, sel, (IMP)eat_1, "v@:");
//    }
//    return YES;
//}
//+(BOOL)hy2_resolveClassMethod:(SEL)sel{
//    
//    if (sel == NSSelectorFromString(@"eat:with:")) {
//        
//        class_addMethod(objc_getMetaClass("Person"), sel, (IMP)eat_2, "v#:@@");
//    }
//    
//    return YES;
//}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(BOOL)resolveInstanceMethod:(SEL)sel{
    
    
    
    return YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
