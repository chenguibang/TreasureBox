//
//  FlowTextWithImage1.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/21.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlowTextWithImageCell1 : UICollectionViewCell
@property(nonatomic,retain)UILabel *title;
@property(nonatomic,retain)UIImageView *icon;
@end
