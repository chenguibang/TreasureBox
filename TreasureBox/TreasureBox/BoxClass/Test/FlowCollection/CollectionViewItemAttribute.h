//
//  CollectionViewItemAttribute.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/16.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CollectionViewItemAttribute : NSObject
//@property(nonatomic,assign) CGFloat y;
@property(nonatomic,assign) CGFloat widthscale;
@property(nonatomic,assign) CGFloat heightscale;
@property(nonatomic,assign) CGFloat xpoint;
@property(nonatomic,assign) CGFloat ypoint;

@property(nonatomic,assign) CGFloat top;
@property(nonatomic,assign) CGFloat left;
@property(nonatomic,assign) CGFloat bottom;
@property(nonatomic,assign) CGFloat right;
@end
