//
//  FlowTextWithImage1.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/21.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowTextWithImageCell1.h"

@implementation FlowTextWithImageCell1

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
         self.contentView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(UILabel *)title{
    if (!_title) {
        _title = [UILabel new];
        [self.contentView addSubview:_title];
        _title.sd_layout.heightIs(self.contentView.height).widthIs(100).centerYEqualToView(self.contentView).leftSpaceToView(self.icon,0);
        _title.text = @"哈哈哈";
    }
    return _title;
}

-(UIImageView *)icon{
    if (!_icon) {
        _icon = [UIImageView new];
        [self.contentView addSubview:_icon];
        _icon.sd_layout.topSpaceToView(self.contentView,1).bottomSpaceToView(self.contentView,2).widthEqualToHeight(0).centerYEqualToView(self.contentView).leftSpaceToView(self.contentView,5);
    }
    return _icon;
}
@end
