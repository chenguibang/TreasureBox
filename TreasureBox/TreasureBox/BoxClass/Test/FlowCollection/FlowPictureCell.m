//
//  PictureCell.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/18.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowPictureCell.h"

@implementation FlowPictureCell
-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [UIImageView new];
        [self.contentView addSubview:_imageView];
        _imageView.sd_layout.leftEqualToView(self.contentView).topEqualToView(self.contentView)
        .rightEqualToView(self.contentView).bottomEqualToView(self.contentView);
        
    }
    return _imageView;
}


-(void)setImageName:(NSString *)imageName{
    _imageName = imageName;
    self.imageView.image = [UIImage imageNamed:imageName];
}
-(void)setImageUrl:(NSString *)imageUrl{
    
}
@end
