//
//  WebViewCell.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/18.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowWebViewCell.h"

@implementation FlowWebViewCell

-(GBWebView *)webView{
    if (!_webView) {
        _webView = [GBWebView new];
        [self.contentView addSubview:_webView];
        _webView.sd_layout.leftEqualToView(self.contentView).topEqualToView(self.contentView).rightEqualToView(self.contentView).bottomEqualToView(self.contentView);
        _webView.scrollView.showsHorizontalScrollIndicator = NO;
        _webView.scrollView.showsVerticalScrollIndicator = NO;
        _webView.opaque = NO;
        _webView.delegate = self;
        [_webView scalesPageToFit];
        
    }
    return _webView;
}


-(void)setUrl:(NSString *)url{
    _url = url;
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:_url].absoluteURL];
    [self.webView loadRequest:request];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    NSString *meta = [NSString stringWithFormat:@"document.getElementsByName(\"viewport\")[0].content = \"width=%f, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\"", self.contentView.width];
    [webView stringByEvaluatingJavaScriptFromString:meta];
}
@end
