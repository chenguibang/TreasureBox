//
//  PictureCell.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/18.
//  Copyright © 2016年 Chen. All rights reserved.

#import <UIKit/UIKit.h>

@interface FlowPictureCell : UICollectionViewCell
@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,copy) NSString *imageName;
@property(nonatomic,copy) NSString *imageUrl;
@end
