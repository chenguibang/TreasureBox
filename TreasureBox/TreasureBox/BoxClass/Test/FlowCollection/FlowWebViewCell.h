//
//  WebViewCell.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/18.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBWebView.h"

@interface FlowWebViewCell : UICollectionViewCell<UIWebViewDelegate>
@property(nonatomic,strong) GBWebView *webView;
@property(nonatomic,copy) NSString *url;
@end
