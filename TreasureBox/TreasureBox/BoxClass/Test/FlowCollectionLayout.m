//
//  FlowCollectionLayout.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/12.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowCollectionLayout.h"
#import "CollectionViewItemAttribute.h"
#import "FlowCollectionSectionAtrrbute.h"



@interface FlowCollectionLayout(){
    
    
}
/** 存放每列高度字典*/
@property (nonatomic, strong) NSMutableDictionary *dicOfheight;

/** 存放所有item的attrubutes属性*/
@property (nonatomic, strong) NSMutableArray *array;


@property(nonatomic,strong)NSMutableArray *sectiondata; //!< 用于存储每个section的信息

/** 计算每个item高度的block，必须实现*/
@property (nonatomic, copy) HeightBlock block;
@end

@implementation FlowCollectionLayout



- (instancetype)init
{
    self = [super init];
    if (self) {
        
//        self.lineNumber = 4; //默认4行
//        self.rowSpacing = 10.0f;//行间距
//        self.lineSpacing = 10.0f; //列间距
//        self.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        _dicOfheight = [NSMutableDictionary dictionary];
        _array = [NSMutableArray array];
    }
    return self;
}






#pragma mark - 需要重写的方法

-(void)prepareLayout{
    [super prepareLayout];
    NSInteger numsOfsection = [self.collectionView numberOfSections];
    CGFloat sectionY = 0;
    for (int currentSection = 0; currentSection<numsOfsection; currentSection++) {
        FlowCollectionSectionAtrrbute *sectionAttrubut = [self.flowDelegate sectionAttributeAtIndexPath:currentSection];
        CGFloat sectionWidth = self.collectionView.frame.size.width;
        CGFloat sectionHeight = sectionAttrubut.scale *sectionWidth;
        NSInteger numsOfitem = [self.collectionView numberOfItemsInSection:currentSection];
        for (int itemindex = 0; itemindex < numsOfitem; itemindex++) {
            CollectionViewItemAttribute *itematrrbute = [self.flowDelegate itemAttributeAtIndexPath:[NSIndexPath indexPathForRow:itemindex inSection:currentSection]];
            
             UICollectionViewLayoutAttributes *attributes = [self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForRow:itemindex inSection:currentSection]];
            CGRect itemFrame;
            itemFrame.size.width = itematrrbute.widthscale*sectionWidth - itematrrbute.left - itematrrbute.right;
            itemFrame.size.height = itematrrbute.heightscale*sectionHeight - itematrrbute.top - itematrrbute.bottom;
            itemFrame.origin.x = itematrrbute.xpoint*sectionWidth + itematrrbute.left;
            itemFrame.origin.y = itematrrbute.ypoint*sectionHeight + itematrrbute.top + sectionY;
            attributes.frame = itemFrame;
            [_array addObject:attributes];
        }
        sectionY += sectionHeight +self.seactionSpacing;
        self.contentHeight = sectionY;
    }
}



//找出最最低的位置且
-(NSArray*)findlowest:(NSArray*)beforeArr{
    NSMutableArray *lowstarr = [[NSMutableArray alloc]init];
    CGFloat lowY = 0;
    for (UICollectionViewLayoutAttributes *attributes  in beforeArr) {
        if (attributes.frame.origin.y+attributes.frame.size.height>lowY) {
            lowY = attributes.frame.origin.y+attributes.frame.size.height;
        }
    }
    for (UICollectionViewLayoutAttributes *attributes  in beforeArr) {
        if (attributes.frame.origin.y==lowY) {
            [lowstarr addObject:attributes];
        }
    }
    return lowstarr;
}

//当collectionView视图位置有新改变(发生移动)时调用，其若返回YES则重新布局
//-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
//    return YES;
//}
// 准备好布局时调用。此时collectionView所有属性都已确定。读者在这里可以将collectionView当做画布，有了画布后，我们便可以在其上面画出每个item

//  返回collectionView视图中所有视图的属性(UICollectionViewLayoutAttributes)数组
-(NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect{
       return _array;
}



//返回indexPath对应item的属性
- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (self.flowDelegate&&[self.flowDelegate respondsToSelector:@selector(layoutAttributesForItemAtIndexPath:)]) {
       return  [self.flowDelegate layoutAttributesForItemAtIndexPath:indexPath];
    }
//
    return nil;

}
//返回indexPath对应item的属性
-(CGSize)collectionViewContentSize{
    return CGSizeMake(self.collectionView.bounds.size.width, self.contentHeight);
}
- (void)computeIndexCellHeightWithWidthBlock:(CGFloat (^)(NSIndexPath *, CGFloat))block{
    if (self.block != block) {
        self.block = block;
    }
}


-(void)prepareForCollectionViewUpdates:(NSArray<UICollectionViewUpdateItem *> *)updateItems{
    [super prepareForCollectionViewUpdates:updateItems];
}
@end
