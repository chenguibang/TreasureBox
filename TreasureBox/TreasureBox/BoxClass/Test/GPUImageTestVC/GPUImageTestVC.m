//
//  GPUImageTestVC.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/1.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "GPUImageTestVC.h"
#import "GPUImage.h"

@interface GPUImageTestVC ()

@end

@implementation GPUImageTestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImage *inputImage = [UIImage imageNamed:@"14"];

    
       //创建一个亮度的滤镜
       GPUImageGaussianBlurFilter *passthroughFilter = [[GPUImageGaussianBlurFilter alloc] init];
       passthroughFilter.texelSpacingMultiplier = 0.4f;
     //设置要渲染的区域
       [passthroughFilter forceProcessingAtSize:inputImage.size];
        [passthroughFilter useNextFrameForImageCapture];
     //获取数据源
       GPUImagePicture *stillImageSource = [[GPUImagePicture alloc]initWithImage:inputImage];
      //添加上滤镜
      [stillImageSource addTarget:passthroughFilter];
       //开始渲染
        [stillImageSource processImage];
      //获取渲染后的图片
       UIImage *newImage = [passthroughFilter imageFromCurrentFramebuffer];
    
    
    
    
    GPUImageGaussianBlurFilter * blurFilter = [[GPUImageGaussianBlurFilter alloc] init];
    blurFilter.blurRadiusInPixels = 10.0;
    UIImage * imageee = [UIImage imageNamed:@"14"];
    UIImage *blurredImage = [blurFilter imageByFilteringImage:imageee];
    
    GPUImageOpacityFilter *op = [[GPUImageOpacityFilter alloc]init];
    op.opacity = 0.6;
    blurredImage = [op imageByFilteringImage:blurredImage];
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64 , 100, 100)];
    image.image = blurredImage;

    [self.view addSubview:image];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
