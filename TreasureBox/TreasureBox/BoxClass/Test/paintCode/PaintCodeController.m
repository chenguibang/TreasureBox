//
//  PaintCodeController.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "PaintCodeController.h"
#import "PaintView.h"
#import "CLineView.h"

@interface PaintCodeController (){
    PaintView *pa;
}

@end

@implementation PaintCodeController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    pa = [[PaintView alloc]initWithFrame:CGRectMake(0, 100, 320, 100)];
    pa.backgroundColor = [UIColor clearColor];
    [self.view addSubview:pa];
    CLineView *pa2 = [[CLineView alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
    
//    [self.view addSubview:pa2];
    
    
    
//    NSTimer *timer = [NSTimer timerWithTimeInterval:1
//                                         target:self
//                                       selector:@selector(updateProgressView)
//                                       userInfo:nil
//                                        repeats:YES];
//    
//    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
//    
//    [timer fire];

    pa.widTh = 150;
    [pa layoutIfNeeded];

    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        [UIView animateWithDuration:4 delay:2 options:UIViewAnimationOptionOverrideInheritedCurve animations:^{
            
            pa.widTh = 3;
            [pa setNeedsDisplay];
            
        } completion:^(BOOL finished) {
            
        }];
      
    });
    
    
}

-(void)ami{
    CABasicAnimation *expandAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
//    expandAnimation.fromValue = (__bridge id)(pa.layer.path);
  
    expandAnimation.beginTime = 0;
    expandAnimation.duration = 0.5;
    expandAnimation.fillMode = kCAFillModeForwards;
    expandAnimation.removedOnCompletion = NO;
    [pa.layer addAnimation:expandAnimation forKey:nil];
}

-(void)updateProgressView{
    pa.widTh +=10;
    [pa setNeedsDisplay];
    [self ami];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self updateProgressView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
