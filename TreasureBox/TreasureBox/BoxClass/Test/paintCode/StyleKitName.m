//
//  StyleKitName.m
//  ProjectName
//
//  Created by AuthorName on 16/7/14.
//  Copyright (c) 2016 CompanyName. All rights reserved.
//
//  Generated by PaintCode (www.paintcodeapp.com)
//

#import "StyleKitName.h"


@implementation StyleKitName

#pragma mark Initialization

+ (void)initialize
{
}

#pragma mark Drawing Methods

+(void)drawCanvas1:(CGFloat)w
{
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();


    //// Shadow Declarations
    NSShadow* shadow = [[NSShadow alloc] init];
    [shadow setShadowColor: UIColor.blackColor];
    [shadow setShadowOffset: CGSizeMake(1.1, 1.1)];
    [shadow setShadowBlurRadius: 5];

    //// Frames
    CGRect frame = CGRectMake(10, 44, 220, 28);


    //// PregressBG
    {
        //// Rectangle Drawing
        UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(CGRectGetMinX(frame) + floor((CGRectGetWidth(frame) - 220) / 2 + 0.5), CGRectGetMinY(frame) + floor((CGRectGetHeight(frame) - 28) / 2 + 0.5), 220, 28) cornerRadius: 5];
        [UIColor.grayColor setFill];
        [rectanglePath fill];


        //// Rectangle 2 Drawing
        UIBezierPath* rectangle2Path = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(CGRectGetMinX(frame) + floor((CGRectGetWidth(frame) - 199) * 0.42857 + 0.5), CGRectGetMinY(frame) + floor((CGRectGetHeight(frame) - 14) * 0.50000 + 0.5), 199, 14) cornerRadius: 5];
        [UIColor.grayColor setFill];
        [rectangle2Path fill];

        ////// Rectangle 2 Inner Shadow
        CGContextSaveGState(context);
        UIRectClip(rectangle2Path.bounds);
        CGContextSetShadowWithColor(context, CGSizeZero, 0, NULL);

        CGContextSetAlpha(context, CGColorGetAlpha([shadow.shadowColor CGColor]));
        CGContextBeginTransparencyLayer(context, NULL);
        {
            UIColor* opaqueShadow = [shadow.shadowColor colorWithAlphaComponent: 1];
            CGContextSetShadowWithColor(context, shadow.shadowOffset, shadow.shadowBlurRadius, [opaqueShadow CGColor]);
            CGContextSetBlendMode(context, kCGBlendModeSourceOut);
            CGContextBeginTransparencyLayer(context, NULL);

            [opaqueShadow setFill];
            [rectangle2Path fill];

            CGContextEndTransparencyLayer(context);
        }
        CGContextEndTransparencyLayer(context);
        CGContextRestoreGState(context);

    }


    //// PregressIn Drawing
    UIBezierPath* pregressInPath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(19, 51, w, 14) cornerRadius: 5];
    [UIColor.blueColor setFill];
    [pregressInPath fill];
}

@end
