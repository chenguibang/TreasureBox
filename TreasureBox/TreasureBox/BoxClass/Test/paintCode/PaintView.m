//
//  PaintView.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "PaintView.h"
#import "StyleKitName.h"
@implementation PaintView
- (instancetype)init
{
    self = [super init];
    if (self) {
         self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
   
    [StyleKitName drawCanvas1:self.widTh];
    
//    [StyleKitName drawStopwatch2];
}

@end
