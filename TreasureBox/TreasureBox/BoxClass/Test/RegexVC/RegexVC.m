//
//  RegexVC.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "RegexVC.h"
#import "RegexKitLite.h"

@interface RegexVC ()

@end

@implementation RegexVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSString *sourceStr = @"http://www.baidu.com http://www.hao123";
    NSArray* urls = [sourceStr componentsMatchedByRegex:@"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)"];
    NSLog(@"匹配到的 url :%@",urls);
    
    
    
    //微博：@用户 http://.... #话题#
    
    NSString *test = @"我在#话题#上课@麻子 你们@罗 在听吗https://www.baidu.com";
    
    //匹配@并且@后面可有n个字符（不包含空格）
    NSString *regex = @"@\\w+";
    
    //匹配#
    regex = @"#\\w+#";
    
    //匹配超链接:1，?表示匹配1次或0次。超链接的匹配是根据链接复杂度而定的。
    regex = @"http(s)?://([A-Za-z0-9.?_-]+(/)?)*";
    
    //把以上3个合成一个：使用或|隔开，每隔小单元格用小括号包含。
    regex = @"(@\\w+)|(#\\w+#)|(http(s)?://([A-Za-z0-9.?_-]+(/)?)*)";
    
    NSArray *regexArray = [test componentsMatchedByRegex:regex];
    
    for (NSString *s in regexArray)
    {
        NSLog(@"匹配结果s:%@",s);
        //字符串替换
        NSString *target = [NSString stringWithFormat:@"<a href='%@'>%@<a>",s,s];
        test = [test stringByReplacingOccurrencesOfString:s withString:target];
    }
    NSLog(@"替换后：%@",test);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
