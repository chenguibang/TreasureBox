//
//  ProductItemModel.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ProductItemModel : NSObject
@property(nonatomic,copy)NSString *imageUrl;
@property(nonatomic,copy)NSString *describe;
@property(nonatomic,copy)NSString *price;
@property(nonatomic,copy)NSString *count;//!< 付款人数
@property(nonatomic,copy)NSString *activity;

@end
