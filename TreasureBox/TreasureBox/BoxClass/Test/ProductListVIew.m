//
//  ProductListVIew.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "ProductListVIew.h"
#import "UpDownButton.h"
#import "ProuductCell.h"
#define CELLSHORT @"ProuductSmallCellTypeShort"
#define CELLLONG @"ProuductSmallCellTypeLong"
@implementation ProductListVIew
-(LineView *)lineView{
    if (!_lineView) {
        _lineView = [LineView new];
        [self addSubview:_lineView];
        _lineView.backgroundColor = [UIColor redColor];
        _lineView.sd_layout.leftEqualToView(self).topSpaceToView(self,0).rightEqualToView(self).heightIs(33);
        _lineView.sellstore.title = @"销量";
        _lineView.pricestore.title = @"价格";
        _lineView.allstroe.title = @"全部排序";
        [_lineView.filterBtn setTitle:@"筛选" forState:UIControlStateNormal];
        _lineView.filterBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_lineView.uiChangeBtn addTarget:self action:@selector(changeUI) forControlEvents:UIControlEventTouchUpInside];
        _lineView.uiChangeBtn.backgroundColor = [UIColor whiteColor];
    }
    return _lineView;
}


-(UICollectionView *)cltView{
    if (!_cltView) {
        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.minimumLineSpacing = 5;
        _cltView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, 320, 200) collectionViewLayout:flowLayout];
        [_cltView setBackgroundColor:[UIColor orangeColor]];
        type = ProuductSmallCellTypeShort;
        _cltView.dataSource=self;
        _cltView.delegate=self;
        [_cltView registerClass:[ProuductCell class] forCellWithReuseIdentifier:CELLSHORT];
        [_cltView registerClass:[ProuductCell class] forCellWithReuseIdentifier:CELLLONG];
        [self addSubview:_cltView];
        _cltView.sd_layout.leftEqualToView(self).topSpaceToView(self.lineView,1).rightEqualToView(self).bottomEqualToView(self);
    }
    return _cltView;
}

#pragma mark - collocation代理
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.cltViewArr.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ProuductCell *cell;
    if (type==ProuductSmallCellTypeLong) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:CELLLONG forIndexPath:indexPath];
    }else{
        cell  = [collectionView dequeueReusableCellWithReuseIdentifier:CELLSHORT forIndexPath:indexPath];
    }
    
    
    
    
    cell.backgroundColor = [UIColor whiteColor];
    
    ProductItemModel *model = [[ProductItemModel alloc]init];
    model.describe = [NSString stringWithFormat:@"%ld描述零距离交流交流空间立刻就立刻就看看见了接口连接",(long)indexPath.row];
    model.count = [NSString stringWithFormat:@"%ld付款",(long)indexPath.row];;
    model.price = [NSString stringWithFormat:@"%ld元",(long)indexPath.row];;
    
    if (indexPath.row%2==0) {
        model.activity = @"活动";
    }
    cell.type = type;
    cell.model = model;
    return cell;
    
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return type == ProuductSmallCellTypeLong?CGSizeMake(SCREEN_WIDTH, SCREEN_WIDTH/3):CGSizeMake((SCREEN_WIDTH -15)/2, (SCREEN_WIDTH/2-6)*9/6);
    
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5,5, 5,5);
    
    
}


//用户交互时间通知VC处理
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(void)refreshView{
    [self.cltView reloadData];
}

-(void)changeUI{
   type= type == ProuductSmallCellTypeLong?ProuductSmallCellTypeShort:ProuductSmallCellTypeLong;
    [self.cltView reloadData];
}

@end
