//
//  UpDownButton.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/10.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^UpDownClick)(BOOL up);

@interface UpDownButton : UIView
@property(nonatomic,retain)UIButton *udBtn;
@property(nonatomic,retain)UIImageView *icon;
@property(nonatomic,copy) NSString *title;
@property(nonatomic,assign) BOOL isUp;
@property(nonatomic,assign) UpDownClick listener;

@end
