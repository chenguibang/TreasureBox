//
//  MVVMTestVC.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestVM.h"

@interface MVVMTestVC : UIViewController
@property(retain,nonatomic)TestVM *viewModel;


@end
