//
//  TestVM.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestVM : NSObject
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *passwd;
@property(nonatomic,assign) BOOL nameIsValid;
@end
