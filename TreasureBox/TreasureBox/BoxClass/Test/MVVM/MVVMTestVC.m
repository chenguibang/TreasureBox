//
//  MVVMTestVC.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "MVVMTestVC.h"

@interface MVVMTestVC (){
    
    
    
}
@property(nonatomic,retain) UITextField *nameTf;
@property(nonatomic,retain) UIButton *goBtn;
@end

@implementation MVVMTestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.viewModel = [[TestVM alloc]init];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.viewModel.nameIsValid = YES;
    });
    
    RAC(self.viewModel,name) = [self.nameTf rac_textSignal];
    
    
    RACSignal *nameIsValidSignal = RACObserve(self.viewModel, nameIsValid);
   
    //监听nameIsValidSignal 改变gobtn透明度
    RAC(self.goBtn,alpha) =  [nameIsValidSignal map:^id(NSNumber* value) {
        return value.boolValue ? @1 :  @0.5;
    }];
    
    RAC(self.goBtn,enabled) = [nameIsValidSignal map:^id(NSNumber* value) {
        return value.boolValue ? @YES:@NO;
    }];
    
    
    [[self.goBtn rac_signalForControlEvents:UIControlEventTouchDown]subscribeNext:^(id x) {
        
    }];
    
    
    [self.nameTf.rac_textSignal subscribeNext:^(NSString* x) {
        NSLog(@"输入*%@",x);
    }];


    
}


-(UITextField *)nameTf{
    if (!_nameTf) {
        _nameTf = [UITextField new];
        [self.view addSubview:_nameTf];
        _nameTf.sd_layout.leftEqualToView(self.view).rightEqualToView(self.view)
        .topSpaceToView(self.view,20)
        .heightIs(40);
        _nameTf.backgroundColor = [UIColor grayColor];
    }
    return _nameTf;
}
-(UIButton *)goBtn{
    if (!_goBtn) {
        _goBtn = [UIButton new];
        [self.view addSubview:_goBtn];
        _goBtn.sd_layout.leftEqualToView(self.view).rightEqualToView(self.view).topSpaceToView(self.nameTf,2).heightIs(30);
        _goBtn.backgroundColor = [UIColor blackColor];
        
    }
    return _goBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
