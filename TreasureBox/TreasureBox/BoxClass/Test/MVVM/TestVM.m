//
//  TestVM.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "TestVM.h"

@implementation TestVM



- (instancetype)init
{
    self = [super init];
    if (self) {
       [self initialize];
    }
    return self;
}
- (void)initialize
{
    self.name = @"name";
    self.passwd = @"passwd";
}


@end
