//
//  ProductListVC.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/10.
//  Copyright © 2016年 Chen. All rights reserved.
//  这里协调View  与 数据的交互

#import "ProductListVC.h"
#import "UpDownButton.h"
#import "ProuductCell.h"
#import "ProductListVIew.h"
#import "ProductListVM.h"
#define CELLSHORT @"ProuductSmallCellTypeShort"
#define CELLLONG @"ProuductSmallCellTypeLong"
@interface ProductListVC (){

}
@property(nonatomic,retain) ProductListVIew *productListView ;
@property(nonatomic,retain) ProductListVM *productListVM;;


@end

@implementation ProductListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.productListView];

    [self makeViewInterface];
    [self makeViewModelInterface];
    
}
/**
 *  监听viewModel 变化  刷新View
 */
-(void)makeViewModelInterface{
    __weak typeof(self.productListView) weekproductListView = self.productListView;
    [RACObserve(self,productListVM.model.productList) subscribeNext:^(NSArray* arr) {
        weekproductListView.cltViewArr = arr;
        [weekproductListView.cltView reloadData];
    }];
    RAC(self.productListView.lineView.allstroe,isUp) =  [RACObserve(self, productListVM.model.allUp) map:^id(NSNumber* value) {
        return value.boolValue ? @YES:@NO;
    }];
    
    RAC(self.productListView.lineView.sellstore,isUp) =  [RACObserve(self, productListVM.model.sellUp) map:^id(NSNumber* value) {
        return value.boolValue ? @YES:@NO;
    }];
    
    RAC(self.productListView.lineView.pricestore,isUp) =  [RACObserve(self, productListVM.model.pricrUp) map:^id(NSNumber* value) {
        return value.boolValue ? @YES:@NO;
    }];
}



 //对View的用户交互
-(void)makeViewInterface{
   
    __weak typeof(self.productListVM) weekproductListVM = self.productListVM;
    
    [[self.productListView.lineView.allstroe.udBtn rac_signalForControlEvents:UIControlEventTouchDown]subscribeNext:^(id x) {
         //综合排序
         [weekproductListVM allstroe];
    }];
    
    [[self.productListView.lineView.sellstore.udBtn rac_signalForControlEvents:UIControlEventTouchDown]subscribeNext:^(id x) {
        //销量排序
        [weekproductListVM sellstore];
    }];
 
    
    [[self.productListView.lineView.pricestore.udBtn rac_signalForControlEvents:UIControlEventTouchDown]subscribeNext:^(id x) {
        //价格排序
        [weekproductListVM pricestore];
    }];
    self.productListView.lineView.filterBtn.rac_command = [[RACCommand alloc]initWithSignalBlock:^RACSignal *(id input) {
        //筛选
        
        
        [weekproductListVM fitter];
        return [RACSignal empty];
    }];
    
    
    
    
}


-(ProductListVM *)productListVM{
    if (!_productListVM) {
        _productListVM = [[ProductListVM alloc]init];
    }
    return _productListVM;
}

-(ProductListVIew *)productListView{
    if (!_productListView) {
        _productListView = [ProductListVIew new];
        [self.view addSubview:_productListView];
        _productListView.sd_layout.leftEqualToView(self.view).topSpaceToView(self.view,20).rightEqualToView(self.view).bottomEqualToView(self.view);
    }
    return _productListView;
}
- (void)didReceiveMemoryWarning {
}


@end
