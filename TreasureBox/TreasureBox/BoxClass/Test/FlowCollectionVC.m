
//
//  FlowCollectionVC.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/12.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "FlowCollectionVC.h"
#import "BQWaterLayout.h"
#import "FlowCollectionLayout.h"
#import "ProuductCell.h"
#import "FlowWebViewCell.h"
#import "JoyPicCell.h"
#import "FlowPictureCell.h"
#import "FlowServer.h"
#import "FlowTextWithImageCell1.h"
#import "GBAlertView.h"
#import "FlowCellManger.h"
#import <CoreImage/CoreImage.h>


@interface FlowCollectionVC ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,FlowCollectionLayoutDelegate>{
    NSArray *data;
}
@property(nonatomic,retain) UICollectionView *cltView;
@property (nonatomic, strong) FlowCollectionLayout *waterLayout;
@end

@implementation FlowCollectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
     self.cltView.backgroundColor = [UIColor lightGrayColor];
//    self.sectionArr = [[NSMutableArray alloc]init];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *path=[paths    objectAtIndex:0];
    NSString *filename=[path stringByAppendingPathComponent:@"FlowData.plist"];   //获取路径
    //读取plist
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FlowData" ofType:@"plist"];
    data = [NSArray arrayWithContentsOfFile:plistPath];
    UIButton *retrunbtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.width-50  , self.view.height-50, 50, 50)];
    retrunbtn.backgroundColor = UIColorWithRandom;
    [self.view addSubview:retrunbtn];
    [retrunbtn setTitle:@"add" forState:UIControlStateNormal];
    [[retrunbtn rac_signalForControlEvents:UIControlEventTouchDown]subscribeNext:^(id x) {
        //遍历VIew   生成数组
        FlowServer *server = [[FlowServer alloc]init];
        [self.navigationController pushViewController:server animated:YES];
        
        
    }];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(sectionrefresh:) name:@"aa" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(itemadd:) name:@"addinfo" object:nil];
}

-(void)sectionrefresh:(NSNotification*)notifi{
    [self.sectionArr addObjectsFromArray:notifi.object];
    [self.cltView reloadData];
}

-(void)itemadd:(NSNotification*)notifi{
    
//    itemdata
    NSNumber *rownum =  [notifi.object objectForKey:@"row"];
    NSNumber *sectionnum =  [notifi.object objectForKey:@"section"];
    self.sectionArr[sectionnum.integerValue][@"items"][rownum.integerValue][@"itemdata"]=notifi.object;
    [self.cltView reloadData];
    NSLog(@"%@-------------",self.sectionArr);
}

-(NSMutableArray *)sectionArr{
    if (!_sectionArr) {
        _sectionArr = [[NSMutableArray alloc]init];
    }
    return _sectionArr;
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UICollectionView *)cltView{
    if (!_cltView) {
        
        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.minimumLineSpacing = 5;
        flowLayout.itemSize = CGSizeMake(50, 50);
        flowLayout.sectionInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        
        _cltView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, 320, 200) collectionViewLayout:self.waterLayout];
        _cltView.showsHorizontalScrollIndicator = NO;
        _cltView.showsVerticalScrollIndicator = NO;
        _cltView.dataSource=self;
        _cltView.delegate=self;
        [_cltView setBackgroundColor:[UIColor lightGrayColor]];
        [_cltView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"UICollectionViewCell"];
        [self.view addSubview:_cltView];
        
        
        //这里应该使用工厂模式
        [_cltView registerClass:[FlowWebViewCell class] forCellWithReuseIdentifier:@"webViewCell"];
        [_cltView registerClass:[FlowPictureCell class] forCellWithReuseIdentifier:@"pictureCell"];
        [_cltView registerClass:[FlowTextWithImageCell1 class] forCellWithReuseIdentifier:@"titleCell"];
        _cltView.sd_layout.leftEqualToView(self.view).topSpaceToView(self.view,20).rightEqualToView(self.view).bottomEqualToView(self.view);
    }
    return _cltView;
}

- (FlowCollectionLayout *)waterLayout {
    if (_waterLayout == nil) {
        _waterLayout = [[FlowCollectionLayout alloc] init];
        _waterLayout.flowDelegate = self;
        _waterLayout.seactionSpacing = 20;
    }
    return _waterLayout;
}


#pragma mark - collocation代理
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSDictionary *dict = [self.sectionArr objectAtIndex:section];
    NSArray *arr = [dict objectForKey:@"items"];
    return arr.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"r:%ld s:%ld",(long)indexPath.row,(long)indexPath.section);

    NSNumber *itemtype =    self.sectionArr[indexPath.section][@"items"][indexPath.row][@"itemdata"][@"itemtype"];
    NSString *headicon =    self.sectionArr[indexPath.section][@"items"][indexPath.row][@"itemdata"][@"headicon"];
    NSString *picName =    self.sectionArr[indexPath.section][@"items"][indexPath.row][@"itemdata"][@"picName"];
    NSString *headtitle =    self.sectionArr[indexPath.section][@"items"][indexPath.row][@"itemdata"][@"headtitle"];
    NSString *urlStr =    self.sectionArr[indexPath.section][@"items"][indexPath.row][@"itemdata"][@"urlStr"];
    
    if (itemtype.intValue == 100) {
        //标题
        
        FlowPictureCell  *cell  = [collectionView dequeueReusableCellWithReuseIdentifier:@"pictureCell" forIndexPath:indexPath];
        cell.imageName = picName;
        return cell;
        
        
    }else if (itemtype.intValue == 101){
        //图片
        FlowTextWithImageCell1  *cell  = [collectionView dequeueReusableCellWithReuseIdentifier:@"titleCell" forIndexPath:indexPath];
        cell.icon.image = [UIImage imageNamed:headicon];
        cell.title.text = headtitle;
        return cell;
    }else if (itemtype.intValue ==102){
        //网页
        FlowWebViewCell * cell  = [collectionView dequeueReusableCellWithReuseIdentifier:@"webViewCell" forIndexPath:indexPath];
        cell.url = urlStr;
        return cell;
    }else{
        FlowPictureCell  *cell  = [collectionView dequeueReusableCellWithReuseIdentifier:@"pictureCell" forIndexPath:indexPath];
//        cell.imageName = @"full_play_btn_hl@3x";
        cell.backgroundColor = UIColorWithRandom;
        return cell;
    }
    

    return nil;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH -15)/2, (SCREEN_WIDTH/2-6)*9/6);
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.sectionArr.count;
}

-(UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath{
     UICollectionViewLayoutAttributes *attr = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    return attr;

}

-(CollectionViewItemAttribute *)itemAttributeAtIndexPath:(NSIndexPath *)indexPath{
    CollectionViewItemAttribute *ss = [[CollectionViewItemAttribute alloc]init];
    NSDictionary *dict = self.sectionArr[indexPath.section][@"items"][indexPath.row][@"itemFrame"];
    ss.widthscale = [dict[@"widthscale"] floatValue];
    ss.heightscale =  [dict[@"heightscale"] floatValue];
    ss.xpoint =  [dict[@"xpoint"] floatValue];
    ss.ypoint =  [dict[@"ypoint"] floatValue];
    ss.top =  [dict[@"top"] floatValue];
    ss.left =  [dict[@"left"] floatValue];
    ss.bottom =  [dict[@"bottom"] floatValue];
    ss.right =  [dict[@"right"] floatValue];
    return ss;
}

-(FlowCollectionSectionAtrrbute *)sectionAttributeAtIndexPath:(CGFloat)section{
    FlowCollectionSectionAtrrbute *ww = [[FlowCollectionSectionAtrrbute alloc]init];
    NSNumber *scle = [self.sectionArr objectAtIndex:section][@"scale"];
    ww.scale = scle.floatValue;
    return ww;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"点击了 section :%ld  row :%ld",(long)indexPath.section,(long)indexPath.row);
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSNumber numberWithFloat:indexPath.row],@"row",
                          [NSNumber numberWithFloat:indexPath.section],@"section",
                          nil];
    [BoxContolerSwiper switchTo:NSStringFromClass([FlowCellManger class]) form:self object:dict];
}
@end
