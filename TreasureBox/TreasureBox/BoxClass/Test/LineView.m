

//
//  LineView.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "LineView.h"

@implementation LineView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}



-(UpDownButton *)allstroe{
    if (!_allstroe) {
        _allstroe = [UpDownButton new];
        [self addSubview:_allstroe];
        _allstroe.sd_layout.leftEqualToView(self).topEqualToView(self).bottomEqualToView(self).widthIs(90);
    }
    return _allstroe;
}
-(UpDownButton *)sellstore{
    if (!_sellstore) {
        _sellstore = [UpDownButton new];
        
        [self addSubview:_sellstore];
              _sellstore.sd_layout.leftSpaceToView(self.allstroe,2).topEqualToView(self).bottomEqualToView(self).widthIs(60);
    }
    return _sellstore;
}

-(UpDownButton *)pricestore{
    if (!_pricestore) {
        _pricestore = [UpDownButton new];
        [self addSubview:_pricestore];
        _pricestore.sd_layout.leftSpaceToView(self.sellstore,2).topEqualToView(self).bottomEqualToView(self).widthIs(60);
    }
    return _pricestore;
}

-(UIButton *)filterBtn{
    if (!_filterBtn) {
        _filterBtn = [UIButton new];
        [self addSubview:_filterBtn];
        _filterBtn.sd_layout.leftSpaceToView(self.pricestore,0).topEqualToView(self).bottomEqualToView(self).widthIs(50);
    }
    return _filterBtn;
}


-(UIButton *)uiChangeBtn{
    if (!_uiChangeBtn) {
        _uiChangeBtn = [UIButton new];
        [self addSubview:_uiChangeBtn];
         _uiChangeBtn.sd_layout.centerYEqualToView(self).rightSpaceToView(self,5).heightIs(30).widthIs(30);
    }
    return _uiChangeBtn;
}


@end
