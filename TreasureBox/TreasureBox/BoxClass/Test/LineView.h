//
//  LineView.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UpDownButton.h"

@interface LineView : UIView
@property(nonatomic,retain) UpDownButton *pricestore;
@property(nonatomic,retain) UpDownButton *sellstore;
@property(nonatomic,retain) UpDownButton *allstroe;
@property(nonatomic,retain) UIButton *filterBtn;
@property(nonatomic,retain) UIButton *uiChangeBtn;


@end
