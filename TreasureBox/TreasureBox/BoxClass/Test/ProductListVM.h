//
//  ProductListVM.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/14.
//  Copyright © 2016年 Chen. All rights reserved.
//  这个文件  ProductListModel 模型 进行数据处理加工

#import <Foundation/Foundation.h>
#import "ProductListVIew.h"
#import "ProductListModel.h"


typedef void(^RspBlock)(ProductListModel *model);
@interface ProductListVM : NSObject

@property(nonatomic,retain) ProductListModel *model;

/**
 *  设置数据
 */
-(void)requestWithParamters:(NSDictionary*)dict;
/**
 *  综合排序事件处理
 *
 *  @param isUp 是否升序
 */
-(void)allstroe;
/**
 *  销量排序事件处理
 *
 *  @param isUp 是否升序
 */
-(void)sellstore;
/**
 *  价格排序事件处理
 *
 *  @param isUp 是否升序
 */
-(void)pricestore;

/**
 *  筛选事件处理
 */
-(void)fitter;

@end
