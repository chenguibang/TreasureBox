//
//  ProuductSmallCell.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductItemModel.h"

typedef enum : NSUInteger {
    ProuductSmallCellTypeShort,
    ProuductSmallCellTypeLong,
} ProuductSmallCellType;

@interface ProuductCell : UICollectionViewCell
@property(nonatomic,retain) ProductItemModel *model;
@property(nonatomic,assign) ProuductSmallCellType type;


@end
