//
//  ProuductSmallCell.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "ProuductCell.h"

@interface ProuductCell(){
    UIImageView *imageView;
    UILabel *describtion;
    UILabel *price ;
    UILabel *count;
    UILabel *activity;
}
@end

@implementation ProuductCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        imageView = [UIImageView new];
        [self.contentView addSubview:imageView];
        describtion = [UILabel new];
        [self.contentView addSubview:describtion];
        
        price  = [UILabel new];
        [self.contentView addSubview:price];
        
        count = [UILabel new];
        [self.contentView addSubview:count];
        
        
        activity = [UILabel new];
        [self.contentView addSubview:activity];
        
       
    }
    return self;
}


-(void)makeContrisShort{
    imageView.sd_layout.topEqualToView(self.contentView).leftEqualToView(self.contentView).rightEqualToView(self.contentView).heightEqualToWidth();
    imageView.backgroundColor = [UIColor blueColor];
    
    describtion.sd_layout.topSpaceToView(imageView,1).leftEqualToView(imageView).rightEqualToView(imageView).autoHeightRatio(0);
    describtion.font = [UIFont systemFontOfSize:13];
    describtion.backgroundColor = [UIColor purpleColor];
    describtion.sd_layout.maxHeightIs((self.height-self.width)*2/3);
    
    
    
    activity.backgroundColor = [UIColor redColor];
    activity.sd_cornerRadius = @(5);
    activity.font = [UIFont systemFontOfSize:13];
    activity.sd_layout.leftSpaceToView(self.contentView,2).topEqualToView(describtion).widthIs(30).heightIs(15);
    activity.textAlignment = NSTextAlignmentCenter;
    
    
    price.sd_layout.leftEqualToView(describtion).rightEqualToView(imageView).topSpaceToView(describtion,1).bottomEqualToView(self.contentView);
    price.sd_layout.minHeightIs(30);
    price.font = [UIFont systemFontOfSize:13];;
    price.textAlignment = NSTextAlignmentLeft;
    
    
    count.sd_layout.topSpaceToView(describtion,1).rightEqualToView(describtion).bottomEqualToView(self.contentView).leftEqualToView(imageView);
    count.sd_layout.minHeightIs(30);
    count.font = [UIFont systemFontOfSize:13];
    count.textAlignment = NSTextAlignmentRight;

}


-(void)makeContrisLong{
    imageView.sd_layout.topSpaceToView(self.contentView,2).leftSpaceToView(self.contentView,2).bottomSpaceToView(self.contentView,2).widthEqualToHeight();
    imageView.backgroundColor = [UIColor blueColor];
    
    describtion.sd_layout.topSpaceToView(self.contentView,30).leftSpaceToView(imageView,2).rightSpaceToView(self.contentView,2).autoHeightRatio(0);
    describtion.text = @"hahahahafds555555555555fasfasdfadsfasdfadsfsdfdsfadsfasdfha哈哈哈哈";
    describtion.font = [UIFont systemFontOfSize:13];
    describtion.backgroundColor = [UIColor purpleColor];
    describtion.sd_layout.maxHeightIs(self.height*2/3);
    
    
    
    activity.backgroundColor = [UIColor redColor];
    activity.sd_cornerRadius = @(5);
    activity.font = [UIFont systemFontOfSize:13];
    activity.sd_layout.leftSpaceToView(imageView,2).topEqualToView(describtion).widthIs(30).heightIs(15);
    activity.textAlignment = NSTextAlignmentCenter;
    
    
    price.sd_layout.leftEqualToView(describtion).rightEqualToView(self.contentView).bottomEqualToView(imageView).autoHeightRatio(0);
//    price.sd_layout.minHeightIs(30);
    price.text = @"222";
    price.font = [UIFont systemFontOfSize:13];;
    price.textAlignment = NSTextAlignmentLeft;
    
    
    count.sd_layout.rightEqualToView(describtion).bottomEqualToView(imageView).leftEqualToView(imageView).autoHeightRatio(0);
    count.text = @"dasdf";
//    count.sd_layout.minHeightIs(30);
    count.font = [UIFont systemFontOfSize:13];
    count.textAlignment = NSTextAlignmentRight;
    
}

-(void)setType:(ProuductSmallCellType)type{
    _type = type;
    
    if (type == ProuductSmallCellTypeShort) {
        [self makeContrisShort];
    }else if (type == ProuductSmallCellTypeLong){
        [self makeContrisLong];
    }
    
}


-(void)setModel:(ProductItemModel *)model{
    _model = model;
    count.text = model.count;
    price.text = model.price;
    if (model.activity.length>0) {
        activity.hidden = NO;
        activity.text = model.activity;
        describtion.text = [NSString stringWithFormat:@"           %@",model.describe];
    }else{
        activity.hidden = YES;
        activity.text = model.activity;
        describtion.text = model.describe;
    }
}
@end
