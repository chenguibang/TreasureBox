//
//  ProductListVIew.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LineView.h"
#import "ProuductCell.h"

@interface ProductListVIew : UIView<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>{
    ProuductSmallCellType type;
}
@property(nonatomic,retain) LineView *lineView;
@property(nonatomic,retain) UICollectionView *cltView;
@property(nonatomic,retain) NSArray *cltViewArr;
-(void)refreshView;
@end
