//
//  ChatControler.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/5.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "ChatControler.h"
#import "XMPPCatCell.h"
#import "XMPPCatCellLayout.h"

@interface ChatControler (){
    NSArray *msgs;
    NSMutableArray<XMPPCatCellLayout *> *layouts;
}

@end

@implementation ChatControler

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 100, 270, 300)];
    UIImage *bullueImage = [UIImage imageNamed:@"SenderAppNodeBkg_HL"];
    imageView.image= [bullueImage stretchableImageWithLeftCapWidth:bullueImage.size.width/2 topCapHeight:bullueImage.size.height/2];
 
    [self.view addSubview:imageView];
    
    [self creatView];
    [self makeConstri];
   
    
    layouts = [[NSMutableArray alloc]init];
    msgs = @[
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             @"XMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayoutXMPPCatCellLayout",
             ];
    
    for (NSString *msg in msgs) {
         XMPPCatCellLayout *layout = [[XMPPCatCellLayout alloc]init];
        XMPPCatCellModel *model = [[XMPPCatCellModel alloc]init];
        model.msg = msg;
        layout.model = model;
        [layouts addObject:layout];
    }
    

    
    
}

-(void)creatView{
    self.msgTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:self.msgTable];
    self.msgTable.delegate = self;
    self.msgTable.dataSource = self;
    [self.msgTable registerClass:[XMPPCatCell class] forCellReuseIdentifier:@"cell"];
    
}

-(void)makeConstri{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}








- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return layouts.count;
}
    
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    XMPPCatCellLayout *lay = layouts[indexPath.row];
    return lay.cellHeight;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    XMPPCatCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    XMPPCatCellLayout *lay = layouts[indexPath.row];
    cell.layout = lay;
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    
     [self.msgTable reloadData];
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    NSLog(@"旋转后屏幕宽度  %f",SCREEN_WIDTH);
    [self.view layoutIfNeeded];
    [self.view setNeedsLayout];
    
    self.msgTable.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
   
}

@end
