//
//  XMPPCatCellModel.h
//  TreasureBox
//
//  Created by chenguibang on 16/7/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMPPCatCellModel : NSObject
@property(nonatomic,copy)NSString *msg;
@property(nonatomic,assign)BOOL senderME;
@end
