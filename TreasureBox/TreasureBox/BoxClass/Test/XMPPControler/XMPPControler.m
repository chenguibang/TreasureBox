//
//  XMPPControler.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/4.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "XMPPControler.h"
#import "XMPPUtil.h"

@interface XMPPControler ()

@end

@implementation XMPPControler

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender {
    [XMPPUtil defultUtil].name = self.nameTF.text;
    [XMPPUtil defultUtil].pwd = self.pwdTF.text;
    [[XMPPUtil defultUtil] login];
    
}


- (IBAction)regist:(id)sender {
    [XMPPUtil defultUtil].name = self.nameTF.text;
    [XMPPUtil defultUtil].pwd = self.pwdTF.text;
    [[XMPPUtil defultUtil] regiest];
}
- (IBAction)addFriend:(id)sender {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[XMPPUtil defultUtil]  addFriend:self.friendTF.text];
    });
    
    
}
- (IBAction)sendmsg:(id)sender {
    [[XMPPUtil defultUtil] sendMessage:self.msg.text toUser:self.msgFriend.text];
}
- (IBAction)fetchFriends:(id)sender {
    
    [[XMPPUtil defultUtil] fetchFriends];
}
- (IBAction)makeRoom:(id)sender {
    [[XMPPUtil defultUtil] makeRoom:self.makeRoomName.text];
}
- (IBAction)joinRoom:(id)sender {
    [[XMPPUtil defultUtil] joinRoom:self.joinRoomName.text];
}
- (IBAction)getRooms:(id)sender {
    [[XMPPUtil defultUtil] getExistRoom];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
