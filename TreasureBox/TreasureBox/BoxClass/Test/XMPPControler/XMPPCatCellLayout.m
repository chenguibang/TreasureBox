//
//  XMPPCatCellLayout.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "XMPPCatCellLayout.h"
#import "YYTextLayout.h"

@implementation XMPPCatCellLayout



-(instancetype)initWithXMPPCatCellModel:(XMPPCatCellModel *)model{
    self = [super init];
    if (self) {
        
    }
    
    return self;
}

-(void)setModel:(XMPPCatCellModel *)model{
    _model = model;

    self.iconSize = CGSizeMake(50, 50);
    UIFont *sysFont = [UIFont systemFontOfSize:15];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:model.msg];
    YYTextContainer *container = [YYTextContainer new];
    container.size = CGSizeMake(SCREEN_WIDTH - self.iconSize.width*2 -30, MAXFLOAT);
    container.insets = UIEdgeInsetsMake(0, 10, 0, 10);
    
    YYTextLayout *telayout =[YYTextLayout layoutWithContainer:container text:text];
    
    self.msgSize = CGSizeMake(container.size.width, telayout.rowCount*sysFont.pointSize);
  
    NSLog(@"高度 -- %f",self.cellHeight);
    self.bgImageSize = CGSizeMake(self.msgSize.width+40, self.msgSize.height+20);
    
      self.cellHeight = self.bgImageSize.height;
    
    if (self.cellHeight<self.iconSize.height + 10) {
        self.cellHeight = self.iconSize.height + 10;
    }
    
    
    
}

-(void)modifyLines:(NSArray<YYTextLine *> *)lines fromText:(NSAttributedString *)text inContainer:(YYTextContainer *)container{
    
}

@end
