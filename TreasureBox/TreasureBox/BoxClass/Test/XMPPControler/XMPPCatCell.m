//
//  XMPPCatCell.m
//  TreasureBox
//
//  Created by chenguibang on 16/8/7.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "XMPPCatCell.h"
#import "MLEmojiLabel.h"
#import "XMPPCatCellModel.h"

@implementation XMPPCatCell
/**
 *
 *
 *  @param style           style description
 *  @param reuseIdentifier reuseIdentifier @"MyCell" @"OtherCell"
 *
 *  @return
 */
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self creatView];
//        [self makeConstri];
    }
    return self;
}

-(void)creatView{
    self.timeLb = [UILabel new];
    [self.contentView addSubview:self.timeLb];
    self.timeLb.font = FONT_SMALL_11;
    self.timeLb.textAlignment = NSTextAlignmentCenter;
    self.icon = [UIImageView new];
    [self.contentView addSubview:self.icon];
    self.msgBgView = [UIImageView new];
    [self.contentView addSubview:self.msgBgView];
    
    self.msgImageView = [[UIImageView alloc]init];
    [self.msgBgView  addSubview:self.msgImageView];
    
    self.msgLb = [UILabel new];
    self.msgLb.font = [UIFont systemFontOfSize:14];
    self.msgLb.textColor = UIColorHex(0x666666);
//     self.msgLb.delegate =self;
//    self.msgLb.textInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    self.msgLb.userInteractionEnabled = YES;
    self.msgLb.numberOfLines = 0;
    [self.msgLb setSingleLineAutoResizeWithMaxWidth:200];
    self.msgLb.backgroundColor =[UIColor orangeColor];
//    self.msgLb.isNeedAtAndPoundSign = YES;
//    self.msgLb.disableEmoji = NO;
    
//    self.msgLb.lineSpacing = 3.0f;
    
//    self.msgLb.verticalAlignment = TTTAttributedLabelVerticalAlignmentCenter;
    [self.msgBgView  addSubview:self.msgLb];
    self.msgBgView.userInteractionEnabled = YES;
//    self.msgBgView.exclusiveTouch
}

-(void)makeConstri{
    self.timeLb.sd_layout.leftEqualToView(self.contentView)
    .rightEqualToView(self.contentView)
    .topEqualToView(self.contentView)
    .heightIs(15);
    
    
    
    
    self.icon.sd_layout.topSpaceToView(self.timeLb,0)
    .rightSpaceToView(self.contentView,5)
    .widthIs(50)
    .heightEqualToWidth();
//    self.icon.sd_cornerRadiusFromHeightRatio = @(0.5);
    
    self.msgBgView.sd_layout.topSpaceToView(self.timeLb,0)
    .leftSpaceToView(self.contentView,40)
    .rightSpaceToView(self.icon,5);
    UIImage *bullueImage = [UIImage imageNamed:@"SenderAppNodeBkg_HL"];
    self.msgBgView.image= [bullueImage stretchableImageWithLeftCapWidth:bullueImage.size.width/2 topCapHeight:bullueImage.size.height/2];
    self.msgImageView.sd_layout
    .leftSpaceToView(self.msgBgView,5)
    .topSpaceToView(self.msgBgView,5)
    .rightSpaceToView(self.msgBgView,5)
    .heightIs(0);
    
    self.msgLb.numberOfLines=0;
   
    self.msgLb.sd_layout
    .leftSpaceToView(self.msgBgView,10)
    .topSpaceToView(self.msgImageView,5)
    .rightSpaceToView(self.msgBgView,20)
    .autoHeightRatio(0);

    
    [self.msgBgView setupAutoHeightWithBottomView:self.msgLb bottomMargin:15];
    
    [self setupAutoHeightWithBottomViewsArray:@[self.msgBgView,self.icon] bottomMargin:5];
    
    UITapGestureRecognizer *tapG = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    tapG.delegate = self;
    [self.contentView addGestureRecognizer:tapG];
}
- (void)tap
{
    NSLog(@"tapped");
}

#pragma mark - gesture delegate



- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
//    return ![self.msgLb containslinkAtPoint:[touch locationInView:self.msgLb]];
    return YES;
}



-(void)setLayout:(XMPPCatCellLayout *)layout{
    self.model = layout.model;
    
    self.icon.size = layout.iconSize;
    self.icon.mj_x = SCREEN_WIDTH - self.icon.size.width-10;
    self.icon.mj_y = 5;
    
    
    self.msgBgView.size = layout.bgImageSize;
    self.msgBgView.mj_y = self.icon.mj_y;
    self.msgBgView.mj_x = SCREEN_WIDTH-self.icon.width - 10 - self.msgBgView.size.width ;
    
    self.msgLb.size = layout.msgSize;
    self.msgLb.mj_x = 15;
    self.msgLb.mj_y = 0;
    

}

-(void)setModel:(XMPPCatCellModel *)model{
    
    _model = model;
    
    YYTextContainer *container = [YYTextContainer new];
    container.size = CGSizeMake(230, MAXFLOAT);
    container.insets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:model.msg];
    YYTextLayout *telayout =[YYTextLayout layoutWithContainer:container text:text];
//    self.msgLb.textLayout = telayout;
    
//    self.msgLb.attributedText = text;
    [self.msgLb setText:model.msg];
    
    self.icon.image = [UIImage imageNamed:@"13"];
    UIImage *bubble = [UIImage imageNamed:@"SenderAppNodeBkg_HL"];
    self.msgBgView.image = [bubble stretchableImageWithLeftCapWidth:floorf(bubble.size.width/2) topCapHeight:floorf(bubble.size.height/2)];

}


- (void)mlEmojiLabel:(MLEmojiLabel*)emojiLabel didSelectLink:(NSString*)link withType:(MLEmojiLabelLinkType)type{
    switch(type){
        case MLEmojiLabelLinkTypeURL:
            NSLog(@"点击了链接%@",link);
            break;
        case MLEmojiLabelLinkTypePhoneNumber:
            NSLog(@"点击了电话%@",link);
            break;
        case MLEmojiLabelLinkTypeEmail:
            NSLog(@"点击了邮箱%@",link);
            break;
        case MLEmojiLabelLinkTypeAt:
            NSLog(@"点击了用户%@",link);
            break;
        case MLEmojiLabelLinkTypePoundSign:
            NSLog(@"点击了话题%@",link);
            break;
        default:
            NSLog(@"点击了不知道啥%@",link);
            break;
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
