//
//  XMPPChatTextView.h
//  TreasureBox
//
//  Created by chenguibang on 16/7/13.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XMPPChatTextView : UITextView
@property(nonatomic,copy)NSString *placeHolder;

@end
