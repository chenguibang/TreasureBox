//
//  XMPPChatModel.h
//  TreasureBox
//
//  Created by chenguibang on 16/7/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMPPChatModel : NSObject
@property(nonatomic,copy)NSString* msgContent;
@property(nonatomic,copy)NSString* iconName;
@property(nonatomic,assign)CGFloat cellHight;

@end
