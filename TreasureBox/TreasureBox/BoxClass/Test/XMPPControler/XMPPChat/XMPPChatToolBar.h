//
//  XMPPChatToolBar.h
//  TreasureBox
//
//  Created by chenguibang on 16/7/12.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMPPChatTextView.h"

@interface XMPPChatToolBar : UIView
@property(nonatomic,strong)NSMutableArray<UIButton*> *leftBtns;
@property(nonatomic,strong)NSMutableArray<UIButton*> *rightBtns;
@property(nonatomic,strong)XMPPChatTextView *chatTextView;

@end
