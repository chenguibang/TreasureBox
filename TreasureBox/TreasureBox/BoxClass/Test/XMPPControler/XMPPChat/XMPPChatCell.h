//
//  XMPPChatCell.h
//  TreasureBox
//
//  Created by chenguibang on 16/7/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMPPChatModel.h"

@interface XMPPChatCell : UITableViewCell
@property(nonatomic,strong)UILabel *timeLb;
@property(nonatomic,strong)UIImageView *icon;
@property(nonatomic,strong)UIImageView *msgBgView;
@property(nonatomic,strong)UIImageView *msgImageView;
@property(nonatomic,strong)UILabel *msgLb;
@property(nonatomic,strong)XMPPChatModel *model;


@end
