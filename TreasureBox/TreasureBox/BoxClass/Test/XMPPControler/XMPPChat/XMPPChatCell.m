//
//  XMPPChatCell.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "XMPPChatCell.h"

@implementation XMPPChatCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self creatView];
//        [self layout];
        [self maslayout];
        
        
        
    }
    return self;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)creatView{
    self.timeLb = [UILabel new];
    [self.contentView addSubview:self.timeLb];
    self.timeLb.numberOfLines = 0;
    self.timeLb.font = FONT_SMALL_11;
    self.timeLb.textAlignment = NSTextAlignmentCenter;
    self.icon = [UIImageView new];
    [self.contentView addSubview:self.icon];
    self.msgBgView = [UIImageView new];
    [self.contentView addSubview:self.msgBgView];
    
//    self.msgImageView = [[UIImageView alloc]init];
//    [self.msgBgView  addSubview:self.msgImageView];
    
    self.msgLb = [UILabel new];
    self.msgLb.font = [UIFont systemFontOfSize:14];
    self.msgLb.textColor = UIColorHex(0x666666);
    self.msgLb.userInteractionEnabled = YES;
    self.msgLb.numberOfLines = 0;
    [self.msgBgView  addSubview:self.msgLb];
//    self.msgLb.backgroundColor = [UIColor orangeColor];
//    self.msgBgView.userInteractionEnabled = YES;
}


-(void)maslayout{
    
    if ([self.reuseIdentifier isEqualToString:@"myCell"]) {
        [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            
        }];
        
        [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).with.offset(-10);
            make.top.equalTo(self.timeLb.mas_bottom).with.offset(0);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(50);
            
        }];
        
        [self.msgLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.lessThanOrEqualTo(self.contentView.mas_width).offset(-100);
            make.right.equalTo(self.icon.mas_left).with.offset(-10);
            make.top.equalTo(self.timeLb.mas_bottom).with.offset(10);
            
            make.height.greaterThanOrEqualTo(self.icon.mas_height).offset(-30);
        }];
        
        
        [self.msgBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.edges.equalTo(self.msgLb).insets(UIEdgeInsetsMake(-10, -10, -20, -15));
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-5);
        }];
        
        UIImage *bubble = [UIImage imageNamed:@"SenderAppNodeBkg_HL"];
        self.msgBgView.image = [bubble stretchableImageWithLeftCapWidth:floorf(bubble.size.width/2) topCapHeight:floorf(bubble.size.height/2)];

    }else{
        
        [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            
        }];
        
        [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset(10);
            make.top.equalTo(self.timeLb.mas_bottom).with.offset(0);
            make.width.mas_equalTo(50);
            make.height.mas_equalTo(50);
            
        }];
        
        [self.msgLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.lessThanOrEqualTo(self.contentView.mas_width).offset(-100);
            make.left.equalTo(self.icon.mas_right).with.offset(10);
            make.top.equalTo(self.timeLb.mas_bottom).with.offset(10);
            
            make.height.greaterThanOrEqualTo(self.icon.mas_height).offset(-30);
        }];
        
        
        [self.msgBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.edges.equalTo(self.msgLb).insets(UIEdgeInsetsMake(-10, -10, -20, -15));
            make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-5);
        }];

        
        UIImage *bubble = [UIImage imageNamed:@"ReceiverTextNodeBkg"];
        self.msgBgView.image = [bubble stretchableImageWithLeftCapWidth:floorf(bubble.size.width/2) topCapHeight:floorf(bubble.size.height/2)];
        
        
    }
    
 
    
    
}


-(void)layout{
    
//    if ([self.reuseIdentifier isEqualToString:@"myCell"]) {
        self.timeLb.sd_layout.topEqualToView(self.contentView)
        .leftEqualToView(self.contentView)
        .rightEqualToView(self.contentView)
        .autoHeightRatio(0);
        
        self.icon.sd_layout
        .rightSpaceToView(self.contentView,5)
        .topSpaceToView(self.timeLb,5)
        .widthIs(50)
        .heightIs(50);
    
    
    
    
        self.msgBgView.sd_layout
//        .leftSpaceToView(self.contentView,25)
        .topSpaceToView(self.contentView,10)
//        .bottomEqualToView(self.contentView)
        .rightSpaceToView(self.icon,5)
        .widthRatioToView(self.msgLb,1.2);
    
    
//        [self.msgBgView setupAutoWidthWithRightView:self.msgLb rightMargin:5];
    [self.msgBgView setupAutoHeightWithBottomView:self.msgLb bottomMargin:15];
    
        self.msgLb.sd_layout
//    .leftSpaceToView(self.contentView,40)
        .topSpaceToView(self.msgBgView,5)
        .rightSpaceToView(self.msgBgView,5)
        .autoHeightRatio(0);
        [self.msgLb setSingleLineAutoResizeWithMaxWidth:SCREEN_WIDTH-100];
        UIImage *bubble = [UIImage imageNamed:@"SenderAppNodeBkg_HL"];
        self.msgBgView.image = [bubble stretchableImageWithLeftCapWidth:floorf(bubble.size.width/2) topCapHeight:floorf(bubble.size.height/2)];

        
    [self setupAutoHeightWithBottomViewsArray:@[self.msgBgView,self.icon] bottomMargin:15];
    
        
        

//    }else{
    
        
        
        
        
//    }
    
    
}

-(void)layoutOtherCell{
    
}


-(void)setModel:(XMPPChatModel *)model{
    _model = model;
    self.msgLb.text = model.msgContent;
    self.icon.image = [UIImage imageNamed:model.iconName];
    self.timeLb.text = @"2016年07月12日10:28:26";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
