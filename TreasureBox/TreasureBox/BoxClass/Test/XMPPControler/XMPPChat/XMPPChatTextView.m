//
//  XMPPChatTextView.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/13.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "XMPPChatTextView.h"

@implementation XMPPChatTextView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didReceiveTextDidChangeNotification:)
                                                     name:UITextViewTextDidChangeNotification
                                                   object:self];
    }
    return self;
}

-(void)didReceiveTextDidChangeNotification:(NSNotification*)notifition{
    [self setNeedsDisplay];
    
    
}

-(void)dealloc{
     [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:self];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
