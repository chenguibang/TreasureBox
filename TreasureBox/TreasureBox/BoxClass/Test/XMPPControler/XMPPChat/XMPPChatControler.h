//
//  XMPPChatControler.h
//  TreasureBox
//
//  Created by chenguibang on 16/7/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMPPChatToolBar.h"

@interface XMPPChatControler : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *msgTable;
@property(nonatomic,strong) XMPPChatToolBar *chatToolBar;
@end
