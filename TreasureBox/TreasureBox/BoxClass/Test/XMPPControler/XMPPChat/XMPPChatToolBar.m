//
//  XMPPChatToolBar.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/12.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "XMPPChatToolBar.h"

@implementation XMPPChatToolBar

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = UIColorWithRGB(245, 245, 245);
        
    }
    return self;
}



-(void)setLeftBtns:(NSMutableArray *)leftBtns{
    _leftBtns = leftBtns;
    
    for (UIButton* leftBtn in leftBtns) {
         [self addSubview:leftBtn];
        
        NSUInteger index =   [leftBtns indexOfObject:leftBtn];
        if (index == 0) {
            [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).with.offset(5);
                make.bottom.equalTo(self).with.offset(-5);
                make.width.mas_equalTo(30);
                make.height.mas_equalTo(30);
            }];
        }else{
            UIButton *lastBtn = leftBtns[index-1];
            [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastBtn.mas_right).with.offset(5);
                make.bottom.equalTo(self).with.offset(-5);
                make.width.mas_equalTo(30);
                make.height.mas_equalTo(30);
            }];
        }
       
    }

    
}

-(void)setRightBtns:(NSMutableArray *)rightBtns{
    _rightBtns = rightBtns;
    for (UIButton *rightBtn in rightBtns) {
        [self addSubview:rightBtn];
         NSUInteger index =   [rightBtns indexOfObject:rightBtn];
        if (index == 0) {
            [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self).with.offset(-5);
                make.bottom.equalTo(self).with.offset(-5);
                make.width.mas_equalTo(30);
                make.height.mas_equalTo(30);
            }];
        }else{
            UIButton *lastBtn = rightBtns[index-1];
            [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(lastBtn.mas_left).with.offset(-5);
                make.bottom.equalTo(self).with.offset(-5);
                make.width.mas_equalTo(30);
                make.height.mas_equalTo(30);
            }];
        }
        
        
    }
    
}

-(void)setChatTextView:(XMPPChatTextView *)chatTextView{
    _chatTextView = chatTextView;
    [self addSubview:_chatTextView];
    [_chatTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).with.offset(-5);
        if (self.leftBtns.count>0) {
            UIButton *lastBtn = [self.leftBtns lastObject];
            make.left.equalTo(lastBtn.mas_right).with.offset(5);
        }else{
            make.left.equalTo(self).with.offset(5);
        }
        
        if (self.rightBtns.count>0) {
            UIButton *lastBtn = [self.rightBtns lastObject];
            make.right.equalTo(lastBtn.mas_left).with.offset(-5);
        }else{
            make.right.equalTo(self).with.offset(-5);
        }
        make.height.greaterThanOrEqualTo(@(35));
        
        
    }];
}



- (void)drawRect:(CGRect)rect {
    UIColor *dark = [UIColor colorWithWhite:0 alpha:0.5];
    UIColor *clear = [UIColor colorWithWhite:0 alpha:0.2];
    NSArray *colors = @[(id)clear.CGColor,(id)dark.CGColor, (id)clear.CGColor];
    NSArray *locations = @[@0.2, @0.5, @0.8];
    
    CAGradientLayer  *_line1 = [CAGradientLayer layer];
    _line1.colors = colors;
    _line1.locations = locations;
    _line1.startPoint = CGPointMake(0, 0);
    _line1.endPoint = CGPointMake(0, 1);
    _line1.frame = CGRectMake(0, 0, rect.size.width, 0.5);
    [self.layer addSublayer:_line1];

}

@end
