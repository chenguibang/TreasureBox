//
//  XMPPChatControler.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "XMPPChatControler.h"
#import "XMPPChatCell.h"
#import "UITableView+FDTemplateLayoutCell.h"
#import "XMPPChatToolBar.h"

@interface XMPPChatControler (){
    NSMutableArray *msgs;
}

@end

@implementation XMPPChatControler

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self creatView];
}

-(void)creatView{
    self.msgTable = [[UITableView alloc]initWithFrame:CGRectZero];
    [self.view addSubview:self.msgTable];
    
    
    
    self.chatToolBar = [XMPPChatToolBar new];
    [self.view addSubview:self.chatToolBar];
    
    
    UIButton *btn = [[UIButton alloc]init];
    [btn setImage:[UIImage imageNamed:@"11"] forState:UIControlStateNormal];
    [btn setTintColor:[UIColor orangeColor]];
    
    
    UIButton *btn1 = [[UIButton alloc]init];
    [btn1 setImage:[UIImage imageNamed:@"11"] forState:UIControlStateNormal];
    [btn1 setShowsTouchWhenHighlighted:YES];
    UIButton *btn2 = [[UIButton alloc]init];
    [btn2 setImage:[UIImage imageNamed:@"11"] forState:UIControlStateNormal];
    
    btn2.reversesTitleShadowWhenHighlighted = YES;
    UIButton *btn3 = [[UIButton alloc]init];
    [btn3 setImage:[UIImage imageNamed:@"11"] forState:UIControlStateNormal];
    btn3.showsTouchWhenHighlighted = YES;
    

    UIButton *btn4 = [[UIButton alloc]init];
    [btn4 setImage:[UIImage imageNamed:@"11"] forState:UIControlStateNormal];
    

    UIButton *btn5 = [[UIButton alloc]init];
    [btn5 setImage:[UIImage imageNamed:@"11"] forState:UIControlStateNormal];
    
    self.chatToolBar.leftBtns = (NSMutableArray*)@[btn
//                                                   ,btn1,btn2,btn3
                                                   ];
    self.chatToolBar.rightBtns = (NSMutableArray*)@[btn4,btn5];
    XMPPChatTextView *chat = [XMPPChatTextView new];
    self.chatToolBar.chatTextView = chat;
    self.chatToolBar.sd_layout.leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .heightIs(44)
    .yIs(SCREEN_HEIGHT-44);
    
    
    
    
    
    self.msgTable.delegate = self;
    self.msgTable.dataSource = self;
    [self.msgTable registerClass:[XMPPChatCell class] forCellReuseIdentifier:@"myCell"];
    [self.msgTable registerClass:[XMPPChatCell class] forCellReuseIdentifier:@"otherCell"];
    self.msgTable.sd_layout
    .topEqualToView(self.view)
    .leftEqualToView(self.view)
    .bottomSpaceToView(self.chatToolBar,0)
    .rightEqualToView(self.view);
    
    
    
    
    
    
    msgs = [[NSMutableArray alloc]init];
    
    
    NSArray *ms = @[
                    @"【环球网综合报道】据美联社7月10日报道，纵使网上呼吁就英国脱欧再次进行公投的签名已超过400万，但德国总理默克尔表示，她预计英国脱欧申请将获得通过。",
                    @"　据报道，网上部分要求再次公投签名的合法性已经受到质疑。10日， 默克尔在接受德国电视二台的年度夏季采访时说：“我正在解决现实问题，我笃定这个申请(英国脱欧)将会通过。”",
                    @"在成都学做宫保鸡丁、在天津乘坐京津高铁、在西安与兵马俑“亲密接触”、在广州参观教堂、在合肥与村民唠家常……从华南到华北，从学做宫保鸡丁、在天津乘坐京津高铁、在西安与兵马俑“亲密接触”、在广州参四川盆地到黄土高坡，对这位德国领导人来说，经济发展水平不同、地域文化总理的足迹踏过中国很多地方，她本次选择沈阳的重要原因之德国驻华大使柯慕贤",
                    @"学做宫保鸡丁、在天津乘坐京津高铁、在西安与兵马俑“亲密接触”、在广州参观教堂、在合肥与村民唠家常……从华南到华北，从四川盆地到黄土高坡，对这位德国领导人来说，经济发展水平不同、地域文化总理的足迹踏过中",
                    @"学做宫保鸡丁、在天津乘坐京津高铁、在西安与兵马俑“亲密接触”、在广州参观教堂、在合肥与村民唠家常……从华南到华北，从四川盆地到黄土高坡，对这位德国领导人来说，经济发展水平不同、地域文化总理四川盆地到黄土高坡，对这位德国领导人来说，经济发展水平不同、地域文化总理的足迹踏过中",
                    @"学做宫保鸡丁、在天津乘坐京津高铁、在西安与兵马俑“亲密接触”、在广州参观教堂、在合肥与村民唠家常……从华南到华北，从四川盆地到黄土高坡，对这位德国领导人来说，经济发展水平不同、地域文化总理的足迹踏过中",
                    @"学坐京津高铁观教堂、在合肥与村民唠家常……从华南到华北，从四川盆地到黄土高坡，对这位德国领导人来说，经济发展水平不同、地域文化总理的足迹踏过中",
                    @"学做宫保鸡丁、在天黄土高坡，对这位德国领导人来说，经济发展水平不同、地域文化总理的足迹踏过中",
                    @"学做宫保鸡丁、在天津乘坐京津高铁、在西安与兵马俑“亲密接触”、在广州参观教堂、在合肥与村民唠家常……从华南到华北，从四川盆地到黄土高坡，对这位德国领导人来说，经济发展水平不同、地域文化总理的足迹踏过中",
                    @"学做宫保",
                    
                    ];
    
    
    for (int i =0 ; i<ms.count; i++) {
        XMPPChatModel *model = [[XMPPChatModel alloc]init];
        model.msgContent = ms[i];
        model.iconName = @"13";
        [msgs addObject:model];
    }
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return msgs.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row%2 == 0) {
        return [self.msgTable fd_heightForCellWithIdentifier:@"myCell" cacheByIndexPath:indexPath configuration:^(XMPPChatCell *cell) {
            
            // 在这个block中，重新cell配置数据源
            [self setupModelOfCell:cell atIndexPath:indexPath];
        }];
    }else{
        return [self.msgTable fd_heightForCellWithIdentifier:@"otherCell" cacheByIndexPath:indexPath configuration:^(XMPPChatCell *cell) {
            
            // 在这个block中，重新cell配置数据源
            [self setupModelOfCell:cell atIndexPath:indexPath];
        }];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    XMPPChatModel *model = msgs[indexPath.row];
    XMPPChatCell *cell = nil;
    if (indexPath.row%2 == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    }else{
       cell = [tableView dequeueReusableCellWithIdentifier:@"otherCell" forIndexPath:indexPath];
    }
    
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    [cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
//    cell.sd_tableView = tableView;
//    cell.sd_indexPath = indexPath;
    cell.model = model;
    
    [self setupModelOfCell:cell atIndexPath:indexPath];
    return cell;
}
- (void)setupModelOfCell:(XMPPChatCell *) cell atIndexPath:(NSIndexPath *) indexPath {
    
    // 采用计算frame模式还是自动布局模式，默认为NO，自动布局模式
    //    cell.fd_enforceFrameLayout = NO;
    XMPPChatModel *model = msgs[indexPath.row];
    cell.model = model;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    
}


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self.msgTable reloadData];
}

@end
