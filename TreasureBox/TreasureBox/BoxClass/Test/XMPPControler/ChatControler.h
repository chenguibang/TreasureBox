//
//  ChatControler.h
//  TreasureBox
//
//  Created by chenguibang on 16/7/5.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatControler : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *msgTable;
@end

