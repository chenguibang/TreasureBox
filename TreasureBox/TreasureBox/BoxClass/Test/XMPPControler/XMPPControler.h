
//
//  XMPPControler.h
//  TreasureBox
//
//  Created by chenguibang on 16/7/4.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XMPPControler : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *pwdTF;
@property (weak, nonatomic) IBOutlet UITextField *friendTF;
@property (weak, nonatomic) IBOutlet UITextField *msgFriend;
@property (weak, nonatomic) IBOutlet UITextField *msg;
@property (weak, nonatomic) IBOutlet UITextField *makeRoomName;
@property (weak, nonatomic) IBOutlet UITextField *joinRoomName;

@end
