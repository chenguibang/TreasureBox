//
//  XMPPCatCellLayout.h
//  TreasureBox
//
//  Created by chenguibang on 16/7/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPCatCellModel.h"

@interface XMPPCatCellLayout : NSObject<YYTextLinePositionModifier>
@property(nonatomic,strong) XMPPCatCellModel *model;
@property(nonatomic,assign) CGFloat cellHeight;



@property(nonatomic,assign) CGSize iconSize;


@property(nonatomic,assign) CGSize msgSize;

@property(nonatomic,assign) CGSize bgImageSize;



-(instancetype)initWithXMPPCatCellModel:(XMPPCatCellModel*)model;
@end
