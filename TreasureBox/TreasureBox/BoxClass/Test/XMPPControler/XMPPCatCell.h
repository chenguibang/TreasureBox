//
//  XMPPCatCell.h
//  TreasureBox
//
//  Created by chenguibang on 16/8/7.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYLabel.h"
#import "MLEmojiLabel.h"
#import "XMPPCatCell.h"
#import "XMPPCatCellModel.h"
#import "XMPPCatCellLayout.h"







@interface XMPPCatCell : UITableViewCell<MLEmojiLabelDelegate,UIGestureRecognizerDelegate>
@property(nonatomic,strong)UILabel *timeLb;
@property(nonatomic,strong)UIImageView *icon;
@property(nonatomic,strong)UIImageView *msgBgView;
@property(nonatomic,strong)UIImageView *msgImageView;
@property(nonatomic,strong)UILabel *msgLb;
@property(nonatomic,strong)XMPPCatCellModel *model;
@property(nonatomic,strong)XMPPCatCellLayout *layout;
@end
