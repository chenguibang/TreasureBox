//
//  ProductListVM.m
//  TreasureBox
//
//  Created by chenguibang on 16/3/14.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "ProductListVM.h"
#define KVOKEYPATH_LISTMODEL @"KVOKEYPATH_LISTMODEL"
@implementation ProductListVM


-(ProductListModel *)model{
    if (!_model) {
        _model = [[ProductListModel alloc]init];
        
        [RACObserve(self, model.allUp) subscribeNext:^(NSNumber* x) {
            if (x.boolValue) {
                _model.sellUp = NO;
                _model.pricrUp = NO;
            }
        }];
        
        [RACObserve(self, model.sellUp) subscribeNext:^(NSNumber* x) {
            if (x.boolValue) {
                _model.allUp = NO;
                _model.pricrUp = NO;
            }
        }];
        
        [RACObserve(self, model.pricrUp) subscribeNext:^(NSNumber* x) {
            if (x.boolValue) {
                _model.sellUp = NO;
                _model.allUp = NO;
            }
        }];
    }
    return _model;
}


-(void)requestWithParamters:(NSDictionary *)dict{
    //假如请求到数据了
    //模拟数据请求
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        
    });
}





-(void)allstroe{
     self.model.allUp = !self.model.allUp;
    
    
    //模拟全部排序数据请求
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //请求到数据后刷新model属性
        self.model.productList = [NSArray arrayWithObjects:@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",nil];
        
        
        
        //请求成功后更改按钮的状态
       
        //若请求失败
//        self.model.allUp = NO;
        
        
        
    });
    
}
-(void)sellstore{
     self.model.sellUp = !self.model.sellUp;
    //模拟销量排序数据请求
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //请求到数据后刷新model属性
        self.model.productList = [NSArray arrayWithObjects:@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",nil];
        
    });
    
   
    
}
-(void)pricestore{
    self.model.pricrUp = !self.model.pricrUp;
    //模拟价格排序数据请求
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //请求到数据后刷新model属性
        self.model.productList = [NSArray arrayWithObjects:@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",nil];
        
    });
    
    
}
-(void)fitter{
    //模拟筛选数据请求
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //请求到数据后刷新model属性
        self.model.productList = [NSArray arrayWithObjects:@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq", @"aa",@"qq",@"qq", @"aa",@"qq",@"qq", @"aa",@"qq",@"qq", @"aa",@"qq",@"qq", @"aa",@"qq",@"qq", @"aa",@"qq",@"qq", @"aa",@"qq",@"qq", @"aa",@"qq",@"qq", @"aa",@"qq",@"qq", @"aa",@"qq",@"qq", @"aa",@"qq",@"qq", @"aa",@"qq",nil];
        
    });
}
@end
