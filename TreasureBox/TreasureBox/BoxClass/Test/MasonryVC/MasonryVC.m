//
//  MasonryVC.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "MasonryVC.h"

@interface MasonryVC (){
    UIView *view3;
}

@end

@implementation MasonryVC

//动画
-(void)updateViewConstraints{
   

     [super updateViewConstraints];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIView *view1 = [self addView];
   
    UIView *view2 = [self addView];
    
    
    view3 = [self addView];
    
    
    
    
    
    
    
    
    [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(100, 100));
    }];
    
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view1).insets(UIEdgeInsetsMake(5, 5, 5, 5));
    }];
    
    
    [view3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(20);
        make.top.equalTo(self.view).with.offset(64);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (int i = 0; i<6; i++) {
        UIView *vieww = [self addView];
        UIView *last = [arr lastObject];
        
        if (!last) {
            last = view3;
        }
        [vieww mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(last.mas_bottom).with.offset(10);
            make.left.equalTo(last).with.offset(10);
            make.size.equalTo(last);
            
        }];
        
        [arr addObject:vieww];
        
        
    }
    
    
    UIView *view5 = [self addView];
    view5.frame = CGRectMake(100, 70, 50, 50);
    
    
    UIView *view6 = [self addView];
    view6.frame = CGRectMake(view5.mj_x+view5.width+20, 75, 50, 50);
    
    view5.width = 100;
    
  
 
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    
    [UIView animateWithDuration:2 animations:^{
       
        [view3 mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).with.offset(100);
            make.top.equalTo(self.view).with.offset(64);
            make.size.mas_equalTo(CGSizeMake(40, 40));
            
        }];
        //动画布局完后要调用这个方法
        [view3.superview layoutIfNeeded];
    }];
}

-(UIView*)addView{
    UIView *view1 = [UIView new];
    [self.view addSubview:view1];
    view1.backgroundColor = UIColorWithRandom;
    return view1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
