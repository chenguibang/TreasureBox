//
//  ProductListModel.h
//  TreasureBox
//
//  Created by chenguibang on 16/3/14.
//  Copyright © 2016年 Chen. All rights reserved.
//
//这个文件存放 View 的数据源

#import <Foundation/Foundation.h>

@interface ProductListModel : NSObject
@property(nonatomic,copy)NSArray *productList;
@property(nonatomic,assign) BOOL allUp;//!<全部排序 按钮状态
@property(nonatomic,assign) BOOL sellUp;//!<销量排序按钮状态
@property(nonatomic,assign) BOOL pricrUp; //!< 价格排序按钮状态

@end
