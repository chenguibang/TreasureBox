//
//  YYKitVC.m
//  TreasureBox
//
//  Created by chenguibang on 16/7/7.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "YYKitVC.h"
#import "YYLabel.h"
#import "YYTextLayout.h"

@interface YYKitVC ()

@end

@implementation YYKitVC

- (void)viewDidLoad {
    [super viewDidLoad];
    YYLabel *lab = [YYLabel new];
//    lab.top = 30;
    lab.frame = CGRectMake(10, 100, 300, 100);
    lab.textVerticalAlignment = YYTextVerticalAlignmentTop;
    lab.displaysAsynchronously = YES;
    lab.ignoreCommonProperties = YES;
    lab.fadeOnAsynchronouslyDisplay = YES;
    lab.fadeOnHighlight = YES;
//    lab.backgroundColor = [UIColor redColor];
    
    lab.highlightTapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
       
        
        NSLog(@"点击了  - %@ ", [text.string substringWithRange:range])
    };
   NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:@"我家这个好忠犬啊～[喵喵]  http://t.cn/Ry4UXdF //@我是呆毛芳子蜀黍w:这是什么鬼？  http://t.cn/Ry4U5fQ //@清新可口喵酱圆脸星人是扭蛋狂魔:窝家这个 超委婉的拒绝了窝 http://t.cn/Ry4ylqt //@GloriAries:我家这位好高冷orz http://t.cn/RyUsE79 //@-水蛋蛋-:我的是玩咖即视感  http://t.cn/RyUsS8Q "];
    YYTextContainer *container = [YYTextContainer new];
    container.size = CGSizeMake(300, MAXFLOAT );
    container.insets = UIEdgeInsetsMake(0, 10, 0, 10);

    
    
    YYTextBorder *highlightBorder = [YYTextBorder new];
//    highlightBorder.insets = UIEdgeInsetsMake(-2, 0, -2, 0);
    highlightBorder.cornerRadius = 3;
    highlightBorder.fillColor = [UIColor purpleColor];
    highlightBorder.lineStyle = YYTextLineStylePatternDot;
    
    
    YYTextHighlight *highlight = [YYTextHighlight new];
    [highlight setBackgroundBorder:highlightBorder];
    // 数据信息，用于稍后用户点击
//    highlight.userInfo = @{@"url" : @"http://www.baidu.com"};

    
    
     NSMutableAttributedString *replace = [[NSMutableAttributedString alloc] initWithString:@"查看图片呵呵呵"];
    YYTextBackedString *backed = [YYTextBackedString stringWithString:[text.string substringWithRange:[text.string rangeOfString:@"http://t.cn/Ry4UXdF"]]];
    [replace setTextBackedString:backed range:NSMakeRange(0, replace.length)];
    [replace setTextHighlight:highlight range:NSMakeRange(0, replace.length)];
    
    
    //阴影
    replace.font = [UIFont boldSystemFontOfSize:30];
    replace.color = [UIColor whiteColor];
    YYTextShadow *shadow = [YYTextShadow new];
    shadow.color = [UIColor yellowColor];
    shadow.offset = CGSizeMake(0, 2);
    shadow.radius = 3;
    replace.textInnerShadow = shadow;
    

    [replace setTextHighlightRange:NSMakeRange(0, replace.length) color:[UIColor purpleColor] backgroundColor:[UIColor redColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        
    }];
    
    
    [text replaceCharactersInRange:[text.string rangeOfString:@"http://t.cn/Ry4UXdF"] withAttributedString:replace];
    
  
    
    [text setTextHighlightRange:NSMakeRange(0, 5) color:[UIColor orangeColor] backgroundColor:[UIColor redColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        
    }];
    
    //    [text setTextBorder:highlightBorder];
    YYTextLayout *telayout =[YYTextLayout layoutWithContainer:container text:text];
    lab.textLayout = telayout;
    lab.height = telayout.rowCount*lab.font.pointSize+30-lab.font.pointSize;
    [self.view addSubview:lab];
      NSLog(@"dddd---ddd  : %lu",(unsigned long)telayout.rowCount);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
