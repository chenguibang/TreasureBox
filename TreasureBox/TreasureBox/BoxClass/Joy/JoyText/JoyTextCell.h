//
//  JoyHomeCell.h
//  TreasureBox
//
//  Created by Chen on 16/2/10.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JoyTextRsp.h"

@interface JoyTextCell : UITableViewCell
@property(nonatomic,strong) JoyTextRsp *model;
@property(nonatomic,strong) UILabel *joyContent;
@end
