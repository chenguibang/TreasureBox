//
//  JoyTextRsp.h
//  TreasureBox
//
//  Created by Chen on 16/2/8.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JoyTextRsp : NSObject
@property(nonatomic,copy) NSString *unixtime;
@property(nonatomic,copy) NSString *updatetime;
@property(nonatomic,copy) NSString *content;
@property(nonatomic,copy) NSString *hashId;
@end
