//
//  JoyTextReq.h
//  TreasureBox
//
//  Created by Chen on 16/2/8.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JoyTextReq : ReqBodyModel
@property(nonatomic,copy)NSString *sort;
@property(nonatomic,copy)NSString *pagesize;
@property(nonatomic,copy)NSString *page;
@property(nonatomic,copy)NSString *key;
@property(nonatomic,copy)NSString *time;
@end
