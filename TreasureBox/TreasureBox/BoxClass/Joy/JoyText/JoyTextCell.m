//
//  JoyHomeCell.m
//  TreasureBox
//
//  Created by Chen on 16/2/10.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "JoyTextCell.h"

@implementation JoyTextCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self creatView];
        [self makeConstr];
    }
    return self;
}
-(void)creatView{
    self.joyContent = [UILabel new];
    [self.contentView addSubview:self.joyContent];
    
}

-(void)makeConstr{
    self.joyContent.sd_layout.leftSpaceToView(self.contentView,5).topSpaceToView(self.contentView,5).rightSpaceToView(self.contentView,5).autoHeightRatio(0);
    [self setupAutoHeightWithBottomView:self.joyContent bottomMargin:5];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(JoyTextRsp *)model{
    self.joyContent.text = model.content;
}
@end
