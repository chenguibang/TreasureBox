//
//  JoyTextReq.m
//  TreasureBox
//
//  Created by Chen on 16/2/8.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "JoyTextReq.h"

@implementation JoyTextReq
-(NSDictionary *)formatToDict{
    [super formatToDict];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          self.key,@"key",
                          self.time,@"time",
                          self.pagesize,@"pagesize",
                          self.page,@"page",
                          self.sort,@"sort",
                          nil];
    return dict;
}
@end
