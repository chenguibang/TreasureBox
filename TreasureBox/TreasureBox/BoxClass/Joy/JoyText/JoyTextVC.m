//
//  JoyHomeVC.m
//  TreasureBox
//
//  Created by Chen on 16/2/4.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "JoyTextVC.h"
#import "JoyTextReq.h"
#import "JoyTextRsp.h"
#import "MJExtension.h"
#import "JoyTextCell.h"
@interface JoyTextVC ()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>{
    UITableView *joyHomeTable;
    JoyTextReq *textReq;
    NSMutableArray *textReqArr;
    JoyTextRsp *joyTextModel;
}

@end

@implementation JoyTextVC


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = YES;
//    self.boxNavigationBarHiden = YES;
    self.boxNavigationBar.hideAllBar = YES;
    textReq = [[JoyTextReq alloc]init];
    textReqArr = [[NSMutableArray alloc]init];
    joyTextModel = [[JoyTextRsp alloc]init];
    
    
    
    
    textReq.key = @"17a68b6772ec38084b7e4c90c94c4a9a";
    textReq.page = @"3";
    textReq.pagesize = @"20";
    textReq.sort = @"asc";
    textReq.time = @"1418745237";
    [self sendTextReq];
    [self creatTableView];
//    
//    self.boxContentView.layer.borderWidth = 9;
//    self.boxContentView.layer.borderColor = [UIColor yellowColor].CGColor;
    
  
    
}



-(void)creatTableView{
    joyHomeTable = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    [self.boxContentView addSubview:joyHomeTable];
    joyHomeTable.sd_layout.leftEqualToView(self.boxContentView).topEqualToView(self.boxContentView).rightEqualToView(self.boxContentView).bottomSpaceToView(self.boxContentView,45);
    joyHomeTable.delegate = self;

    joyHomeTable.dataSource =self;
    
//    joyHomeTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        
//    }];
    
    
    joyHomeTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self sendTextReq];
    }];
    
     [joyHomeTable registerClass:[JoyTextCell class] forCellReuseIdentifier:@"joyHomeCell"];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
      
    });
}

-(void)sendTextReq{
    
    
//    BaseReqModel *re = [[BaseReqModel alloc]init];
//    
//    re.baseUrl = @"http://japi.juhe.cn/joke/content/list.from";
//    
//    GBNetToolOperation *opt = [[GBNetToolOperation alloc]init];
//    opt.thirdServer = YES;
//    
//    [GBNetTool netGetAsyn:re success:^(NSDictionary *result) {
//        
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        
//    } operation:opt];
    
    
    

    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CoreData" withExtension:@"momd"];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"CoreData" ofType:@"momd"];
    
    
    
    
    BaseReqModel *re = [[BaseReqModel alloc]init];
  
    re.body = textReq;
    re.baseUrl = @"http://japi.juhe.cn/joke/content/list.from";

    GBNetToolOperation *opt = [[GBNetToolOperation alloc]init];
    opt.thirdServer = YES;
    opt.cachePolicy = GBCachePolicyPermanent;
    
    
//    
//    [GBNetTool netGetAsyn:re success:^(NSDictionary *result) {
//        [joyHomeTable.mj_footer endRefreshing];
//        NSArray *arr = [NSArray arrayWithArray:[JoyTextRsp mj_objectArrayWithKeyValuesArray:result[@"result"][@"data"]]];
//        if (arr.count!=0) {
//            [textReqArr addObjectsFromArray:arr];
//            
//            int i = [textReq.page intValue];
//            i ++;
//            textReq.page = [NSString stringWithFormat:@"%d",i];
//            [joyHomeTable reloadData];
//        }
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        
//    } operation:opt];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return textReqArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JoyTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"joyHomeCell"];
    cell.model = textReqArr[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // >>>>>>>>>>>>>>>>>>>>> * cell自适应 * >>>>>>>>>>>>>>>>>>>>>>>>
    id model = textReqArr[indexPath.row];
    return [joyHomeTable cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[JoyTextCell class] contentViewWidth:[self cellContentViewWith]];
}
- (CGFloat)cellContentViewWith
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    // 适配ios7
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && [[UIDevice currentDevice].systemVersion floatValue] < 8) {
        width = [UIScreen mainScreen].bounds.size.height;
    }
    return width;
}


//BOOL yy;
//BOOL isDrag;
//-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
//    isDrag = YES;
//}
//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//    isDrag = NO;
//}


int temp_y;
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y - temp_y>0&&scrollView.contentOffset.y>0) {
       
        XTPageViewController *pa = (XTPageViewController*)self.tapRootViewControler;
        [pa hideBar:YES];
    }else{
        
        if (scrollView.contentOffset.y<scrollView.contentSize.height-scrollView.frame.size.height) {
            XTPageViewController *pa = (XTPageViewController*)self.tapRootViewControler;
            [pa hideBar:NO];
        }
      
    }
    
    temp_y = scrollView.contentOffset.y;
    
    
    //    if (yy&&!isDrag) {
    //
    //        yy = !yy;
    //    }
    //
    
    
    
}

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
////    if (!yy&&!isDrag) {
////        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
////            XTPageViewController *pa = (XTPageViewController*)self.tapRootViewControler;
////            [pa hideBar:YES];
////            yy = !yy;
////        });
////       
////    }
//}
@end
