//
//  JoyVideoVC.m
//  TreasureBox
//
//  Created by Chen on 16/2/13.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "JoyVideoVC.h"
#import "JoyVideoCell.h"
#import "XTPageViewController.h"
static NSString * ID = @"cell";
@interface JoyVideoVC ()<UITableViewDataSource,UITableViewDelegate,FMGVideoPlayViewDelegate>{
    UITableView *joyVideoTable;

    NSMutableArray *joyVideoArr;
    
    int  i;
}

@property (nonatomic, strong) FMGVideoPlayView * fmVideoPlayer; // 播放器
@property (nonatomic, strong) FullViewController *fullVc;

@end

@implementation JoyVideoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.boxNavigationBar.hideAllBar = YES;
    [self creatTable];
    
    
    joyVideoArr = [[NSMutableArray alloc]init];
  
    self.fmVideoPlayer = [FMGVideoPlayView videoPlayView];
    self.fmVideoPlayer.delegate = self;
    
    
    
    
     i = 0;
    [self sendJoyVideoReq];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifi:) name:NOTFICTION_STOPPLAY object:nil];
   
}

-(void)notifi:(NSNotification*)noti{

    
    if (_fmVideoPlayer) {
         [_fmVideoPlayer.player pause];
    }
    
    
}


-(void)creatTable{
    joyVideoTable = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    
    [self.boxContentView addSubview:joyVideoTable];
    joyVideoTable.sd_layout.leftEqualToView(self.boxContentView).topEqualToView(self.boxContentView).rightEqualToView(self.boxContentView).bottomSpaceToView(self.boxContentView,45);
    joyVideoTable.delegate = self;
    joyVideoTable.dataSource =self;
    joyVideoTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self sendJoyVideoReq];
    }];
    [joyVideoTable registerClass:[JoyVideoCell class] forCellReuseIdentifier:ID];
}


-(void)sendJoyVideoReq{
    BaseReqModel *re = [[BaseReqModel alloc]init];
    
    re.baseUrl = [NSString stringWithFormat:@"http://c.m.163.com/nc/video/home/%ld-10.html",joyVideoArr.count - joyVideoArr.count%10];
    

    
//    [[GBNetTool shareGBNetTool] netReqAsyncGet:re success:^(NSDictionary *result) {
//
//        
//        NSArray *arr = [NSArray arrayWithArray:[JoyVideoModel mj_objectArrayWithKeyValuesArray:result[@"videoList"]]];
//        
//        
//        if (arr.count!=0) {
//            [joyVideoArr addObjectsFromArray:arr];
//            
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                 [joyVideoTable reloadData];
//            });
//           
//        }
//        
//        
//        
//    }];
    GBNetToolOperation *opt = [[GBNetToolOperation alloc]init];
    opt.thirdServer = YES;
    [GBNetTool netGetAsyn:re success:^(NSDictionary *result) {
                NSArray *arr = [NSArray arrayWithArray:[JoyVideoModel mj_objectArrayWithKeyValuesArray:result[@"videoList"]]];
        
        
                if (arr.count!=0) {
                    [joyVideoArr addObjectsFromArray:arr];
        
        
                    dispatch_async(dispatch_get_main_queue(), ^{
                         [joyVideoTable reloadData];
                    });
                   
                }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    } operation:opt];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return joyVideoArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    JoyPicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"joyPicCell"];
//    cell.model = joyVideoArr[indexPath.row];
//    return cell;
    
    JoyVideoCell *cell = [tableView cellForRowAtIndexPath:indexPath]; //根据indexPath准确地取出一行，而不是从cell重用队列中取出
    if (cell == nil) {
        cell = [[JoyVideoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.title.text =@"dddd";
    
    cell.video = joyVideoArr[indexPath.row];
    [cell.playButton addTarget:self action:@selector(playButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.playButton.tag = 100 + indexPath.row;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // >>>>>>>>>>>>>>>>>>>>> * cell自适应 * >>>>>>>>>>>>>>>>>>>>>>>>
//    id model = joyPicArr[indexPath.row];
//    return [joyPicTable cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[JoyPicCell class] contentViewWidth:[self cellContentViewWith]];
    
    
    return 275;

    
    
}
- (CGFloat)cellContentViewWith
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    // 适配ios7
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && [[UIDevice currentDevice].systemVersion floatValue] < 8) {
        width = [UIScreen mainScreen].bounds.size.height;
    }
    return width;
}


- (void)playButtonAction:(UIButton *)sender{
    
    
    
    _fmVideoPlayer.index = sender.tag - 100;
        JoyVideoCell *cell =   [joyVideoTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_fmVideoPlayer.index inSection:0]];
    JoyVideoModel * video = joyVideoArr[sender.tag - 100];
    [_fmVideoPlayer setUrlString:video.mp4_url];
    _fmVideoPlayer.frame = CGRectMake(0, CGRectGetMaxY(cell.descriptionLabel.frame), SCREEN_WIDTH, CGRectGetWidth(cell.title.frame)/2);
    [cell addSubview:_fmVideoPlayer];
    _fmVideoPlayer.contrainerViewController = self;
    [_fmVideoPlayer.player play];
    [_fmVideoPlayer showToolView:NO];
    _fmVideoPlayer.playOrPauseBtn.selected = YES;
    _fmVideoPlayer.hidden = NO;

}



// 根据Cell位置隐藏并暂停播放
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == _fmVideoPlayer.index) {
        [_fmVideoPlayer.player pause];
        _fmVideoPlayer.hidden = YES;
    }
    
}

#pragma mark - 懒加载代码
- (FullViewController *)fullVc
{
    if (_fullVc == nil) {
        _fullVc = [[FullViewController alloc] init];
    }
    return _fullVc;
}


#pragma mark - 视频delegate
- (void)videoplayViewSwitchOrientation:(BOOL)isFull{
    if (isFull) {
        [self.tapRootViewControler.navigationController presentViewController:self.fullVc animated:NO completion:^{
            [self.fullVc.view addSubview:self.fmVideoPlayer];
            _fmVideoPlayer.center = self.fullVc.view.center;
            
            [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
                _fmVideoPlayer.frame = self.fullVc.view.bounds;
                self.fmVideoPlayer.danmakuView.frame = self.fmVideoPlayer.frame;

                
                
            } completion:nil];
        }];
    } else {
        [self.fullVc dismissViewControllerAnimated:NO completion:^{

            
            
               JoyVideoCell *cell =   [joyVideoTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_fmVideoPlayer.index inSection:0]];
                _fmVideoPlayer.frame = CGRectMake(0, CGRectGetMaxY(cell.descriptionLabel.frame), SCREEN_WIDTH, CGRectGetWidth(cell.title.frame)/2);
               [cell addSubview:_fmVideoPlayer];
            
        }];
    }
    
}

int temp_y3;
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y - temp_y3>0&&scrollView.contentOffset.y>0) {
  
        XTPageViewController *pa = (XTPageViewController*)self.tapRootViewControler;
        [pa hideBar:YES];
        
        self.tabBarController.tabBar.hidden = YES;
        
//        [self hideTabBar:pa];
    }else{
        
        if (scrollView.contentOffset.y<scrollView.contentSize.height-scrollView.frame.size.height) {
            XTPageViewController *pa = (XTPageViewController*)self.tapRootViewControler;
            [pa hideBar:NO];
//            [self showTabBar:pa];
        }
    
        
    }
    
    temp_y3 = scrollView.contentOffset.y;
    
    
    //    if (yy&&!isDrag) {
    //
    //        yy = !yy;
    //    }
    //
    
    
    
}


//- (void)hideTabBar:(XTPageViewController *) tabbarcontroller
//{
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:0.5];
//    
//    for(UIView *view in tabbarcontroller.view.subviews)
//    {
//        if([view isKindOfClass:[UITabBar class]])
//        {
//            [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
//        }
//        else
//        {
//            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
//        }
//    }
//    
//    [UIView commitAnimations];
//}
//
//- (void)showTabBar:(XTPageViewController *) tabbarcontroller
//{
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:0.5];
//    for(UIView *view in tabbarcontroller.view.subviews)
//    {
//        NSLog(@"%@", view);
//        
//        if([view isKindOfClass:[UITabBar class]])
//        {
//            [view setFrame:CGRectMake(view.frame.origin.x, 431, view.frame.size.width, view.frame.size.height)];
//            
//        }
//        else
//        {
//            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 431)];
//        }
//    }
//    
//    [UIView commitAnimations];
//}



-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
     [_fmVideoPlayer.player pause];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTFICTION_STOPPLAY object:nil];
    
}


-(void)scrollViewDidScrollToTop:(UIScrollView *)scrollView{
    
}
@end
