//
//  JoyPicCell.h
//  TreasureBox
//
//  Created by Chen on 16/2/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JoyPicModel.h"

@interface JoyPicCell : UITableViewCell
@property(nonatomic,strong)JoyPicModel *model;
@property(nonatomic,strong)UILabel *joyContent;
@property(nonatomic,strong)UIImageView *joyImageView;

@end
