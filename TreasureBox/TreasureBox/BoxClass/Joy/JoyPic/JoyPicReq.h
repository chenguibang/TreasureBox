//
//  JoyPicReq.h
//  TreasureBox
//
//  Created by Chen on 16/2/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <GBNetWork/GBNetWork.h>

@interface JoyPicReq : ReqBodyModel
@property(nonatomic,copy)NSString *sort;
@property(nonatomic,copy)NSString *pagesize;
@property(nonatomic,copy)NSString *page;
@property(nonatomic,copy)NSString *key;
@property(nonatomic,copy)NSString *time;
@end
