//
//  JoyPicModel.h
//  TreasureBox
//
//  Created by Chen on 16/2/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JoyPicModel : NSObject
@property(nonatomic,copy)NSString *content;
@property(nonatomic,copy)NSString *hashId;
@property(nonatomic,copy)NSString *unixtime;
@property(nonatomic,copy)NSString *updatetime;
@property(nonatomic,copy)NSString *url;
@end
