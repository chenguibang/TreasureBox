//
//  JoyPicVC.m
//  TreasureBox
//
//  Created by Chen on 16/2/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "JoyPicVC.h"
#import "JoyPicCell.h"
#import "JoyPicReq.h"
#import "XTPageViewController.h"

@interface JoyPicVC ()<UITableViewDataSource,UITableViewDelegate>{
    UITableView *joyPicTable;
    JoyPicReq *joyPicReq;
    NSMutableArray *joyPicArr;
}

@end

@implementation JoyPicVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.boxNavigationBar.hideAllBar = YES;
    joyPicReq = [[JoyPicReq alloc]init];
    joyPicArr = [[NSMutableArray alloc]init];
    
    
    [self creatTableView];
    
    
    joyPicReq.key = @"17a68b6772ec38084b7e4c90c94c4a9a";
    joyPicReq.page = @"1";
    joyPicReq.pagesize = @"20";
    joyPicReq.sort = @"asc";
    joyPicReq.time = @"1418745237";
    [self sendJoyPicReq];
    

}

-(void)creatTableView{
    joyPicTable = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    
    
    [self.boxContentView addSubview:joyPicTable];
    joyPicTable.sd_layout.leftEqualToView(self.boxContentView).topEqualToView(self.boxContentView).rightEqualToView(self.boxContentView).bottomSpaceToView(self.boxContentView,45);
    joyPicTable.delegate = self;
    joyPicTable.dataSource =self;
    joyPicTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self sendJoyPicReq];
    }];
    [joyPicTable registerClass:[JoyPicCell class] forCellReuseIdentifier:@"joyPicCell"];
}

-(void)sendJoyPicReq{
    BaseReqModel *re = [[BaseReqModel alloc]init];
   
    re.body = joyPicReq;
    re.baseUrl = @"http://japi.juhe.cn/joke/img/list.from";
    
    GBNetToolOperation *opt = [[GBNetToolOperation alloc]init];
    opt.thirdServer = YES;
    opt.cachePolicy = GBCachePolicyPermanent;
    [GBNetTool netGetAsyn:re success:^(NSDictionary *result) {
        [joyPicTable.mj_footer endRefreshing];
        
        
        NSArray *arr = [NSArray arrayWithArray:[JoyPicModel mj_objectArrayWithKeyValuesArray:result[@"result"][@"data"]]];
        
        
        if (arr.count!=0) {
            [joyPicArr addObjectsFromArray:arr];
            
            int i = [joyPicReq.page intValue];
            i ++;
            joyPicReq.page = [NSString stringWithFormat:@"%d",i];
            [joyPicTable reloadData];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    } operation:opt];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return joyPicArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JoyPicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"joyPicCell"];
    cell.model = joyPicArr[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // >>>>>>>>>>>>>>>>>>>>> * cell自适应 * >>>>>>>>>>>>>>>>>>>>>>>>
    id model = joyPicArr[indexPath.row];
    return [joyPicTable cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[JoyPicCell class] contentViewWidth:[self cellContentViewWith]];
}
- (CGFloat)cellContentViewWith
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    // 适配ios7
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait && [[UIDevice currentDevice].systemVersion floatValue] < 8) {
        width = [UIScreen mainScreen].bounds.size.height;
    }
    return width;
}

int temp_y2;
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y - temp_y2>0&&scrollView.contentOffset.y>0) {
     
        XTPageViewController *pa = (XTPageViewController*)self.tapRootViewControler;
        [pa hideBar:YES];
    }else{
        
        if (scrollView.contentOffset.y<scrollView.contentSize.height-scrollView.frame.size.height) {
            XTPageViewController *pa = (XTPageViewController*)self.tapRootViewControler;
            [pa hideBar:NO];
        }
      
        
    }
    
    temp_y2 = scrollView.contentOffset.y;
    
    
    //    if (yy&&!isDrag) {
    //
    //        yy = !yy;
    //    }
    //
    
    
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
