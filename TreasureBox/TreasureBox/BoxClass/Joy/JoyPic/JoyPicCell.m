//
//  JoyPicCell.m
//  TreasureBox
//
//  Created by Chen on 16/2/11.
//  Copyright © 2016年 Chen. All rights reserved.
//

#import "JoyPicCell.h"

@implementation JoyPicCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self creatView];
        [self makeConstri];
    }
    return self;
}


-(void)creatView{
    self.joyContent = [UILabel new];
    [self.contentView addSubview:self.joyContent];
    self.joyImageView = [UIImageView new];
    [self.contentView addSubview:self.joyImageView];
}

-(void)makeConstri{
    self.joyContent.sd_layout.leftSpaceToView(self.contentView,5).topSpaceToView(self.contentView,5).rightSpaceToView(self.contentView,5).autoHeightRatio(0);
    self.joyImageView.sd_layout.leftEqualToView(self.joyContent).topSpaceToView(self.joyContent,2).rightEqualToView(self.joyContent);
    [self setupAutoHeightWithBottomView:self.joyImageView bottomMargin:5];
}





- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setModel:(JoyPicModel *)model{
    self.joyContent.text = model.content;
  
    
    
    [self.joyImageView setShowActivityIndicatorView:YES];
    [self.joyImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.joyImageView sd_setImageWithURL:[NSURL URLWithString:model.url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    
     

        
    }];
    
    
//    if (image.size.width > 0) {
//        CGFloat scale = image.size.height / image.size.width;
//        self.joyImageView.sd_layout.autoHeightRatio(scale);
//        self.joyImageView.image = image;
//        
//    } else {
//        self.joyImageView.sd_layout.autoHeightRatio(0);
//        
//    }
    
    self.joyImageView.sd_layout.autoHeightRatio(1);
    [self setupAutoHeightWithBottomView:self.joyImageView bottomMargin:5];
    
}

@end
