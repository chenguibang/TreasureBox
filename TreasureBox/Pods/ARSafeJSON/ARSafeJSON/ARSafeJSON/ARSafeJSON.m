//
//  ARSafeJSON.m
//  ARSafeJSON
//
//  Created by Antoine Rabanes on 18/09/2012.
//  Copyright (c) 2012 Antoine Rabanes. All rights reserved.
//
//Copyright (c) 2012 App Revolution Ltd
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "ARSafeJSON.h"

@interface ARSafeJSON()

- (BOOL)isthisADirtyObject:(id)anObject;
- (BOOL)isThisObjectACollection:(NSObject *)anObject;
- (NSObject *)cleanUpCollection:(NSObject *)aCollection;
@end

@implementation ARSafeJSON

- (id)cleanUpJson:(id)JSON
{
    if (!JSON) return nil;
    if ([JSON isKindOfClass:[NSNull class]]) return nil;
    id cleanJSON = [self cleanUpCollection:JSON];
    return cleanJSON;
}

- (NSArray *)cleanUpArray:(NSArray *)anArray
{
    NSEnumerator *enumerator = [anArray objectEnumerator];
    NSMutableArray *mutableArray = [NSMutableArray new];
    NSObject *object;
    
    while (object = [enumerator nextObject])
    {
        if ([self isThisObjectACollection:object])
            object = [self cleanUpCollection:object];
        if (![self isthisADirtyObject:object])
            [mutableArray addObject:object];
    }
    return mutableArray;
}

- (NSDictionary *)cleanUpDictionary:(NSDictionary *)aDictionary
{
    NSMutableDictionary *mutableDico = [NSMutableDictionary new];
    NSArray *keys = [aDictionary allKeys];
    for (NSString *key in keys)
    {
        NSObject *object = [aDictionary objectForKey:key];
        if ([self isThisObjectACollection:object])
            object = [self cleanUpCollection:object];
        if (![self isthisADirtyObject:object])
            [mutableDico setObject:object forKey:key];
    }
    return mutableDico;
}

- (NSObject *)cleanUpCollection:(NSObject *)aCollection
{    
    if ([aCollection isKindOfClass:[NSArray class]])
        return [self cleanUpArray:(NSArray *)aCollection];
    return [self cleanUpDictionary:(NSDictionary *)aCollection];
}

- (BOOL)isThisObjectACollection:(NSObject *)anObject;
{
    if ([anObject isKindOfClass:[NSDictionary class]]) return YES;
    
    if ([anObject isKindOfClass:[NSArray class]]) return YES;
    
    return NO;
}

- (BOOL)isthisADirtyObject:(id)anObject;
{
    if ([anObject isKindOfClass:[NSString class]]) {
        NSString *aString = anObject;
        if ([aString isEqualToString:@""]) return YES;
    }
    if ([anObject isKindOfClass:[NSNull class]]) return YES;
    return NO;
}

@end
